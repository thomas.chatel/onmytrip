# OnMyTrip

Projet annuel de 5ème année à l'ESGI sur un site qui permet de créer son roadtrip

##Si les logs ne marchent pas

 - Pour chaque microservice concerné, créez un sous dossier dans docker/ avec le nom du microservice. 
   Créer dans chaque sous dossier un Dockerfile, écrire dedans :
    ```
   FROM node:16-alpine3.11
   
   RUN mkdir /var/log/<Le nom du micro service>
   RUN chown 1000:1000 /var/log/<Le nom du micro service>
   ```
 - Pour chachun des microservices dans le docker-compose.yml , mettre :
    ```
    build: ./docker/<Le nom du micro service>
    volumes: 
    -  users_log:/var/log/<Le nom du micro service>
    ```
 - Et dans les logger.js de chaque microservice concerné, remplacer
 ``` 
 '/var/log/<Le nom du micro service>.log'
 ``` 
 par 
 ```
 '/var/log/<Le nom du micro service>/<Le nom du micro service>.log'
 ```

##Si nodemon ne marche pas sur docker pour windows
- Aller dans le package.json, et rajouter --legacy-watch en argument pour lancer le projet

##Fichier d'environnement
 - Dans app/jwt_api, app/mailer_api et app/users_api : copier .env.example dans .env
 - Copier .env.example.rabbitmq dans .env.rabbitmq
 
##Envoie de mails 
 - Dans app/mailer_api/.env, mettre des login smtp corrects

##Installer les dépendances
 - Dans tout les sous dossiers de app/ : npm install
 
##Lancer le projet
  docker-compose up -d