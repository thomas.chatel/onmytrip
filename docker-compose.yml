version: '3'
services:

  #vuejs_dev:
  #  image: node:14-alpine3.11
  #  container_name: vuejs_dev
  #  volumes:
  #    - ./app/vuejs:/app
  #  working_dir: /app
  #  command: sh -c "npm i && npm run serve"
  #  ports:
  #    - 80:8080
  #  networks:
  #    - web

  vuejs:
    build:
      dockerfile: ./Dockerfile
      context: ./app/vuejs
    container_name: vuejs
    ports:
      - 80:80
    networks:
      - web

  gateway:
    image: nginx:stable
    container_name: gateway
    depends_on:
      - poi_api
      - trip_api
      - route_api
      - users_api
    volumes:
      - gateway_log:/var/log/gateway
      - ./docker/gateway/nginx.conf:/etc/nginx/conf.d/default.conf:ro
    networks:
      - web
      - users_api
      - trip_api
      - jwt_api
      - route_api
    ports:
      - 8081:80

  users_api:
    env_file:
      - ./app/users_api/.env
      - .env.rabbitmq
    build:
      args:
        API_NAME: users
      context: ./docker/node
    container_name: users_api
    working_dir: /app
    volumes:
      - ./app/users_api:/app
      - users_log:/var/log/users
      - ./app/common_libs:/app/common_libs
    command: npm run dev
    networks:
      - users_api
      - users_db
      - jwt_api
      - mailer_api
      - rabbitmq

  users_db:
    image: postgres:9.6-alpine
    container_name: users_db
    depends_on:
      - rabbitmq
    environment:
      - POSTGRES_DB=db
      - POSTGRES_USER=user
      - POSTGRES_PASSWORD=pwd
    volumes:
      - users_data:/var/lib/postgresql/data:rw
    ports:
      - '5432:5432'
    networks:
      - users_db

  jwt_api:
    build:
      args:
        API_NAME: jwt
      context: ./docker/node
    container_name: jwt_api
    command: npm run dev
    working_dir: /app
    volumes:
      - ./app/jwt_api:/app
    networks:
      - jwt_api
    env_file:
      - ./app/jwt_api/.env

  mailer_api:
    env_file:
      - ./app/mailer_api/.env
      - .env.rabbitmq
    build:
      context: ./docker/node
      args:
        API_NAME: mailer
    container_name: mailer_api
    command: npm run dev
    working_dir: /app
    depends_on:
      - rabbitmq
    volumes:
    - ./app/mailer_api:/app
    - mailer_log:/var/log/mailer
    - ./app/common_libs:/app/common_libs
    networks:
      - web #Mailer need to access to the web to contact smtp servers
      - mailer_api
      - rabbitmq

  trip_api:
    env_file:
      - ./app/trip_api/.env
    build:
      context: ./docker/node
      args:
        API_NAME: trip
    container_name: trip_api
    working_dir: /app
    depends_on:
      - trip_db
      - poi_api
    volumes:
      - ./app/trip_api:/app
      - trip_log:/var/log/trip
      - ./app/common_libs:/app/common_libs
    command: npm run dev
    networks:
      - trip_api
      - route_api
      - poi_api
      - jwt_api

  trip_db:
    image: mongo:bionic
    volumes:
      - ./app/trip_db/mongod.conf:/etc/mongod.conf
      - trip_data:/data/db/
      - ./app/trip_db/initdb.d/:/docker-entrypoint-initdb.d/
    env_file:
      - ./app/trip_api/.env
    networks:
      - trip_api

  trip_manager_api:
    env_file:
      - ./app/trip_manager_api/.env
      - .env.rabbitmq
    build:
      context: ./docker/node
      args:
        API_NAME: trip_manager
    container_name: trip_manager_api
    working_dir: /app
    volumes:
      - ./app/trip_manager_api:/app
      - trip_manager_log:/var/log/trip_manager
      - ./app/common_libs:/app/common_libs
    command: npm run dev
    networks:
      - trip_manager_api
      - jwt_api
      - rabbitmq
      - web

  trip_manager_db:
    image: mongo:bionic
    volumes:
      - ./app/trip_manager_db/mongod.conf:/etc/mongod.conf
      - trip_manager_data:/data/db/
      - ./app/trip_manager_db/initdb.d/:/docker-entrypoint-initdb.d/
    env_file:
      - ./app/trip_manager_api/.env
    networks:
      - trip_manager_api

  poi_api:
    env_file:
      - ./app/poi_api/.env
    build:
      context: ./docker/node
      args:
        API_NAME: poi
    container_name: poi_api
    working_dir: /app
    volumes:
      - ./app/poi_api:/app
      - poi_log:/var/log/poi
      - ./app/common_libs:/app/common_libs
    command: npm run dev
    networks:
      - web
      - poi_api

  poi_db:
    image: postgres:9.6-alpine
    volumes:
      - poi_data:/var/lib/postgresql/data:rw
    container_name: poi_db
    env_file:
      - ./app/poi_api/.env
    networks:
      - poi_api

  route_api:
    env_file:
      - ./app/route_api/.env
    build:
      context: ./docker/node
      args:
        API_NAME: route
    container_name: route_api
    working_dir: /app
    volumes:
      - ./app/route_api:/app
      - route_log:/var/log/route
      - ./app/common_libs:/app/common_libs
    command: npm run dev
    networks:
      - web
      - route_api
      - jwt_api

  city_cache:
    image: 'redis:alpine'
    container_name: city_cache
    ports:
      - '6379:6379'

  rabbitmq:
    image: library/rabbitmq:3.9-management
    container_name: rabbitmq
    networks:
      - rabbitmq
    env_file:
      - .env.rabbitmq

  logstash:
    image: docker.elastic.co/logstash/logstash:7.4.2
    container_name: logstash
    volumes:
      - ./docker/elk/logstash/config/logstash.yml:/usr/share/logstash/config/logstash.yml:ro
      - ./docker/elk/logstash/pipeline:/usr/share/logstash/pipeline:ro
      - users_log:/var/log/users:ro
      - mailer_log:/var/log/mailer:ro
      - route_log:/var/log/route:ro
      - poi_log:/var/log/poi:ro
      - trip_log:/var/log/trip:ro
      - trip_manager_log:/var/log/trip_manager:ro
    environment:
      LS_JAVA_OPTS: "-Xmx256m -Xms256m"
    networks:
      - elk
    depends_on:
      - elasticsearch

  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch:7.4.2
    container_name: elasticsearch
    volumes:
      - ./docker/elk/elasticsearch/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml
      - elasticsearch:/usr/share/elasticsearch/data
    ports:
      - "9200:9200"
      - "9300:9300"
    environment:
      ES_JAVA_OPTS: "-Xmx256m -Xms256m"
      ELASTIC_PASSWORD: changeme
    networks:
      - elk

  kibana:
    image: docker.elastic.co/kibana/kibana:7.4.2
    container_name: kibana
    volumes:
      - "./docker/elk/kibana/config/kibana.yml:/usr/share/kibana/config/kibana.yml"
    ports:
      - "5601:5601"
    networks:
      - elk
      - web
    depends_on:
      - elasticsearch

  prometheus:
    image: prom/prometheus:v2.30.3
    container_name: prometheus
    volumes:
      - ./docker/monitoring/prometheus:/etc/prometheus
      - prometheus_data:/prometheus
    command:
      - '--config.file=/etc/prometheus/prometheus.yml'
    ports:

      - 9090:9090
    networks:
      - monitoring

  cadvisor:
    image: gcr.io/google-containers/cadvisor:latest
    container_name: cadvisor
    ports:
      - 8080:8080
    volumes:
      - /:/rootfs:ro
      - /var/run/docker.sock:/var/run/docker.sock:rw
      - /sys:/sys:ro
      - /var/lib/docker/:/var/lib/docker:ro
    networks:
      - monitoring

  nodeexporter:
    image: prom/node-exporter
    container_name: nodeexporter
    ports:
      - '9100:9100'
    networks:
      - monitoring

  grafana:
    image: grafana/grafana:8.2.1
    container_name: grafana
    volumes:
      - gf_data:/var/lib/grafana
      - ./docker/monitoring/grafana/provisioning/dashboards:/etc/grafana/provisioning/dashboards:ro
      - ./docker/monitoring/grafana/provisioning/datasources:/etc/grafana/provisioning/datasources:ro
    environment:
      - GF_SECURITY_ADMIN_USER=${ADMIN_USER:-admin}
      - GF_SECURITY_ADMIN_PASSWORD=${ADMIN_PASSWORD:-admin}
      - GF_USERS_ALLOW_SIGN_UP=false
    ports:
      - 8080:3000
    networks:
      - monitoring
      - web
volumes:
  users_data: {}
  elasticsearch: {}
  gf_data: {}
  prometheus_data: {}
  users_log: {}
  gateway_log: {}
  mailer_log: {}
  poi_log: {}
  poi_data: {}
  route_log: {}
  trip_log: {}
  trip_manager_log: {}
  trip_data: {}
  trip_manager_data: {}
networks:
  web:
    internal: false
  users_api:
    internal: true
  users_db:
    internal: true
  mailer_api:
    internal: true
  jwt_api:
    internal: true
  poi_api:
    internal: true
  route_api:
    internal: true
  trip_api:
    internal: true
  trip_manager_api:
    internal: true
  rabbitmq:
    internal: true
  elk:
    internal: true
  monitoring:
    internal: true
