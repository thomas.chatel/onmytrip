const isFloat = n => (
	(typeof(n) === "number") ||
	(parseFloat(n).toString() === n && n !== "NaN") ) ?
		parseFloat(n) : false;

const isInteger = n => (
	(typeof(n) === "number" && n%1 === 0) ||
	(parseInt(n).toString() === n && n !== "NaN") ) ?
		parseInt(n) : false;



module.exports = {isFloat,isInteger}
