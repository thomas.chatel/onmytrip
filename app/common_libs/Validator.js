class Validator {
    fields;
    body;
    static additionalDBFields;

    constructor(body) {
        this.body = body;
    }

    validate() {
        const violations = [];
        for (const field in this.fields) {
            if (this.body[field] === undefined && (this.fields[field].required === undefined || this.fields[field].required)) {
                violations.push({
                    propertyPath: field,
                    message: "Champs '"+field+"' non spécifié"
                });
                continue;
            }
            if (this.body[field] !== undefined && typeof(this.fields[field].valid) == "function" && !this.fields[field].valid(this.body[field],this.body)) {
                violations.push({
                    propertyPath: field,
                    message: this.fields[field].msg
                });
            }
        }
        return violations.length > 0 ? violations : true;
    }

    getFieldsForDB() {
        return Object.entries(this.getFields()).reduce((acc,[field,value]) => ({
            ...acc,
            ...((this.fields[field].inDB || this.fields[field].inDB === undefined) ? {[field]: value} : {})
        }), {})
    }

    getFields() {
        return Object.entries(this.body).reduce((acc,[field,value]) => ({
            ...acc,
            ...((this.fields[field]) ?
                    {[field]: typeof(this.fields[field].format) === "function" ? this.fields[field].format(value) : value} :
                    {}
            )
        }), {})
    }

    static getAdditionalDBFields() {
        return Object.entries(this.additionalDBFields).reduce((acc,[field,func]) => ({
            [field]: func()
        }), {});
    }
}


module.exports = Validator
