const CustomError = require("./CustomError");
const logErrors = require("./logErrors");

module.exports = function logErrorsMiddleware(e,req,res,next) {
        logErrors(e);
        if (!res.headersSent)
                res.sendStatus(e instanceof CustomError ? e.code : 500);
}
