const numberOverZero = require("./numberOverZero");

module.exports = value => numberOverZero(value) && parseInt(value) <= 30