const time = require("./time");

module.exports = (value ,_) => {
	if (!time(value))
		return false;
	const [h,m] = value.split(":").map(v => +v);
	return h+m > 0;
}
