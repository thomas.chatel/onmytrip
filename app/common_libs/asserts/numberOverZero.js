const number = require("./number");

module.exports = value => number(value) && parseFloat(value) > 0
