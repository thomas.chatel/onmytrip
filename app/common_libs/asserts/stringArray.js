module.exports = (value,_) => value instanceof Array && !value.some(v => typeof(v) !== "string");
