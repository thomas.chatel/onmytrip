const time = require("./time");

module.exports = value => {
    if (!time(value))
        return false;

    const mults = [60,1];
    const nbMinutes = value.split(":").reduce(
        (acc,digit, index) => acc + parseInt(digit)*mults[index],
        0);
    return nbMinutes >= 30 && nbMinutes <= 3*60
}