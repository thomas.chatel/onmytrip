const {isInteger} = require("../converters");

module.exports = (obj,args) => {
	if (!(obj instanceof Array))
		return false;

	const nbDays = isInteger(args.nbDays) || 0;

	let allFound = false;

	for (let [key,value] of obj) {
		if (key === "all")
			allFound = true;
		if (
			(key !== "all" && (!(key = isInteger(key)) || key > nbDays)) ||
			(!(value = isInteger(value)) || value <= 0 || value > 5)
		)
			return false;
	}
	return allFound || Object.keys(obj).length === nbDays
}
