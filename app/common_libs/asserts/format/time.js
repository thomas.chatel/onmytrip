module.exports = value => {
	const [h,m] = value.split(":").map(v => +v);
	return h*3600+m*60;
}
