module.exports = obj =>
    obj.reduce((acc,[day,nbSteps]) => ({
        ...acc,
        [day]: parseInt(nbSteps)
    }), {})