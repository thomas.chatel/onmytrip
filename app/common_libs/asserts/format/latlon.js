module.exports = value => {
	const [lat,lng] = value.split(",").map(v => parseFloat(v.trim()));
	return {lat,lng};
}
