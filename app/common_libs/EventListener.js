const logger = require("./logger");
const amqplib = require("amqplib");

class EventListener {
    url;
    conn;
    ch;
    listeners;
    nbMaxTries;
    nbTries;
    connected;

    constructor(url,listeners) {
        this.url = url;
        this.listeners = listeners;
        this.nbTries = 0;
        this.nbMaxTries = 75;
        this.connected = false;
    }

    async createConnection() {
        try {
            this.conn = await amqplib.connect(this.url, "heartbeat=60");
        } catch(e) {
            if (this.nbTries === this.nbMaxTries) {
                logger.error("EventListener: Max tries to connect to RabbitMQ reached");
                return false
            }
            this.nbTries++;
            console.log("connection rabbitmq listening failed, retry ("+this.nbTries+"/"+this.nbMaxTries+") ...");
            await (() => new Promise(resolve =>
                setTimeout(() => {
                    resolve();
                }, 1000)
            ))()
            return this.createConnection();
        }
        console.log("connection rabbitmq listening successful")

        this.ch = await this.conn.createChannel();

        this.connected = true;

        return this;
    }

    async listenAll() {
        console.log("rabbitmq listening all queues")
        for (const [queue,callback] of Object.entries(this.listeners)) {
            await this.ch.assertQueue(queue, {durable: true});
            await this.ch.consume(queue, callback);
        }
    }
}


module.exports = async function listenAll(listeners) {
    const {RABBITMQ_URL: url} = process.env;

    const eventListener = new EventListener(url,listeners);
    await eventListener.createConnection();
    if (!eventListener.connected) {
        logger.error("EventListener: Can't listen events, not connected");
        return false
    }
    await eventListener.listenAll();
    return true;
}
