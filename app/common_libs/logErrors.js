const logger = require("./logger");

module.exports = e =>
    logger.error(e.message, {
        stack: e.stack.split("\n").slice(1).map(w => w.trim()),
        ...(e.data??{})
    })