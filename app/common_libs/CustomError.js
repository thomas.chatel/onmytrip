const logger = require('./logger');

class CustomError extends Error {
    constructor(eOrMessage, data = null, code = 500) {
        super();
        if (eOrMessage instanceof Error) {
            this.message = eOrMessage.message;
            this.name = eOrMessage.name;
            this.stack = eOrMessage.stack;
        } else
            this.message = eOrMessage;
        this.data = data;
        this.code = code;
        logger.error(this.message, { returnCode: this.code, stack: this.stack, method: 'CustomError'})
    }
}

module.exports = CustomError
