const { createLogger, format, transports } = require('winston');
const { combine, splat, printf } = format;

const {API_NAME} = process.env;

const myFormat = printf( ({level, message, service, stack, ...data }) => JSON.stringify({
    level: level,
    message: message,
    stack,
    data,
    service
}));

const logger = createLogger({
    level: 'info',
    defaultMeta: { service: API_NAME+'_api' },
    format: combine(
        splat(),
        myFormat
    ),
    transports: [
        new transports.Console({json: false}),
        new transports.File({ filename: "/var/log/"+API_NAME+"/"+API_NAME+".log" }),
    ]
});

module.exports = logger
