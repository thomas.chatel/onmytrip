module.exports = {
    isAuth: (req, res, next) => {
        const authHeader = req.headers.authorization;
        const token = authHeader && authHeader.split(' ')[1];
        if (!token)
            return res.sendStatus(401);
        req.user = JSON.parse(atob(token));
        next();
    },
    hasRole: (roles) => (req, res, next) => {
        if (!req.user)
            return res.sendStatus(403);

        if (!(roles instanceof Array ? roles : [roles]).some(role => req.user.roles.includes(role)))
            return res.sendStatus(403);
        next();
    }
}