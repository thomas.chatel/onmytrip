const logger = require("./logger");
const amqplib = require("amqplib");

class EventSubscriber {
    conn;
    url;
    ch;
    rkeys;
    subscribers;
    nbTries;
    nbMaxTries;
    connected;

    constructor(url,subscribers) {
        this.url = url;
        this.subscribers = subscribers;
        this.exchangeByRkeys = {};
        this.nbTries = 0;
        this.nbMaxTries = 75;
        this.connected = false;
    }

    async createConnection() {
        try {
            this.conn = await amqplib.connect(this.url, "heartbeat=60");
        } catch(e) {
            if (this.nbTries === this.nbMaxTries) {
                logger.error("EventSubscriber: Max tries to connect to RabbitMQ reached");
                return false
            }
            this.nbTries++;
            logger.info("EventSubscriber: connection rabbitmq subcribing failed, retry ("+this.nbTries+"/"+this.nbMaxTries+") ...");
            await (() => new Promise(resolve =>
                setTimeout(() => {
                    resolve();
                }, 1000)
            ))()
            return this.createConnection();
        }
        logger.info("EventSubscriber: connection rabbitmq subcribing successful")
        this.ch = await this.conn.createChannel();

        for (const [exchange_name,route_keys] of Object.entries(this.subscribers)) {
            await this.ch.assertExchange(exchange_name, 'direct', {durable: true});
            for (const [route_key,queues] of Object.entries(route_keys)) {
                if (this.exchangeByRkeys[route_key] !== undefined) {
                    throw new Error("You can't define the same route key any times");
                }
                this.exchangeByRkeys[route_key] = exchange_name
                for (const queue of queues) {
                    await this.ch.assertQueue(queue, {durable: true});
                    await this.ch.bindQueue(queue, exchange_name, route_key);
                }
            }
        }
        this.connected = true;
        return this;
    }

    async publish(rkey,msg) {
        if (!this.connected)
            return logger.error("EventSubscriber: Can't send event, not connected")
        if (typeof(msg) === "object") {
            msg = JSON.stringify(msg);
        }
        if (this.exchangeByRkeys[rkey] === undefined) {
            throw new Error("The route key '"+rkey+"' does not exist");
        }
        await this.ch.publish(this.exchangeByRkeys[rkey], rkey, Buffer.from(msg));
    }
}

const {RABBITMQ_URL} = process.env;
let instance = null;

function setEventSubscriberInstance(subscribers) {
    if (!subscribers)
        throw new Error("You need to specify subscribers to init eventSubscriber");
    instance = new EventSubscriber(RABBITMQ_URL,subscribers);
    return instance.createConnection();
}

async function getEventSubscriberInstance(subscribers = null) {
    if (instance === null) {
        if (!subscribers)
            throw new Error("You need to specify subscribers to init eventSubscriber");
        await setEventSubscriberInstance(subscribers);
    }
    return instance;
}

async function publishEvent(rkey,msg, subscribers = null) {
    const eventSubscriber = await getEventSubscriberInstance(subscribers);
    await eventSubscriber.publish(rkey,msg);
}

module.exports = {getEventSubscriberInstance, setEventSubscriberInstance, publishEvent}
