const request = require("./request");
const logger = require("./logger");

const isAuth = async (req, res, next) => {
    request(`${process.env.JWT_API_HOST}/token`, {
        method: 'get',
        port: 3000,
        headers: {
            'authorization': (req.headers.authorization || "")
        }
    })
        .then(async response => {
            if(response && response.body !== "Unauthorized") {
                req.user = JSON.parse(response.body);
                next();
            } else {
                logger.warn(undefined, { returnCode: 401, method: 'isAuth'})
                return res.sendStatus(401);
            }
        })
}

const hasRole = (roles,getUser = null) => async (req,res,next) => {
    if (!(roles instanceof Array))
        roles = [roles]
    const user = req.user ? (getUser ? await getUser(req.user.id) : req.user) : null
    if (!user || !roles.some(role => user.roles.includes(role))) {
        logger.warn(undefined, { returnCode: 403, method: 'hasRole'})
        return res.sendStatus(403);
    }
    next();
}

module.exports = { isAuth, hasRole }
