function jsonToUrlEncoded(obj) {
    return Object.entries(obj).map(([key,value]) => encodeURIComponent(key)+"="+encodeURIComponent(value)).join("&");
}

module.exports = jsonToUrlEncoded;