const express = require("express");
const cors = require("cors");
const {isFloat} = require("./common_libs/converters");
const {getRoute} = require("./libs/APIConnector");
const logger = require("./common_libs/logger");
const request = require("./common_libs/request");
const jsonToUrlEncoded = require("./common_libs/jsonToUrlEncoded");
const logErrorsMiddleware = require("./common_libs/logErrorsMiddleware");
const CustomError = require("./common_libs/CustomError");
const {isAuth} = require("./common_libs/security");

let {DEFAULT_TRANSPORT_MODE, TRANSPORT_MODES, GET_CITY_URL} = process.env;
TRANSPORT_MODES = TRANSPORT_MODES.split(",");

const app = express();

app.use(cors());
app.use(express.json());

app.get("/route", (req,res,next) => (async (req, res) => {
    let {lat1, lng1, lat2, lng2 } = req.query;
    const mode = req.query.mode??DEFAULT_TRANSPORT_MODE

    if (
        !(lat1 = isFloat(lat1)) ||
        !(lng1 = isFloat(lng1)) ||
        !(lat2 = isFloat(lat2)) ||
        !(lng2 = isFloat(lng2)) ||
        !TRANSPORT_MODES.includes(mode))
        return res.sendStatus(400);

    let routeRes;
    if (!(routeRes = await getRoute(lat1,lng1,lat2,lng2,mode)))
        return res.sendStatus(500);

    res.json(routeRes);
})(req,res).catch(e => next(new CustomError(e, {...req.query}))));

app.get("/city/:code", (req, res) => {
    const fields = ['contour','centre'];
    request(`${GET_CITY_URL}/${req.params.code}?${jsonToUrlEncoded({
        fields
    })}`)
        .then(({body,status}) => {
            if (status !== 200) {
                logger.error("Failed fetching city details", { returnCode: status, method: 'getCityByCode'});
                return res.sendStatus(status);
            }
            logger.info(undefined, { returnCode: 200, method: 'getCityByCode'});
            res.json(Object.entries(JSON.parse(body)).reduce((acc,[key,value]) => ({
                ...acc,
                [key]: fields.includes(key) ? value : undefined
            }), {}));
        }).catch(e => new CustomError(e, {code: req.params.code}))
})

app.get("/city/:code/name", (req, res) => {
    request(`${GET_CITY_URL}/${req.params.code}?${jsonToUrlEncoded({
        fields: ['nom']
    })}`)
        .then(({body,status}) => {
            if (status !== 200) {
                logger.error("Failed fetching city details", { returnCode: status, method: 'getCityByCodeAndName'});
                return res.sendStatus(status);
            }
            logger.info(undefined, { returnCode: 200, method: 'getCityByCodeAndName'});
            res.json(JSON.parse(body).nom);
        }).catch(e => new CustomError(e, {code: req.params.code}))
})

app.use(isAuth);

app.get('/city', (req, res, next) => {
    const { nom, code } = req.query
    if (!nom && !code) {
        logger.error(undefined, { user: req.user.id, returnCode: 400, method: 'getCity'});
        return res.sendStatus(400);
    }
    const params = {
        fields: ['nom','code','centre'],
        boost: "population",
        limit: 5
    }
    Promise.all([
        (nom ? request(`${GET_CITY_URL}?${jsonToUrlEncoded({
            ...params,
            nom
        })}`).then(({body}) => JSON.parse(body)) : []),
        (code ? request(`${GET_CITY_URL}?${jsonToUrlEncoded({
            ...params,
            code
        })}`).then(({body}) => JSON.parse(body)) : [])
    ]).then(responses => {
        let datas = [];
        for (const response of responses)
            datas = [...datas, ...response];

        if (datas.length === 0) {
            logger.warn(undefined, { user: req.user.id, returnCode: 404, method: 'getCity'});
            return res.sendStatus(404);
        }

        logger.info(undefined, { user: req.user.id, returnCode: 200, method: 'getCity'});
        return res.status(200).send(datas);
    }).catch(e => next(new CustomError(e, {nom,code})))
});

app.use(logErrorsMiddleware);


app.listen(process.env.PORT ?? 3000, () => logger.info(`Route api server started and listening on port ${process.env.PORT ?? 3000}`));
