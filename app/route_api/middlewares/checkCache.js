const cache = require("../libs/RedisClient");
const logger = require("../libs/logger");

const checkCache = async (req, res, next) => {
    const { search } = req.query;
    logger.info(`searching cities for ${search}`, { method: 'checkCache' })
    await cache.connect();
    cache.get(search, (err, data) => {
        if (!err && data != null) {
            logger.info(`retrieving data from cache for city ${search}`, { method: 'checkCache' });
            res.status(200).send(JSON.parse(data));
        } else next();
    });
}

module.exports = checkCache;