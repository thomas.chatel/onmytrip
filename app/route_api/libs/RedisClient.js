const redis = require("redis");

const cache = redis.createClient({
    host: 'city_cache',
    port: 6379
})

module.exports = cache;