const request = require("../common_libs/request");
const logger = require("../common_libs/logger");
const jsonToUrlEncoded = require("../common_libs/jsonToUrlEncoded");

const {API_KEY, GET_ROUTE_URL} = process.env;

async function getRoute(lat1,lng1,lat2,lng2,mode) {
    const {body,status} = await request(GET_ROUTE_URL+"?"+jsonToUrlEncoded({
        waypoints: `${lat1},${lng1}|${lat2},${lng2}`,
        mode,
        apiKey: API_KEY
    }));

    if (status === 401) {
        logger.error("Connection denied to the route API", { errorCode: 401 });
        return false;
    }

    if (status !== 200) {
        logger.error(`Unknown error (status: ${status})\n${body}`, { errorCode: status });
        return false;
    }

    return JSON.parse(body);
}

module.exports = {getRoute};
