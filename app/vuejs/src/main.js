import Vue from "vue"
import App from "./App.vue"
import router from "./router"
import "./index.css"
import { library } from "@fortawesome/fontawesome-svg-core"
import Field from "@/components/Form/Field"
import Vuemik from "@/components/Form/Vuemik"
import propAccess from "@/libs/propAccess";
import {
  faLock,
  faCheck,
  faTimes,
  faSignInAlt,
  faUserEdit,
  faRoad,
  faClipboard,
  faSignOutAlt,
  faCarSide,
  faHome,
  faAddressBook,
  faUserCircle,
  faBars,
  faTimesCircle,
  faAngleDown,
  faAngleRight,
  faMapPin,
  faRoute,
  faPlus,
  faTrash,
  faUser,
  faEye,
  faSave,
  faCheckCircle,
  faBed,
  faUtensils,
  faEdit,
  faWindowClose,
  faBan,
  faDoorOpen
} from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome"

library.add(
  faLock,
  faCheck,
  faTimes,
  faSignInAlt,
  faUserEdit,
  faRoad,
  faClipboard,
  faSignOutAlt,
  faCarSide,
  faHome,
  faAddressBook,
  faUserCircle,
  faBars,
  faTimesCircle,
  faAngleDown,
  faAngleRight,
  faMapPin,
  faRoute,
  faPlus,
  faTrash,
  faUser,
  faEye,
  faSave,
  faCheckCircle,
  faBed,
  faUtensils,
  faEdit,
  faWindowClose,
  faBan,
  faDoorOpen
)
Vue.component("font-awesome-icon", FontAwesomeIcon)
Vue.component("Field", Field)
Vue.component("Vuemik", Vuemik)
Vue.config.productionTip = false

Vue.directive("click-outside", {
  bind(el, binding, vnode) {
    el.clickOutsideEvent = (event) => {
      if (!(el === event.target || el.contains(event.target))) {
        propAccess(vnode.context,binding.expression)(event);
      }
    };
    document.body.addEventListener("click", el.clickOutsideEvent);
  },
  unbind(el) {
    document.body.removeEventListener("click", el.clickOutsideEvent);
  },
});

router.beforeEach((to, from, next) => {
  const nearestWithTitle = to.matched
    .slice()
    .reverse()
    .find(r => r.meta && r.meta.title)
  const previousNearestWithMeta = from.matched
    .slice()
    .reverse()
    .find(r => r.meta && r.meta.title)

  if (nearestWithTitle) {
    document.title = nearestWithTitle.meta.title
  } else if (previousNearestWithMeta) {
    document.title = previousNearestWithMeta.meta.title
  }
  next()
})

new Vue({
  render: h => h(App),
  router
}).$mount("#app")
