import Vue from "vue"
import VueRouter from "vue-router"
import isAuthenticated from "@/middlewares/isAuthenticated";
import hasRole from "@/middlewares/hasRole";

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "accueil",
    meta: {
      title: "Own My Trip - Accueil"
    },
    component: () => import("@/views/HomeView")
  },
  {
    path: "/login",
    name: "connexion",
    meta: {
      title: "Own My Trip - Connexion"
    },
    component: () => import("@/views/LoginView")
  },
  {
    path: "/register",
    name: "inscription",
    meta: {
      title: "Own My Trip - Inscription"
    },
    component: () => import("@/views/RegisterView")
  },
  {
    path: "/confirmAccount/:token",
    name: "confirmation",
    meta: {
      title: "Own My Trip - Confirmation"
    },
    component: () => import("@/views/ValidateView")
  },
  {
    path: "/forgot-password",
    name: "oubli-mot-de-passe",
    meta: {
      title: "Own My Trip - Oubli Mot de passe"
    },
    component: () => import("@/views/ForgotPasswordView")
  },
  {
    path: "/reset_password/:token",
    name: "reset-password",
    meta: {
      title: "Own My Trip - Oubli Mot de passe"
    },
    component: () => import("../views/ResetPasswordView")
  },
  {
    path: "/edit-profile",
    name: "mon-profil",
    meta: {
      title: "Own My Trip - Mon profil"
    },
    component: () => import("../views/EditProfileView"),
    beforeEnter: isAuthenticated
  },
  {
    path: "/roadtrip",
    name: "recherche-roadtrip-base",
    component: () => import("@/views/roadtrip/RoadTripBase"),
    beforeEnter: isAuthenticated,
    children: [
      {
        path: "/",
        name: "recherche-roadtrip",
        meta: {
          title: "Own My Trip - Créer mon roadtrip"
        },
        component: () => import("@/views/roadtrip/RoadTripView")
      },
      {
        path: "AtoB",
        name: "recherche-roadtrip-AtoB",
        meta: {
          title: "Own My Trip - Créer mon roadtrip"
        },
        component: () => import("@/views/roadtrip/forms/AtoBForm")
      },
      {
        path: "AtoB/step",
        name: "recherche-roadtrip-AtoB-step",
        meta: {
          title: "Own My Trip - Créer mon roadtrip"
        },
        component: () => import("@/views/roadtrip/forms/AtoBStepForm")
      },
      {
        path: "result",
        name: "recherche-roadtrip-result",
        meta: {
          title: "Own My Trip - Voir mon roadtrip"
        },
        component: () => import("@/views/roadtrip/ResultView")
      },
      {
        path: "CityTour",
        name: "recherche-roadtrip-CityTour",
        meta: {
          title: "Own My Trip - Créer mon city tour"
        },
        component: () => import("@/views/roadtrip/forms/CityTourForm")
      },
      {
        path: "CityTour/step",
        name: "recherche-roadtrip-CityTour-step",
        meta: {
          title: "Own My Trip - Créer mon city tour"
        },
        component: () => import("@/views/roadtrip/forms/CityTourStepForm")
      },
    ]
  },
  {
    path: "/roadtrips",
    name: "roadtrips-base",
    beforeEnter: isAuthenticated,
    component: () => import("@/views/roadtripsGestions/GestionRoadTripsBase"),
    children: [
      {
        path: "/",
        name: "roadtrips",
        meta: {
          title: "Own My Trip - Voir mes road trips"
        },
        component: () => import("@/views/roadtripsGestions/GestionRoadTrips")
      },
      {
        path: "admin",
        name: "roadtrips-admin",
        meta: {
          title: "Own My Trip - Voir tout les road trips"
        },
        beforeEnter: hasRole(['ADMIN','SUPER_ADMIN']),
        component: () => import("@/views/roadtripsGestions/GestionRoadTrips")
      },
      {
        path: "admin/user/:id",
        name: "roadtrips-other-user",
        meta: {
          title: "Own My Trip - Voir les road trips d'un autre utilisateur"
        },
        beforeEnter: hasRole(['ADMIN','SUPER_ADMIN']),
        component: () => import("@/views/roadtripsGestions/GestionRoadTrips")
      },
      {
        path: ":id",
        name: "show-roadtrip",
        meta: {
          title: "Own My Trip - Visionner un road trip"
        },
        component: () => import("@/views/roadtripsGestions/ShowRoadTrip")
      }
    ]
  },
  {
    path: '/admin-dashboard',
    name: "dashboard-admin-base",
    beforeEnter: hasRole("SUPER_ADMIN"),
    component: () => import("@/views/adminDashboard/AdminDashboardBase"),
    children: [
      {
        path: "/",
        name: "dashboard-admin",
        meta: {
          title: "Own My Trip - Dashboard administrateur"
        },
        component: () => import("@/views/adminDashboard/AdminDashboard")
      }
    ]
  }
]

const router = new VueRouter({
  mode: "history",
  routes
})

export default router
