export function getViolations(violationsList, violationState, aliases = null) {
  clearViolations(violationState)
  for (const { propertyPath, message } of violationsList) {
    if (aliases && aliases[propertyPath])
      for (const alias of (aliases[propertyPath] instanceof Array ? aliases[propertyPath] : [aliases[propertyPath]])) {
        if (violationState[alias])
          violationState[alias].push(message)
      }
    if (violationState[propertyPath] === undefined) continue
    violationState[propertyPath].push(message)
  }
}

export function clearViolations(violationState) {
  for (let key in violationState) {
    violationState[key] = []
  }
}
