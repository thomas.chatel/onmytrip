export default function propAccess(obj, key) {
    const keyArray = key instanceof Array ? key : key.split(".");

    if (keyArray.length === 0 || obj === undefined)
        return obj;

    return propAccess(obj[keyArray[0]], keyArray.splice(1));
}