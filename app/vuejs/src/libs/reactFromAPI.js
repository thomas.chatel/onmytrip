import { clearViolations, getViolations } from "@/libs/formViolations"
import { getErrorMessage } from "@/libs/errorMessages"

export const reactFromAPIDelete = (
  res,
  violations,
  createAlert,
  successMessage = null,
  timeout = 5000,
  aliases = null
) => reactFromAPI(res, violations, createAlert, successMessage, 204, timeout, aliases)

export const reactFromAPIPost = (
  res,
  violations,
  createAlert,
  successMessage = null,
  timeout = 5000,
  aliases = null
) => reactFromAPI(res, violations, createAlert, successMessage, 201, timeout, aliases)

export const reactFromAPIPut = (
  res,
  violations,
  createAlert,
  successMessage = null,
  timeout = 5000,
  aliases = null
) => reactFromAPI(res, violations, createAlert, successMessage, 200, timeout, aliases)

export const reactFromAPI = async (res,violations,createAlert,successMessage,successCode, timeout = 5000, aliases = null) => {
    let json;
    switch (res.status) {
        case successCode:
            clearViolations(violations);
            if (successMessage !== null) {
                createAlert({
                    color: "green",
                    text: successMessage,
                    icon: "check"
                }, timeout);
            }
            return true;
        case 422:
            json = await res.json();
            getViolations(json.violations, violations, aliases);
            return false;
        default:
            createAlert({
                color: "red",
                text: await getErrorMessage(res),
                icon: "times"
            });
            return false;
    }
}
