const siteUrl = `http://${window.location.hostname}:8081`;

let sessionSetter;

export const setSessionSetter = aSessionSetter => {
    sessionSetter = aSessionSetter;
}

export default function request(url,params = {}) {
    params.headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        ...(params.headers||{}),
    }
    const session = JSON.parse(localStorage.getItem("currentUser"));
    url = url.replace(/{( )*[a-zA-Z0-1]+( )*}/i, function (match) {
        return session ? session[match.replace("{","").replace("}","")]??match : match
    });
    return fetch(url,params)
        .then(async res => {
            if (res.status === 401 && url.split("/")[3] !== "login" && session) {
                try {
                    const refreshToken = session.refresh_token;
                    if (refreshToken && refreshToken !== '') {
                        const res = await fetch(siteUrl+'/refresh_token', {
                            method: 'post',
                            headers: {
                                'authorization': `Bearer ${refreshToken}`
                            }
                        });

                        if (res.status > 199 && res.status < 300) {
                            const data = await res.json();
                            localStorage.setItem("currentUser", JSON.stringify({
                                ...session,
                                access_token: data.access_token
                            }));
                            sessionSetter();
                            params.headers.authorization = `Bearer ${data.access_token}`;
                            const originalResponse = await fetch(url, params);
                            if (originalResponse.status !== 401) {
                                return originalResponse;
                            }
                        }
                    }
                    localStorage.removeItem("currentUser");
                    location.href = "/";
                } catch (e) {
                    localStorage.removeItem("currentUser");
                    location.href = "/";
                }
            } else {
                return res;
            }
        });
}
