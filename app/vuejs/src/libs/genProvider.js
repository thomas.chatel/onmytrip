export function provideMethods(provider, providerName, app) {
  if (provider[providerName] === undefined) provider[providerName] = {}

  for (const key in app) {
    if (
      typeof app[key] == "function" &&
      key[0] !== "$" &&
      key[0] !== "_" &&
      key !== "constructor"
    )
      provider[providerName][key] = app[key]
  }
}

export function provideDatas(provider, providerName, app, writing = false) {
  if (provider[providerName] === undefined) provider[providerName] = {}

  for (const key in app) {
    if (typeof app[key] !== "function" && key[0] !== "$" && key[0] !== "_")
      Object.defineProperty(provider[providerName], key, {
        enumerable: true,
        get: () => app[key],
        ...(writing ? {set: value => {
            app[key] = value
          } } : {})
      })
  }
}
