const errorMessages = {
  general: {
    400: "Requête malformée",
    403: "Accès interdit !",
    404: "Ressource introuvable",
    401: "Vous devez être connecté",

    500: "Erreur interne"
  },

  "/account/login": {
    401: "Echec de la connexion",
    423: "Vous devez valider votre compte"
  },

  "/account/register": {
    409: "Email déjà utilisé"
  },
  "/AtoB": {
    404: "Route introuvable, ou aucune étape trouvée, essayez de changer les points sur la carte",
    413: "Echec du calcul du roadtrip, veuillez réessayer",
    409: "Votre voyage ne peut pas être effectué dans les temps indiqués"
  },
  "/tourCity": {
    404: "Aucune étape trouvée ici",
    413: "Echec du calcul du city tour, veuillez réessayer",
    409: "Votre voyage ne peut pas être effectué dans les temps indiqués"
  }
}

export async function getErrorMessage(res) {
  let json;
  try {
    json = await res.json()
  } catch (_) {
    json = null
  }
  if (json && json.detail) {
    return json.detail
  }

  const url =
    "/" +
    res.url
      .split("/")
      .slice(3)
      .join("/")

  const code = res.status

  return (errorMessages[url] && errorMessages[url][code])
    ? errorMessages[url][code]
    : (errorMessages.general[code] ?? "Une erreur est survenue")
}
