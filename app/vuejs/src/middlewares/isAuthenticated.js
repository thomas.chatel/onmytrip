export default (to, from, next) => {
    if (localStorage.getItem("currentUser") === null)
        next({name: "connexion"})
    else
        next();
}