export default roles => (to, from, next) => {
    if (localStorage.getItem("currentUser") === null) {
        next({name: "connexion"});
        return;
    }
    if (!(roles instanceof Array))
        roles = [roles];
    const userRoles = JSON.parse(localStorage.getItem("currentUser")).roles;
    if (!roles.some(role => userRoles.includes(role))) {
        next({name: "accueil"});
        return;
    }
    next();
}
