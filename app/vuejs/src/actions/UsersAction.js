import request from "@/libs/request";

const url = `${process.env.VUE_APP_BACKEND_URL}/users`;

export const findUsers = (page = 1, search = null) =>
	request(url+(search ? '/search/'+search : '')+"?page="+page, {
		headers: localStorage.getItem('currentUser') ?
			{ authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
			{}
	})

export const getUserById = id =>
	request(url+"/"+id, {
		headers: localStorage.getItem('currentUser') ?
			{ authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
			{}
	})
