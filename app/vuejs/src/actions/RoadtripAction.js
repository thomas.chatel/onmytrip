import request from "@/libs/request";

const url = `${process.env.VUE_APP_BACKEND_URL}`;

export const city = search =>
    request(url+"/city?nom="+search+"&code="+search, {
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    })


export const AToB = values =>
    request(url+"/AtoB", {
        method: "POST",
        body: JSON.stringify(values),
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    })

export const cityTour = values =>
    request(url+"/tourCity", {
        method: "POST",
        body: JSON.stringify(values),
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    })