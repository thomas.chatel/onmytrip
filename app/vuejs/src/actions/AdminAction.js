import request from "@/libs/request";

const url = `${process.env.VUE_APP_BACKEND_URL}/admin`;

export const addAdminRole = id =>
    request(url+"/add/"+id, {
        method: "PATCH",
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    })

export const removeAdminRole = id =>
    request(url+"/remove/"+id, {
        method: "PATCH",
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    })