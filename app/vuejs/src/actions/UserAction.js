import request from "@/libs/request";

const url = `${process.env.VUE_APP_BACKEND_URL}/account`;

export const login = values =>
  fetch(url + "/login", {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    method: "POST",
    body: JSON.stringify(values)
  })

export const register = values =>
  fetch(url + "/register", {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    method: "POST",
    body: JSON.stringify(values)
  })

export const confirmAccount = token =>
  fetch(url + "/confirm/" + token, {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    method: "PATCH"
  })

export const forgotPassword = email =>
    request(`${url}/forgot_password`, {
        method: "PUT",
        body: JSON.stringify({email})
    })

export const changePassword = (values,token = null) =>
    request(`${url}/change_password${token ? '/'+token : ''}`, {
        method: "PUT",
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {},
        body: JSON.stringify(values)
    })

export const updateUser = values =>
    request(url, {
      method: "PUT",
      headers: localStorage.getItem('currentUser') ?
          { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
          {},
      body: JSON.stringify(values)
    })
