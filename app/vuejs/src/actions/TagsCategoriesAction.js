import request from "@/libs/request";

const url = `${process.env.VUE_APP_BACKEND_URL}`;

export const getTagsTranslations = () =>
    request(url + "/tags/translations", {
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    })

export const getTags = () =>
    request(url + "/tags", {
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    }).then(res => res.json())

export const searchTags = search =>
    request(url + "/tags/search/" + search, {
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    }).then(res => res.json())


export const getRestaurantTags = () =>
    request(url + "/tags/restaurant", {
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    }).then(res => res.json())

export const searchRestaurantTags = search =>
    request(url + "/tags/restaurant/search/" + search, {
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    }).then(res => res.json())

export const getAllHostelCategories = () =>
    request(url + "/hostel_categories", {
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    }).then(res => res.json())

export const searchHostelCategories = search =>
    request(url + "/hostel_categories/"+search, {
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    })
        .then(res => res.json())
        .then(hostels => hostels.map(({id}) => id))

// Mock preferences in waiting for API

export const getPreferences = () =>
    request(url + "/preferences/{id}/pois", {
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    })

export const savePreferences = (preferences) =>
    request(url + "/preferences/{id}/pois", {
        method: "PUT",
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {},
        body: JSON.stringify({
            preferences
        })
    })

export const getRestaurantPreferences = () =>
    request(url + "/preferences/{id}/restaurants", {
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    })

export const saveRestaurantPreferences = (preferences) =>
    request(url + "/preferences/{id}/restaurants", {
        method: "PUT",
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {},
        body: JSON.stringify({
            preferences
        })
    })

export const getHostelPreferences = () =>
    request(url + "/preferences/{id}/hostels", {
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    })

export const saveHostelPreferences = (preferences) =>
    request(url + "/preferences/{id}/hostels", {
        method: "PUT",
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {},
        body: JSON.stringify({
            preferences
        })
    })
