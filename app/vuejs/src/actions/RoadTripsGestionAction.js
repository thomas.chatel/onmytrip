import request from "@/libs/request";

const url = `${process.env.VUE_APP_BACKEND_URL}`;


export const getMineRoadTrips = () =>
    request(url + "/trip_manager/user/{id}", {
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    })

export const getAllRoadTrips = () =>
    request(url + "/trip_manager/", {
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    })

export const getRoadTripOfAnotherUser = userId =>
    request(url + "/trip_manager/user/"+userId, {
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    })

export const getOneRoadTrip = id =>
    request(url + "/trip_manager/"+id, {
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    })

export const deleteRoadTrip = id =>
    request(url + "/trip_manager/"+id, {
        method: "DELETE",
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    })

export const saveRoadTrip = data =>
    request(url+"/trip_manager", {
        method: "POST",
        body: JSON.stringify(data),
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    })

export const updateRoadTripName = (id,name) =>
    request(url + "/trip_manager/"+id, {
        method: "PATCH",
        body: JSON.stringify({name}),
        headers: localStorage.getItem('currentUser') ?
            { authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).access_token }` } :
            {}
    })