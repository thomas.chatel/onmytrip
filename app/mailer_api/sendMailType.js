const subjectByEmailType = require("./subjectByEmailType");
const Mailer = require("./Mailer");
const logger = require("./common_libs/logger");

module.exports = function sendMailType(type, recipients, params) {
	if (!(recipients instanceof Array))
		recipients = [recipients];

	const mailer = new Mailer();
	mailer.setType(type);
	mailer.setSubject(subjectByEmailType[type]);
	for (const recipient of recipients) {
		mailer.addDestinations(recipient.trim());
	}
	mailer.setParams(params, true)

	mailer.send().then(success => {
		if (success) {
			logger.info("Success send mail (recipients: '" + recipients.join(", ") + "'; type: '" + type + "')");
		} else {
			logger.error("Failed send mail (recipients: '" + recipients.join(", ") + "'; type: '" + type + "')");
		}
	});
}
