const { initListeners } = require("./listeners")
const logger = require("./common_libs/logger")

initListeners().then(res => {
  if (res) console.log("RabbitMq eventlistener initialized")
})
