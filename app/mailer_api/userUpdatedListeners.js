const {
	USER_PASSWORD_UPDATED_QUEUE,
	USER_ROLES_UPDATED_QUEUE,
	USER_NAMES_UPDATED_QUEUE
} = process.env;
const sendMailType = require("./sendMailType");

function checkQueue(msg,cols) {
	if (msg.fields.redelivered)
		return false;

	const {fields,attributes,user} = JSON.parse(msg.content.toString());

	return (cols instanceof Array ? cols : [cols]).some(col => fields.includes(col)) ? {fields,attributes,user} : false;
}

module.exports = {
	[USER_PASSWORD_UPDATED_QUEUE]: msg => {
		const datas = checkQueue(msg,'password');
		if (!datas)
			return;

		const {user} = datas;
		sendMailType('userPasswordUpdated',user.email, user);
	},
	[USER_ROLES_UPDATED_QUEUE]: msg => {
		const datas = checkQueue(msg,'roles');
		if (!datas)
			return;
		const {attributes,user} = datas;
		sendMailType(attributes.roles.includes('ADMIN') ? 'addedToAdmins': 'removedFromAdmins',user.email, user)
	},
	[USER_NAMES_UPDATED_QUEUE]: msg => {
		const datas = checkQueue(msg,['firstname','lastname','gender']);
		if (!datas)
			return;
		const {fields,user} = datas;
		const fieldsNames = {
			firstname: "prénom",
			lastname: "nom",
			gender: "genre"
		}
		sendMailType('userNamesUpdated',user.email, {
			...user,
			fields: fields
				.reduce((acc,key) => [
					...acc,
					...(fieldsNames[key] ? [fieldsNames[key]] : [])
				] , [])
		})
	}
}
