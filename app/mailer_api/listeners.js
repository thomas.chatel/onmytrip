const {
    USER_CREATED_QUEUE,
    USER_FORGOT_PASSWORD_QUEUE,
    ROADTRIP_DELETED_QUEUE
} = process.env;
const listenAll = require("./common_libs/EventListener");
const sendMailType = require("./sendMailType");
const userUpdatedListener = require("./userUpdatedListeners");


const listeners = {
    [USER_CREATED_QUEUE]: (msg) => {
        if (msg.fields.redelivered)
            return;

        const {user,referer} = JSON.parse(msg.content.toString());

        const recipient = user.email;
        const params = {
            ...user,
            referer
        }

        sendMailType("confirmAccount", recipient, params);
    },
    [USER_FORGOT_PASSWORD_QUEUE]: (msg) => {
        if (msg.fields.redelivered)
            return;

        const {user,referer} = JSON.parse(msg.content.toString());

        const recipient = user.email;
        const params = {
            ...user,
            referer
        }
        sendMailType("forgotPassword", recipient, params);
    },
    [ROADTRIP_DELETED_QUEUE]: (msg) => {
        if (msg.fields.redelivered)
            return;

        const {user,roadtrip,deleterId} = JSON.parse(msg.content.toString());

        if (deleterId !== roadtrip.userId)
            sendMailType("roadTripDeletedByAdmin", user.email, {
                ...user,
                roadtrip: roadtrip.roadtrip
            });
    },
    ...userUpdatedListener
}

function initListeners() {
    return listenAll(listeners);
}

module.exports = {initListeners};
