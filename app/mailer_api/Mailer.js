const nodemailer = require("nodemailer");
const templater = require("./templater");

const { SMTP_HOST, SMTP_PORT, SMTP_SECURE, SMTP_USER, SMTP_PASSWORD, MAIL_NAME } = process.env;

module.exports = class Mailer {
  host;
  port;
  secure;

  user;
  password;

  destinations;
  subject;
  fromName;
  fromEmail;

  params;
  type;

  constructor() {
    this.host = SMTP_HOST;
    this.port = SMTP_PORT;
    this.secure = SMTP_SECURE === 'true';

    this.user = SMTP_USER;
    this.password = SMTP_PASSWORD;

    this.destinations = [];
    this.subject = "";
    this.fromName = MAIL_NAME;
    this.fromEmail = SMTP_USER;

    this.type = null;
    this.params = {};
  }

  async send() {
    let transporter = nodemailer.createTransport({
      host: this.host,
      port: this.port,
      secure: this.secure, // true for 465, false for other ports
      auth: {
        user: this.user, // generated ethereal user
        pass: this.password, // generated ethereal password
      },
    });
    const templated = await templater(this.type, this.params);
    const sended = await transporter.sendMail({
      from: '"'+this.fromName+'" <'+this.fromEmail+'>', // sender address
      to: this.destinations.join(", "), // list of receivers
      subject: this.subject, // Subject line
      text: templated, // plain text body
      html: templated, // html body
    });
    return sended.response.split(" ").slice(0,3).join(" ") == "250 2.0.0 OK";
  }


  addDestinations(destination) {
    this.destinations.push(destination);
  }

  setSubject(subject) {
    this.subject = subject;
  }

  setType(type) {
    this.type = type;
  }

  setParams(params,erase = false) {
    this.params = erase ? params : {...this.params, ...params};
  }

  setFromName(fromName) {
    this.fromName = fromName;
  }

  setFromEmail(fromEmail) {
    this.fromEmail = fromEmail;
  }
}
