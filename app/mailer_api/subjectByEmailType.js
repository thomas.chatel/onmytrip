module.exports =  {
  confirmAccount: "OwnMyTrip - Veuillez confirmer votre compte",
  forgotPassword: "OwnMyTrip - Réinitialisation",
  addedToAdmins: "OwnMyTrip - Vous êtes administrateur",
  removedFromAdmins: "OwnMyTrip - Vous n'êtes plus administrateur",
  userNamesUpdated: "OwnMyTrip - Données mises à jour",
  userPasswordUpdated: "OwnMyTrip - Mot de passe mis à jour",
  roadTripDeletedByAdmin: "OwnMyTrip - Road trip supprimé"
}
