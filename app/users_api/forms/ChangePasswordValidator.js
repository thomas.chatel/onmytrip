const Validator = require("../common_libs/Validator");

const password = require("../common_libs/asserts/password");
const confirm_password = require("../common_libs/asserts/confirm_password");

module.exports = class ChangePasswordValidator extends Validator {
    constructor(body) {
        super(body);

        this.fields = {
            password: {
                valid: password,
                msg: "Votre mot de passe n'est pas assez complexe"
            },
            confirm_password: {
                valid: confirm_password,
                msg: "Les deux mots de passe ne correspondent pas",
                inDB: false
            }
        }
    }
}
