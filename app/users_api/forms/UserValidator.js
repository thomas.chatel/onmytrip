const generateToken = require("../libs/generateToken");
const Validator = require("../common_libs/Validator");

const email = require("../common_libs/asserts/email");
const password = require("../common_libs/asserts/password");
const confirm_password = require("../common_libs/asserts/confirm_password");
const gender = require("../common_libs/asserts/gender");

module.exports = class UserValidator extends Validator {
    constructor(body) {
        super(body);

        this.fields = {
            email: {
                valid: email,
                msg: "Adresse mail invalide",
            },
            password: {
                valid: password,
                msg: "Votre mot de passe n'est pas assez complexe"
            },
            confirm_password: {
                valid: confirm_password,
                msg: "Les deux mots de passe ne correspondent pas",
                inDB: false
            },
            firstname: {
                valid: (value,_) => value.length >= 2 && value.length <= 30,
                msg: "Le prénom doit faire entre 2 et 30 caractères"
            },
            lastname: {
                valid: (value,_) => value.length >= 2 && value.length <= 30,
                msg: "Le nom doit faire entre 2 et 30 caractères"
            },
            gender: {
                valid: gender,
                format: (value) => ({
                    'H': true,
                    'F': false,
                    'A': null
                })[value],
                msg: "Genre spécifié incorrect"
            },
        }
    }

    static additionalDBFields = {
        registerToken: generateToken 
    }
}
