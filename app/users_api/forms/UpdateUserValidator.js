const Validator = require("../common_libs/Validator");
const gender = require("../common_libs/asserts/gender");

module.exports = class UserValidator extends Validator {
    constructor(body) {
        super(body);

        this.fields = {
            firstname: {
                valid: (value,_) => value.length >= 2 && value.length <= 30,
                msg: "Le prénom doit faire entre 2 et 30 caractères"
            },
            lastname: {
                valid: (value,_) => value.length >= 2 && value.length <= 30,
                msg: "Le nom doit faire entre 2 et 30 caractères"
            },
            gender: {
                valid: gender,
                format: (value) => ({
                    'H': true,
                    'F': false,
                    'A': null
                })[value],
                msg: "Genre spécifié incorrect"
            },
        }
    }
}
