const app = require("../app");
const request = require("supertest");
const migrate = require("../libs/migrate");
const User = require("../models/User");

describe("Test admin and users routes", () => {
    let jwt_superadmin;
    let user1Datas;
    let user2Datas;
    let jwt_user1;
    let jwt_user2;

    beforeAll(async () => {
        await migrate();

        const responses = await Promise.all([
            (async () => {
                await User.create({
                    email: "superadmin@test.com",
                    password: "1234",
                    firstname: "super",
                    lastname: "admin",
                    gender: null,
                    roles: ['USER','SUPER_ADMIN']
                }).then(async user => {
                    user.registerToken = null;
                    await user.save();
                });

                 return request(app)
                    .post("/account/login")
                    .send({
                        email: "superadmin@test.com",
                        password: "1234"
                    })
                    .then(res => {
                        const {access_token} = JSON.parse(res.text);
                        return access_token;
                    })
            })(),
            (async () => {
                const user1Datas = await User.create({
                    email: "user1@test.com",
                    password: "1234",
                    firstname: "user1",
                    lastname: "user1",
                    gender: null,
                    roles: ['USER']
                }).then(async user => {
                    user.registerToken = null;
                    await user.save();
                    return {
                        ...user.dataValues,
                        createdAt: user.dataValues.createdAt.toISOString(),
                        updatedAt: user.dataValues.updatedAt.toISOString()
                    };
                });

                const jwt_user1 = await request(app)
                    .post("/account/login")
                    .send({
                        email: "user1@test.com",
                        password: "1234"
                    })
                    .then(res => {
                        const json = JSON.parse(res.text);
                        return json.access_token;
                    })
                return {jwt_user1,user1Datas}
            })(),
            (async () => {
                const user2Datas = await User.create({
                    email: "user2@test.com",
                    password: "1234",
                    firstname: "user2",
                    lastname: "user2",
                    gender: null,
                    roles: ['USER','ADMIN']
                }).then(async user => {
                    user.registerToken = null;
                    await user.save();
                    return {
                        ...user.dataValues,
                        createdAt: user.dataValues.createdAt.toISOString(),
                        updatedAt: user.dataValues.updatedAt.toISOString()
                    };
                });

                const jwt_user2 = await request(app)
                    .post("/account/login")
                    .send({
                        email: "user2@test.com",
                        password: "1234"
                    })
                    .then(res => {
                        const json = JSON.parse(res.text);
                        return json.access_token;
                    })
                return {jwt_user2,user2Datas}
            })()
        ])

        jwt_superadmin = responses[0];

        user1Datas = responses[1].user1Datas;
        jwt_user1 = responses[1].jwt_user1;

        user2Datas = responses[2].user2Datas;
        jwt_user2 = responses[2].jwt_user2;

    })


    test("Test get users, user by id, and user by search without connection", () => {
        return Promise.all([
            request(app)
                .get("/users"),
            request(app)
                .get("/users/"+user2Datas.id),
            request(app)
                .get("/users/search/user2"),
        ]).then(([users_res, one_user_res, search_user_res]) => {
            expect(users_res.statusCode).toBe(401);
            expect(one_user_res.statusCode).toBe(401);
            expect(search_user_res.statusCode).toBe(401);
        })
    })

    test("Test get users, user by id, and user by search, with user1 which has not admin role", () => {
        return Promise.all([
            request(app)
                .get("/users")
                .set('Authorization', 'Bearer '+jwt_user1),
            request(app)
                .get("/users/"+user2Datas.id)
                .set('Authorization', 'Bearer '+jwt_user1),
            request(app)
                .get("/users/search/user2")
                .set('Authorization', 'Bearer '+jwt_user1)
        ]).then(([users_res, one_user_res, search_user_res]) => {
            expect(users_res.statusCode).toBe(403);
            expect(one_user_res.statusCode).toBe(403);
            expect(search_user_res.statusCode).toBe(403);
        })
    })

    test("Test get users, user by id, and user by search, with user2 which has admin role", () => {
        return Promise.all([
            request(app)
                .get("/users")
                .set('Authorization', 'Bearer '+jwt_user2),
            request(app)
                .get("/users/"+user1Datas.id)
                .set('Authorization', 'Bearer '+jwt_user2),
            request(app)
                .get("/users/search/user1")
                .set('Authorization', 'Bearer '+jwt_user2)
        ]).then(([users_res, one_user_res, search_user_res]) => {
            expect(users_res.statusCode).toBe(200);
            expect(JSON.parse(users_res.text)).toEqual(expect.objectContaining({
                users: expect.any(Array),
                totalPages: expect.any(Number)
            }))

            expect(one_user_res.statusCode).toBe(200);
            expect(JSON.parse(one_user_res.text)).toEqual(user1Datas);

            expect(search_user_res.statusCode).toBe(200);
            expect(JSON.parse(search_user_res.text)).toEqual(expect.objectContaining({
                users: expect.any(Array),
                totalPages: expect.any(Number)
            }))
        })
    })

    test("Add admin role without connection", () => {
        return request(app)
            .patch("/admin/add/"+user1Datas.id)
            .then(res => {
                expect(res.statusCode).toBe(401);
            })
    })

    test("Add admin role with non superadmin user", () => {
        return request(app)
            .patch("/admin/add/"+user1Datas.id)
            .set('Authorization', 'Bearer '+jwt_user2)
            .then(res => {
                expect(res.statusCode).toBe(403);
            })
    })

    test("Add admin role to user1 and get users, user by id, and user by search with him", () => {
        return request(app)
            .patch("/admin/add/"+user1Datas.id)
            .set('Authorization', 'Bearer '+jwt_superadmin)
            .then(res => {
                expect(res.statusCode).toBe(200);

                const json = JSON.parse(res.text);
                expect(json).toEqual({
                    ...user1Datas,
                    updatedAt: json.updatedAt,
                    roles: [...user1Datas.roles, 'ADMIN']
                });

                return request(app)
                    .post("/account/login")
                    .send({
                        email: user1Datas.email,
                        password: '1234'
                    })
            })
            .then(res => {
                expect(res.statusCode).toBe(200);

                const json = JSON.parse(res.text);
                expect(json).toHaveProperty('access_token');
                expect(json).toHaveProperty('refresh_token');

                const {access_token} = json;

                return Promise.all([
                    request(app)
                        .get("/users")
                        .set('Authorization', 'Bearer '+access_token),
                    request(app)
                        .get("/users/"+user2Datas.id)
                        .set('Authorization', 'Bearer '+access_token),
                    request(app)
                        .get("/users/search/user1")
                        .set('Authorization', 'Bearer '+access_token)
                ])
            }).then(([users_res, one_user_res, search_user_res]) => {
                expect(users_res.statusCode).toBe(200);
                expect(JSON.parse(users_res.text)).toEqual(expect.objectContaining({
                    users: expect.any(Array),
                    totalPages: expect.any(Number)
                }))

                const one_user_json = JSON.parse(one_user_res.text);
                expect(one_user_res.statusCode).toBe(200);
                expect(one_user_json).toEqual({
                    ...user2Datas,
                    updatedAt: one_user_json.updatedAt,
                    roles: one_user_json.roles
                });

                expect(search_user_res.statusCode).toBe(200);
                expect(JSON.parse(search_user_res.text)).toEqual(expect.objectContaining({
                    users: expect.any(Array),
                    totalPages: expect.any(Number)
                }))
            })
    })

    test("Remove admin role from user2, and get users, user by id, and search user", () => {
        return request(app)
            .patch("/admin/remove/"+user2Datas.id)
            .set('Authorization', 'Bearer '+jwt_superadmin)
            .then(res => {
                expect(res.statusCode).toBe(200);

                const json = JSON.parse(res.text);
                expect(json).toEqual({
                    ...user2Datas,
                    updatedAt: json.updatedAt,
                    roles: user2Datas.roles.filter(role => role !== 'ADMIN')
                });

                return request(app)
                    .post("/account/login")
                    .send({
                        email: user2Datas.email,
                        password: "1234"
                    })
            }).then(res => {
                expect(res.statusCode).toBe(200);

                const json = JSON.parse(res.text);
                expect(json).toHaveProperty('access_token');
                expect(json).toHaveProperty('refresh_token');

                const {access_token} = json;

                return Promise.all([
                    request(app)
                        .get("/users")
                        .set('Authorization', 'Bearer '+access_token),
                    request(app)
                        .get("/users/"+user2Datas.id)
                        .set('Authorization', 'Bearer '+access_token),
                    request(app)
                        .get("/users/search/user1")
                        .set('Authorization', 'Bearer '+access_token)
                ])
            })
            .then(([users_res, one_user_res, search_user_res]) => {
                expect(users_res.statusCode).toBe(403);
                expect(one_user_res.statusCode).toBe(403);
                expect(search_user_res.statusCode).toBe(403);
            })
    })
})