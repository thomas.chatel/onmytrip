const app = require("../../app");
const User = require("../../models/User");
const request = require("supertest");
const migrate = require("../../libs/migrate");

describe("Tests user password change", () => {
    let jwt_token;
    beforeAll(async () => {
        await migrate();
        const user = await User.create({
            email: "test3@test.com",
            password: "1234",
            firstname: "test3",
            lastname: "test3",
            gender: null,
            roles: ['USER']
        });
        user.registerToken = null;
        await user.save();
        jwt_token = await request(app)
            .post("/account/login")
            .send({
                email: "test3@test.com",
                password: "1234"
            })
            .then(res => {
                const json = JSON.parse(res.text);
                return json.access_token;
            })
    });

    test("Test change password without token", () => {
        return request(app)
            .put("/account/change_password")
            .send({
                password: "1234",
                confirm_password: "1234"
            })
            .then(res => {
                expect(res.statusCode).toBe(401);
            })
    })

    test("Test change password with session but too simple passwords", () => {
        return request(app)
            .put("/account/change_password")
            .send({
                password: "1234",
                confirm_password: "1234"
            })
            .set('Authorization', 'Bearer '+jwt_token)
            .then(res => {
                expect(res.statusCode).toBe(422);
                expect(JSON.parse(res.text)).toEqual({
                    "violations":
                        [
                            {"propertyPath":"password","message":"Votre mot de passe n'est pas assez complexe"}
                        ]
                })
            })
    })

    test("Test change password with session but bad confirm passwords", () => {
        return request(app)
            .put("/account/change_password")
            .send({
                password: "12ABCDef$%",
                confirm_password: "12ABCDef"
            })
            .set('Authorization', 'Bearer '+jwt_token)
            .then(res => {
                expect(res.statusCode).toBe(422);
                expect(JSON.parse(res.text)).toEqual({
                    "violations":
                        [
                            {"message": "Les deux mots de passe ne correspondent pas", "propertyPath": "confirm_password"}
                        ]
                })
            })
    })

    test("Test change password with session and good params", () => {
        return request(app)
            .put("/account/change_password")
            .send({
                password: "12ABCDef$%",
                confirm_password: "12ABCDef$%"
            })
            .set('Authorization', 'Bearer '+jwt_token)
            .then(res => {
                expect(res.statusCode).toBe(200);
            })
    })

    test("Test call forgot password without body", () => {
        return request(app)
            .put("/account/forgot_password")
            .then(res => {
                expect(res.statusCode).toBe(400);
            })
    })

    test("Test call forgot password on not found account", () => {
        return request(app)
            .put("/account/forgot_password")
            .send({
                email: "test64864@test.com"
            })
            .then(res => {
                expect(res.statusCode).toBe(200);
            })
    })

    test("Test change password with bad password token", () => {
        return request(app)
            .put("/account/change_password/trucmuche")
            .send({
                password: "12ABCDef$%",
                confirm_password: "12ABCDef$%"
            })
            .then(res => {
                expect(res.statusCode).toBe(403);
            })
    })

    test("Test create password token and reset password with it", () => {
        return request(app)
            .put("/account/forgot_password")
            .send({
                email: "test3@test.com"
            })
            .then(res => {
                expect(res.statusCode).toBe(200);

                return User.findOne({
                    where: {
                        email: "test3@test.com"
                    }
                })
            })
            .then(user => {
                expect(user).not.toBeNull();
                return user
            })
            .then(user => {
                return request(app)
                    .put("/account/change_password/"+user.passwordToken)
                    .send({
                        password: "12ABCDef$%",
                        confirm_password: "12ABCDef$%"
                    })
                    .then(res => {
                        expect(res.statusCode).toBe(200);
                    })
            })
    })
})