const request = require("supertest");
const migrate = require("../../libs/migrate");
const app = require("../../app");

describe("Test register route", () => {
    beforeAll(() => {
        return migrate();
    })
    test("Should return 422 code with all fields missing", () => {
        return request(app)
            .post("/account/register")
            .then(res => {
                expect(res.statusCode).toBe(422);
                expect(JSON.parse(res.text)).toEqual({
                    "violations":
                        [
                            {"propertyPath": "email", "message": "Champs 'email' non spécifié"},
                            {"propertyPath": "password", "message": "Champs 'password' non spécifié"},
                            {
                                "propertyPath": "confirm_password",
                                "message": "Champs 'confirm_password' non spécifié"
                            },
                            {"propertyPath": "firstname", "message": "Champs 'firstname' non spécifié"},
                            {"propertyPath": "lastname", "message": "Champs 'lastname' non spécifié"},
                            {"propertyPath": "gender", "message": "Champs 'gender' non spécifié"}
                        ]
                })
            });
    })
    test("Should return 422 code with email and firstname missing", () => {
        return request(app)
            .post("/account/register")
            .send({
                password: "12ABCDef$%",
                confirm_password: "12ABCDef$%",
                lastname: "test",
                gender: "A"
            })
            .then(res => {
                expect(res.statusCode).toBe(422);
                expect(JSON.parse(res.text)).toEqual({
                    "violations":
                        [
                            {"propertyPath": "email", "message": "Champs 'email' non spécifié"},
                            {"propertyPath": "firstname", "message": "Champs 'firstname' non spécifié"},
                        ]
                })
            });
    })
    test("Should return 422 code with firstname, password and gender invalids", () => {
        return request(app)
            .post("/account/register")
            .send({
                email: "toto@toto.com",
                password: "1234",
                confirm_password: "1234",
                firstname: "t",
                lastname: "test",
                gender: "w"
            })
            .then(res => {
                expect(res.statusCode).toBe(422);
                expect(JSON.parse(res.text)).toEqual({
                    "violations": [
                        {"propertyPath": "password", "message": "Votre mot de passe n'est pas assez complexe"},
                        {"propertyPath": "firstname", "message": "Le prénom doit faire entre 2 et 30 caractères"},
                        {"propertyPath": "gender", "message": "Genre spécifié incorrect"}
                    ]
                })
            });
    })
    test("Try creating account", () => {
        const data = {
            email: "test@test.com",
            password: "12ABCDef$%",
            confirm_password: "12ABCDef$%",
            firstname: "test",
            lastname: "test",
            gender: "A"
        }
        return request(app)
            .post("/account/register")
            .send(data)
            .then(res => {
                expect(res.statusCode).toBe(201);
                return request(app)
                    .post("/account/register")
                    .send(data)
            })
            .then(res => {
                expect(res.statusCode).toBe(409);
            })
    })
})