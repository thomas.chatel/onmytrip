const request = require("supertest");
const migrate = require("../../libs/migrate");
const User = require("../../models/User");
const app = require("../../app");

describe("Test login route", () => {
    beforeAll(async () => {
        await migrate();
        await request(app)
            .post("/account/register")
            .send({
                email: "test2@test.com",
                password: "12ABCDef$%",
                confirm_password: "12ABCDef$%",
                firstname: "test2",
                lastname: "test2",
                gender: "A"
            })
            .expect(201)
    })

    test("Test login without body", () => {
        return request(app)
            .post("/account/login")
            .then(res => {
                expect(res.statusCode).toBe(404);
            })
    })
    test("Test login with bad credentials", () => {
        return request(app)
            .post("/account/login")
            .send({
                email: "test2@test.com",
                password: "12345"
            })
            .then(res => {
                expect(res.statusCode).toBe(401);
            })
    })
    test("Test login with non confirmed account", () => {
        return request(app)
            .post("/account/login")
            .send({
                email: "test2@test.com",
                password: "12ABCDef$%"
            })
            .then(res => {
                expect(res.statusCode).toBe(423);
            })
    })
    test("Confirm account and try login with good credentials", () => {
        return User.findOne({
            where: {
                email: "test2@test.com"
            }
        })
            .then(user => {
                expect(user).not.toBeNull();
                return user
            })
            .then(user => {
                return request(app)
                    .patch("/account/confirm/"+user.registerToken)
            })
            .then(res => {
                expect(res.statusCode).toBe(204);

                return request(app)
                    .post("/account/login")
                    .send({
                        email: "test2@test.com",
                        password: "12ABCDef$%"
                    })
            })
            .then(res => {
                expect(res.statusCode).toBe(200);

                const json = JSON.parse(res.text);
                expect(json).toHaveProperty('access_token');
                expect(json).toHaveProperty('refresh_token');
            })
    })
})