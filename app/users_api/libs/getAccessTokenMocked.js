module.exports = async function getAccessTokenUnitTests(user) {
    const token = btoa(JSON.stringify(user.dataValues));
    return {
        access_token: token,
        refresh_token: token
    }
}