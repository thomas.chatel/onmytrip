const rand = (a,b) => a+Math.floor(Math.random()*(b-a+1));

module.exports = function generateToken() {
    const chars = "abcdefghijklmnopqrstuvwxyz0123456789";
    const len = 15;
    let token = "";

    for (let i=0;i<len;i++) {
        let char = chars[rand(0,chars.length-1)];
        if (rand(0,1) === 0)
            char = char.toUpperCase();
        token += char;
    }
    return token;
}