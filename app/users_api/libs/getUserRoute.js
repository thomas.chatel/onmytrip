const getUser = require("./getUser");
const logger = require("../common_libs/logger");

module.exports = async (req, res) => {
    const user = await getUser(req.params.id);
    if (user === null) {
        logger.error(undefined, { returnCode: 404, method: 'getUserRoute'})
        return res.sendStatus(404);
    }
    logger.info(undefined, { returnCode: 200, method: 'getUserRoute'})
    res.json(user.dataValues)
}