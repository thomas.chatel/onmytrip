const Sequelize = require("sequelize");

const { USERS_DB_NAME, USERS_DB_USER, USERS_DB_PASSWORD, USERS_DB_DRIVER, USERS_DB_HOST } = process.env;

const connection = new Sequelize(USERS_DB_NAME, USERS_DB_USER, USERS_DB_PASSWORD, {
    host: USERS_DB_HOST,
    dialect: USERS_DB_DRIVER,
    logging: false
});

connection.authenticate();

module.exports = connection;
