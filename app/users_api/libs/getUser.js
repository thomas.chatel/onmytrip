const User = require("../models/User");

module.exports = id => User.findOne({
    where: {id}
})