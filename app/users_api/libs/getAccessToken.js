const request = require("../common_libs/request");

module.exports = function getAccessToken(user) {
    return request(`${process.env.JWT_API_HOST}/access_token`,
        {
            method: 'POST',
            body: JSON.stringify(user),
            port: 3000
        }).then(({body}) => JSON.parse(body));
}