const { Model, DataTypes } = require("sequelize");
const sequelize = require("../libs/sequelize");
const bcrypt = require("bcryptjs");
const {publishEvent} = require("../common_libs/EventSubscriber");
const {USER_UPDATED_ROUTE_KEY} = process.env;
const UserValidator = require("../forms/UserValidator");

class User extends Model {}

User.init(
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING(50),
            unique: true,
            allowNull: false,
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        firstname: {
            type: DataTypes.STRING(30),
            allowNull: false
        },
        lastname: {
            type: DataTypes.STRING(30),
            allowNull: false
        },
        gender: {
            type: DataTypes.BOOLEAN,
            allowNull: true
        },
        roles: {
            type: DataTypes.ARRAY(DataTypes.STRING),
            allowNull: false,
            defaultValue: ["USER"]
        },
        registerToken: {
            type: DataTypes.STRING,
            allowNull: true
        },
        passwordToken: {
            type: DataTypes.STRING,
            allowNull: true
        }
    },
    {
        sequelize,
        modelName: "User",
    }
);

const updatePassword = async (user) => {
    if ((user.type === "BULKUPDATE" && user.fields.includes('password')) ||
        (user.type === undefined && user._previousDataValues.password !== user.dataValues.password)) {

        const attributes = user.type === "BULKUPDATE" ? user.attributes : user;
        attributes.password = await bcrypt.hash(attributes.password, await bcrypt.genSalt());
    }
};

const notifyUpdates = async obj => {
    if (process.env.JEST_WORKER_ID)
        return;
    const fields = obj.type === "BULKUPDATE" ?
        obj.fields :
        Object.keys(obj._previousDataValues)
            .filter(key => obj._previousDataValues[key] !== obj.dataValues[key]);

    const attributes = obj.type === "BULKUPDATE" ?
        obj.attributes :
        fields.reduce((acc,key) => ({
            ...acc,
            [key]: obj.dataValues[key]
        }), {});

    const user = obj.type === "BULKUPDATE" ?
        await User.findOne({where: obj.where}).then(user => user ? user.dataValues : null) :
        obj.dataValues

    publishEvent(USER_UPDATED_ROUTE_KEY, {fields,attributes,user});
}

const setAdditionalDBFields = (user) => {
    for (const [field,value] of Object.entries(UserValidator.getAdditionalDBFields())) {
        user[field] = value;
    }
}

User.addHook("beforeCreate", setAdditionalDBFields);

User.addHook("beforeCreate", updatePassword);
User.addHook("beforeBulkUpdate", updatePassword);
User.addHook("beforeUpdate", updatePassword);

User.addHook("beforeUpdate", notifyUpdates);
User.addHook("beforeBulkUpdate", notifyUpdates);

module.exports = User;
