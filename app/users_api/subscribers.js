const {setEventSubscriberInstance} = require("./common_libs/EventSubscriber");
const {
    USER_CREATED_QUEUE,
    USER_EXCHANGE,
    USER_CREATED_ROUTE_KEY,
    USER_FORGOT_PASSWORD_QUEUE,
    USER_FORGOT_PASSWORD_ROUTE_KEY,
    USER_UPDATED_ROUTE_KEY,
    USER_PASSWORD_UPDATED_QUEUE,
    USER_ROLES_UPDATED_QUEUE,
    USER_NAMES_UPDATED_QUEUE
} = process.env;

const subscribers = {
    [USER_EXCHANGE]: {
        [USER_CREATED_ROUTE_KEY]: [USER_CREATED_QUEUE],
        [USER_FORGOT_PASSWORD_ROUTE_KEY]: [USER_FORGOT_PASSWORD_QUEUE],
        [USER_UPDATED_ROUTE_KEY]: [
            USER_PASSWORD_UPDATED_QUEUE,
            USER_ROLES_UPDATED_QUEUE,
            USER_NAMES_UPDATED_QUEUE
        ]
    }
}

function initSubscribers() {
    return setEventSubscriberInstance(subscribers);
}

module.exports = {initSubscribers};
