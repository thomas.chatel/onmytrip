const logger = require("./common_libs/logger");
const app = require("./app");
const migrate = require("./libs/migrate");
const {initSubscribers} = require("./subscribers");

migrate().then(() => {
    logger.info("User table successfully synchronized");
});

initSubscribers().then((res) => {
    if (res)
        logger.info("RabbitMQ subscriber connected successfully")
});

app.listen(process.env.PORT || 3000);

logger.info("users_api listened on " + (process.env.PORT || 3000) + " port");