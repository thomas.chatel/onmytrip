module.exports = async () => ({
    moduleNameMapper: {
        "(.*)/common_libs/(security|EventSubscriber)": "$1/common_libs/$2Mocked",
        "(.*)/libs/getAccessToken": "$1/libs/getAccessTokenMocked"
    }
})