const {Router} = require("express");
const UserValidator = require("../forms/UserValidator");
const UpdateUserValidator = require("../forms/UpdateUserValidator");
const User = require("../models/User");
const bcrypt = require("bcryptjs");
const generateToken = require("../libs/generateToken");
const getAccessToken = require("../libs/getAccessToken");
const ChangePasswordValidator = require("../forms/ChangePasswordValidator");
const {publishEvent} = require("../common_libs/EventSubscriber");
const {USER_CREATED_ROUTE_KEY, USER_FORGOT_PASSWORD_ROUTE_KEY} = process.env;
const {isAuth} = require("../common_libs/security");
const logger = require("../common_libs/logger");
const CustomError = require("../common_libs/CustomError");

const router = Router();

router.post("/register", (req, res, next) => {
        const userValidator = new UserValidator(req.body)
        let validateRes;
        if ((validateRes = userValidator.validate()) instanceof Array) {
            logger.error(undefined, { returnCode: 422, method: 'register' })
            return res.status(422).json({violations: validateRes});
        }
        new User(userValidator.getFieldsForDB())
            .save()
            .then(async user => {
                await publishEvent(USER_CREATED_ROUTE_KEY, {
                    user: user.dataValues,
                    referer: req.get('referer')
                })
                logger.info(`Account created for ${user.email}`, { returnCode: 201, user: user.id, method: 'register'});
                res.sendStatus(201);
            })
            .catch((e) =>
                logger.error(`Error while trying to create an account. e=${e.message}`, 
                    {
                        stack: e.stack.split("\n").slice(1).map(w => w.trim()),
                        returnCode: e.name === 'SequelizeValidationError' ? 400 : e.name === 'SequelizeUniqueConstraintError' ? 409 : 500,
                        method: 'register'
                    }) |
                res.sendStatus(e.name === 'SequelizeValidationError' ? 400 : e.name === 'SequelizeUniqueConstraintError' ? 409 : 500))
    }
);

router.post("/login", (req, res, next) => {
    (req.body.email && req.body.password) ?
        User.findOne({
            where:
                {
                    email: req.body.email
                }
        }).then(async user => {
            if (user == null || !(await bcrypt.compare(req.body.password, user.password))) {
                logger.warn(undefined, { returnCode: 401, method: 'login'})
                return res.sendStatus(401)
            } else if (user.registerToken !== null) {
                logger.warn(undefined, { returnCode: 423, method: 'login'})
                return res.sendStatus(423);
            }

            const tokens = await getAccessToken(user);
            logger.info(undefined, { returnCode: 200, method: 'login'})
            res.status(200).json({
                ...tokens,
                user: user.dataValues
            });
        }).catch(e => next(e)) : logger.warn(undefined, { returnCode: 404, method: 'login'}) | res.sendStatus(404)
});

router.patch("/confirm/:token", (req, res, next) =>
    User.findOne({
        where: {
            registerToken: req.params.token
        }
    }).then(user => {
        if (user === null) {
            logger.warn(`User with registerToken ${req.params.token} not found`, { returnCode: 404, method: 'confirmAccount'});
            return res.sendStatus(404);
        }
        user.registerToken = null;
        user.save().then(_ => logger.info(`Account confirmed for ${user.email}`, { email: user.email, user: user.id, returnCode: 204, method: 'confirmAccount'}) | res.sendStatus(204))
    }).catch(e => next(e))
);

router.put("/forgot_password", (req, res, next) => {
    const {email} = req.body;
    if (!email) {
        logger.error(undefined, { returnCode: 400, method: 'forgotPassword'})
        return res.sendStatus(400);
    }
    User.findOne({
        where: {
            email,
            registerToken: null
        }
    }).then(user => {
        if (user === null) {
            logger.error(undefined, { returnCode: 200, method: 'forgotPassword'})
            return res.sendStatus(200);
        }
        user.passwordToken = generateToken();
        Promise.all([
            user.save(),
            publishEvent(USER_FORGOT_PASSWORD_ROUTE_KEY, {
                user: user.dataValues,
                referer: req.get('referer')
            })
        ])
            .then(() => logger.info(`New token to change password generated for ${user.email}`, { user: user.id, returnCode: 200, method: 'forgotPassword'}) | res.sendStatus(200))
            .catch(e => next(e));
    })
});

const changePassword = async (req, res) => { // Use the same route, to change password from forgot password link, and from the connected user
    const {passwordToken} = req.params;

    let user;
    if (passwordToken) {
        if ((user = await User.findOne({where: {passwordToken}})) === null) {
            logger.warn(undefined, { returnCode: 403, method: 'changePassword'})
            return res.sendStatus(403);
        }
    } else {
        if ((user = await User.findOne({where: {id: req.user.id}})) === null) {
            logger.warn(undefined, { returnCode: 403, method: 'changePassword'})
            return res.sendStatus(401);
        }
    }

    let violations;
    const validator = new ChangePasswordValidator(req.body);
    if ((violations = validator.validate()) instanceof Array) {
        logger.warn(undefined, { returnCode: 422, method: 'changePassword'})
        return res.status(422).json({violations});
    }

    user.password = req.body.password;
    user.passwordToken = null;
    await user.save();
    logger.info(`Password changed for ${user.email}`,  { returnCode: 200, method: 'changePassword'});
    res.sendStatus(200);
}

router.put("/change_password/:passwordToken",
    (req, res, next) =>
        changePassword(req, res).catch(e => next(e)))


router.use(isAuth);

router.put("/change_password/",
    (req, res, next) =>
        changePassword(req, res).catch(e => next(e)))

router.put("/", (req, res, next) => {
    const updateUserValidator = new UpdateUserValidator(req.body)
    let validateRes;
    if ((validateRes = updateUserValidator.validate()) instanceof Array) {
        logger.warn(undefined, { returnCode: 422, method: 'updateUser'})
        return res.status(422).json({violations: validateRes});
    }

    const {id} = req.user;

    User.findOne({
        where: {
            id: req.user.id
        }
    }).then(user => {
        if (user === null) {
            logger.warn(undefined, { returnCode: 404, method: 'updateUser'})
            return res.sendStatus(404);
        }
        return user.update(updateUserValidator.getFields())
            .then(user => logger.info('User updated for ' + user.email, { user: user.id, returnCode: 200, method: 'updateUser'}) | res.status(200).send(user));
    })
        .catch(e => next(new CustomError(e, {id, ...req.body})))
});

module.exports = router;
