const {Router} = require("express");
const User = require("../models/User");
const CustomError = require("../common_libs/CustomError");
const { isAuth,hasRole } = require("../common_libs/security");
const logger = require("../common_libs/logger");
const {isInteger} = require("../common_libs/converters")
const getUser = require("../libs/getUser");
const router = Router();

router.use(isAuth);
router.use(hasRole('SUPER_ADMIN', getUser));

router.patch('/add/:id', (req, res, next) => {
    let {id} = req.params;
    if (!(id = isInteger(id)) || id === req.user.id) {
        logger.error(undefined, { returnCode: 400, method: 'addAdmin'})
        return res.sendStatus(400);
    }
    User.findOne({where: {id}})
        .then(async user => {
            if (user === null) {
                logger.error(undefined, { returnCode: 404, method: 'addAdmin'})
                return res.sendStatus(404);
            }
            (!user.roles.includes('ADMIN') ?
                    user.update({
                        roles: [...user.roles, 'ADMIN']
                    }) :
                    (async () => user)()
            )
                .then(usr => logger.info(`${usr.email} has been granted administrator role`, {user: user.id, returnCode: 200, method: 'addAdmin'}) | res.status(200).json(usr))
                .catch(e => next(new CustomError(e, {email: user.email})));
        })
        .catch(e => next(new CustomError(e, {id: req.params.id})));
})

router.patch('/remove/:id', (req, res, next) => {
    let {id} = req.params;
    if (!(id = isInteger(id)) || id === req.user.id) {
        logger.error(undefined, { returnCode: 400, method: 'removeAdmin'})
        return res.sendStatus(400);
    }
    User.findOne({where: {id}})
        .then(user => {
            if (user === null) {
                logger.error(undefined, { returnCode: 404, method: 'removeAdmin'})
                return res.sendStatus(404);
            }
            user.update({
                roles: user.roles.filter(role => role !== 'ADMIN')
            })
                .then(usr => logger.info(`${usr.email} administrator role has been removed`, {user: user.id, returnCode: 200, method: 'removeAdmin'}) | res.status(200).json(usr))
                .catch(e => next(new CustomError(e, {email: user.email})));
        })
        .catch(e => next(new CustomError(e, {id: req.params.id})));
})

module.exports = router;
