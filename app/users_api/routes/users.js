const {Router} = require("express");
const User = require("../models/User");
const {Op} = require("sequelize");
const { isAuth,hasRole } = require("../common_libs/security");
const CustomError = require("../common_libs/CustomError");
const logger = require("../common_libs/logger");
const getUser = require("../libs/getUser");
const getUserRoute = require("../libs/getUserRoute");

const nbUsersByPage = 10;

const router = Router();

const getUsers = (req, res, next) => {
	const page = req.query.page??1;
	const search = req.params.search??null
	const searchObj = search ? {
		where: {
			[Op.or]: [
				{ email: {[Op.iLike]: '%'+search+'%'} },
				{ firstname: {[Op.iLike]: '%' + search + '%'} },
				{ lastname: {[Op.iLike]: '%' + search + '%'} }
			]
		}
	} : {};
	return Promise.all([
		User.findAll({
			...searchObj,
			limit: nbUsersByPage,
			offset: nbUsersByPage * (page - 1),
			order: [
				['email', 'ASC'],
				['firstname', 'ASC'],
				['lastname', 'ASC'],
			],
		}),
		User.count(searchObj)
	])
		.then(([users, total]) => ({
			users,
			totalPages: Math.floor(total / nbUsersByPage) + (total % nbUsersByPage !== 0 ? 1 : 0)
		}))
		.then(data => logger.info(undefined, { returnCode: 200, method: 'getUsers'}) | res.json(data))
		.catch(e => next(new CustomError(e, req.query)))
}

router.use(isAuth);
router.use(hasRole(['ADMIN','SUPER_ADMIN'], getUser));

router.get("/", getUsers)
router.get("/search/:search", getUsers)
router.get("/:id", getUserRoute)

module.exports = router;
