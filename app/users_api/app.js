const express = require("express");
const cors = require("cors");
const logErrorsMiddleware = require("./common_libs/logErrorsMiddleware");
const account = require("./routes/account");
const admin = require("./routes/admin");
const users = require("./routes/users");
const getUserRoute = require("./libs/getUserRoute");

const app = express()

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/account", account);
app.use("/admin", admin);
app.use("/users", users);

// This route is only accessible from internal API calls
app.get("/getUser/:id", getUserRoute)

app.use(logErrorsMiddleware);

app.get('/status', (req, res) => res.send('OK'));

module.exports = app;
