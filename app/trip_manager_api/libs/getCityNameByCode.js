const request = require("../common_libs/request");

module.exports = function getCityNameByCode(codeCommune) {
    return request(`${process.env.ROUTE_API_HOST}/city/${codeCommune}/name`, {port: 3000})
        .then(({status,body}) => status === 200 ? JSON.parse(body) : null )
}