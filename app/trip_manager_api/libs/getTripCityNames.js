const getCityNameByCode = require("./getCityNameByCode");

function getOneTripWithCityNames(trip) {
	return getTripsWithCityNames([trip])
		.then(roadtrips => roadtrips[0])
}

async function getTripsWithCityNames(trips, tripsWithCityNames = [], cityNamesByCode = {}) {
	if (trips.length === 0)
		return tripsWithCityNames;

	const {trip: tripWithCityName, cityNamesByCode: cityNamesByCodeUpdated} = await getTripWithCityNames(trips[0].roadtrip._doc, cityNamesByCode);

	return getTripsWithCityNames(trips.slice(1), [...tripsWithCityNames, { ...trips[0]._doc, roadtrip: tripWithCityName  }], cityNamesByCodeUpdated);
}

async function getTripWithCityNames(trip, cityNamesByCode = {}) {
	const foundCols = [
		['codeCommune','nameCommune'],
		['codeCommuneA','nameCommuneA'],
		['codeCommuneB','nameCommuneB']
	].filter(([codeKey]) => trip[codeKey] !== undefined);

	return getCityNames(
		trip,
		foundCols.map(([codeCol]) => codeCol),
		foundCols.map(([,nameCol]) => nameCol),
		cityNamesByCode);
}

async function getCityNames(trip,columnsCode,columnsName,cityNamesByCode = {}) {
	if (columnsCode.length === 0 || columnsName.length === 0 || columnsCode.length !== columnsName.length) {
		return {trip, cityNamesByCode};
	}

	const codeCommuneCol = columnsCode[0];
	const codeCommune = trip[codeCommuneCol];

	const cityName =
		cityNamesByCode[codeCommune] ??
		await getCityNameByCode(codeCommune)

	return getCityNames({
		...trip,
		[columnsName[0]]: cityName
	}, columnsCode.slice(1), columnsName.slice(1), {
		...cityNamesByCode,
		[codeCommune]: cityName
	});
}

module.exports = {getTripsWithCityNames,getOneTripWithCityNames};
