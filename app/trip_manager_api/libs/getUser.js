const request = require("../common_libs/request");

module.exports = id =>
    request(`${process.env.USER_API_HOST}/getUser/${id}`, {port: 3000})
        .then(({status,body}) => status === 200 ? JSON.parse(body) : null)