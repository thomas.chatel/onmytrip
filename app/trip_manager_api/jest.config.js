module.exports = async () => ({
    moduleNameMapper: {
        "(.*)/common_libs/(security|EventSubscriber)": "$1/common_libs/$2Mocked",
        "(.*)/(getCityNameByCode|getUser)": "$1/../mocks/$2Mocked",
    }
})