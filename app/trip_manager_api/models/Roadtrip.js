const { Schema } = require("mongoose");
const { connect } = require("../Mongo");

const db = connect();

const POISchema = new Schema({
    type: String, // AMADEUS or TOMTOM
    POIType: String, // POI, restaurant or hostel
    nbDay: { type: Number, required: false },
    id: String,
    category: { type: String, required: false },
    lat: Number,
    lng: Number,
    name: String,
    rank: { type: Number, required: false },
    tags: [String],
    categorySet: [Number],
})

const LatLngSchema = new Schema({
    lat: Number,
    lng: Number
})

const RoadtripSchema = new Schema({
	userId: Number,
    roadtrip: new Schema({
        type: String, // AToB or cityTour
        nbDays: Number,
        name: String,

        // For cityTour type
        codeCommune: { type: String, required: false },
        hostel: { type: POISchema, required: false },
        // For AtoB type
        from: {type: LatLngSchema, required: false},
        to: {type: LatLngSchema, required: false},
        codeCommuneA: { type: String, required: false },
        codeCommuneB: { type: String, required: false },

        road: [POISchema],
    }, { _id: false }),
    createdAt: { type: Date, default: () => new Date() }
}); 

// @ts-ignore
module.exports = db.model('Roadtrip', RoadtripSchema);