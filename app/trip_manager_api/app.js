const express = require("express")
const cors = require("cors")
const logger = require("./common_libs/logger")
const Roadtrip = require("./models/Roadtrip")
const { isAuth, hasRole } = require("./common_libs/security")
const logErrorsMiddleware = require("./common_libs/logErrorsMiddleware")
const CustomError = require("./common_libs/CustomError")
const { isInteger } = require("./common_libs/converters")
const {
  getOneTripWithCityNames,
  getTripsWithCityNames
} = require("./libs/getTripCityNames")
const { ROADTRIP_DELETED_ROUTE_KEY } = process.env
const { publishEvent } = require("./common_libs/EventSubscriber")
const getUser = require("./libs/getUser")

const app = express()

app.use(cors())
app.use(express.json())

async function detectAlreadyUsedName(name, userId, id = null) {
  let n = 1
  while (
    await Roadtrip.findOne({
      ...(id ? { _id: { $ne: id } } : {}),
      userId,
      "roadtrip.name": name + (n > 1 ? " " + n : "")
    })
  ) {
    n += 1
  }
  return name + (n > 1 ? " " + n : "")
}

app.use(isAuth)

app.post("/trip_manager", (req, res, next) =>
  (async (req, res, next) => {
    if (!req.body.roadtrip.name || req.body.roadtrip.name.length > 50) {
      logger.warn(undefined, { returnCode: 403, method: 'saveRoadtrip', user: req.body.userId})
      return res.sendStatus(403)
    }

    const { roadtrip: { name }, userId } = req.body

    req.body.roadtrip.name = await detectAlreadyUsedName(name, userId)

    const roadtrip = await Roadtrip.create(req.body)
    logger.info(`New roadtrip saved by ${req.user.email}`, { email: req.user.email, returnCode: 201, method: 'saveRoadtrip' })
    res.status(201).json(roadtrip)
  })(req, res).catch(e => next(e))
)

app.patch("/trip_manager/:id", (req, res, next) =>
  (async (req, res) => {
    const { name } = req.body
    if (!name || name.length > 50) {
      logger.error(undefined, { returnCode: 400, method: 'patchRoadtrip' })
      return res.sendStatus(400)
    }

    const { id } = req.params
    const roadTrip = await Roadtrip.findOne({
      _id: id
    })
    if (roadTrip === null) {
      logger.error(undefined, { returnCode: 404, method: 'patchRoadtrip' })
      return res.sendStatus(404)
    }

    roadTrip.roadtrip.name = await detectAlreadyUsedName(
      name,
      roadTrip.userId,
      id
    )
    await roadTrip.save()
    logger.info(`Roadtrip updated with name '${name}'`, { roadtripId: id, user: roadTrip.userId, returnCode: 200, method: 'patchRoadtrip' })
    res.status(200).json(roadTrip)
  })(req, res).catch(
    e =>
      e.name === "CastError"
        ? logger.error("Cast error when get try to get road trip", {
            name: e.name,
            message: e.message,
            returnCode: 400, 
            method: 'patchRoadtrip'
          }) | res.sendStatus(400)
        : next(new CustomError(e, { id: req.params.id, body: req.body }))
  )
)

app.get(
  "/trip_manager",
  hasRole(["ADMIN", "SUPER_ADMIN"], getUser),
  (req, res, next) => {
    return Roadtrip.find()
      .sort({ _id: -1 })
      .then(roadtrips => getTripsWithCityNames(roadtrips))
      .then(roadtrips => logger.info(undefined, { returnCode: 200, method: 'getRoadtrips'}) | res.status(200).json(roadtrips))
      .catch(e => next(e))
  }
)

app.get("/trip_manager/:id", (req, res, next) => {
  const id = req.params.id
  return Roadtrip.findById(id)
    .then(async roadtrip => {
      if (roadtrip == null) {
        logger.error(undefined, { user: req.user.id, returnCode: 404, method: 'getRoadtripById'})
        return res.sendStatus(404)
      }
      const user = await getUser(req.user.id)
      if (
        user &&
        (user.id === roadtrip.userId ||
          user.roles.includes("ADMIN") ||
          user.roles.includes("SUPER_ADMIN"))
      ) {
        return getOneTripWithCityNames(roadtrip).then(roadtrip => {
          logger.info(undefined, { user: req.user.id, returnCode: 200, method: 'getRoadtripById'})
          res.status(200).json(roadtrip)
        })
      }
      logger.warn(undefined, { user: req.user.id, returnCode: 403, method: 'getRoadtripById'})
      return res.sendStatus(403)
    })
    .catch(
      e =>
        e.name === "CastError"
          ? logger.error("Cast error when get try to get road trip", {
              name: e.name,
              message: e.message,
              returnCode: 400, 
              method: 'getRoadtripById'
            }) | res.sendStatus(400)
          : next(new CustomError(e, { id }))
    )
})

app.get("/trip_manager/user/:id", async (req, res, next) => {
  let id = req.params.id

  if (!(id = isInteger(id))) {
    logger.error(undefined, { user: req.user.id, returnCode: 400, method: 'getUserRoadtrips'})
    return res.sendStatus(400)
  }
  const user = await getUser(req.user.id)
  if (
    user &&
    (id === user.id ||
      user.roles.includes("ADMIN") ||
      user.roles.includes("SUPER_ADMIN"))
  ) {
    return Roadtrip.find({ userId: id })
      .sort({ _id: -1 })
      .then(roadtrips => getTripsWithCityNames(roadtrips))
      .then(roadtrips => logger.info(undefined, { user: req.user.id, returnCode: 200, method: 'getUserRoadtrips'}) | res.status(200).json(roadtrips))
      .catch(e => next(new CustomError(e, { id })))
  } else {
    logger.warn(undefined, { user: req.user.id, returnCode: 403, method: 'getUserRoadtrips'})
    return res.sendStatus(403)
  }
})

app.delete("/trip_manager/:id", (req, res, next) => {
  const id = req.params.id

  return Roadtrip.findById(id)
    .then(async roadtrip => {
      if (roadtrip === null) {
        logger.error(undefined, { user: req.user.id, returnCode: 404, method: 'deleteRoadtripById'})
        return res.sendStatus(404)
      }
      const user = await getUser(req.user.id)
      if (
        user &&
        (user.id === roadtrip.userId ||
          user.roles.includes("ADMIN") ||
          user.roles.includes("SUPER_ADMIN"))
      ) {
        return Roadtrip.deleteOne({ _id: id })
          .then(async data => {
            if (data.deletedCount === 0) {
              logger.error(undefined, { user: req.user.id, returnCode: 404, method: 'deleteRoadtripById'})
              return res.sendStatus(404)
            }

            const user =
              req.user.id === roadtrip.userId
                ? req.user
                : await getUser(roadtrip.userId)

            publishEvent(ROADTRIP_DELETED_ROUTE_KEY, {
              roadtrip: await getOneTripWithCityNames(roadtrip),
              user,
              deleterId: req.user.id
            })

            logger.info(`Roadtrip deleted`, { user: req.user.id, returnCode: 204, method: 'deleteRoadtripById'});
            res.sendStatus(204)
          })
          .catch(
            e =>
              e.name === "CastError"
                ? logger.error(undefined, { user: req.user.id, returnCode: 400, method: 'deleteRoadtripById'}) | res.sendStatus(400)
                : next(new CustomError(e, { id }))
          )
      }
      logger.warn(undefined, { user: req.user.id, returnCode: 403, method: 'deleteRoadtripById'});
      return res.sendStatus(403)
    })
    .catch(
      e =>
        e.name === "CastError"
          ? logger.info(undefined, { user: req.user.id, returnCode: 400, method: 'deleteRoadtripById'}) | res.sendStatus(400)
          : next(new CustomError(e, { id }))
    )
})

app.use(logErrorsMiddleware)


module.exports = app