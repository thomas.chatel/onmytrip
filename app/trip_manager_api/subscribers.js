const {setEventSubscriberInstance} = require("./common_libs/EventSubscriber");
const {
    ROADTRIP_MANAGER_EXCHANGE,
    ROADTRIP_DELETED_ROUTE_KEY,
    ROADTRIP_DELETED_QUEUE
} = process.env;

const subscribers = {
    [ROADTRIP_MANAGER_EXCHANGE]: {
        [ROADTRIP_DELETED_ROUTE_KEY]: [ROADTRIP_DELETED_QUEUE]
    }
}

function initSubscribers() {
    return setEventSubscriberInstance(subscribers);
}

module.exports = {initSubscribers};
