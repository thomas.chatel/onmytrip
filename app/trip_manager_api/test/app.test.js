const request = require("supertest");
const getUser = require("../mocks/getUserMocked")
const app = require("../app");

let jwt_token;
let testRT;

const roadtrip = {
    "userId": 1,
    "roadtrip": {
        "road": [
            {
                "type": "AMADEUS",
                "id": "D821F8D8C0",
                "category": "SIGHTS",
                "lat": 48.776627,
                "lng": 2.511089,
                "name": "Citysun94",
                "rank": 100,
                "tags": [
                    "beauty&spas",
                    "sightseeing",
                    "commercialplace"
                ],
                "POIType": "POI",
                "nbDay": 1
            },
            {
                "type": "AMADEUS",
                "id": "BFC2B9155A",
                "category": "RESTAURANT",
                "lat": 48.79757,
                "lng": 2.50265,
                "name": "Cocotte et Vins",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "bar"
                ],
                "nbDay": 1,
                "POIType": "restaurant"
            },
            {
                "type": "AMADEUS",
                "id": "CC43AAE419",
                "category": "SHOPPING",
                "lat": 48.819668,
                "lng": 2.523265,
                "name": "Kiloutou",
                "rank": 100,
                "tags": [
                    "shopping",
                    "professionalservices",
                    "rental",
                    "sightseeing",
                    "commercialplace"
                ],
                "POIType": "POI",
                "nbDay": 1
            },
            {
                "type": "TOMTOM",
                "id": "250009041026924",
                "categories": [
                    "bed breakfast guest houses",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314002
                ],
                "name": "La Maison de Madeleine",
                "lat": 49.01557,
                "lng": 3.02671,
                "POIType": "hostel",
                "nbDay": 1
            },
            {
                "type": "AMADEUS",
                "id": "BAB3602A21",
                "category": "SHOPPING",
                "lat": 49.07613,
                "lng": 3.61066,
                "name": "Champagne Météyer Père et Fils",
                "rank": 10,
                "tags": [
                    "shopping",
                    "liquor",
                    "vineyard",
                    "sightseeing",
                    "commercialplace"
                ],
                "POIType": "POI",
                "nbDay": 2
            },
            {
                "type": "AMADEUS",
                "id": "74169D375C",
                "category": "RESTAURANT",
                "lat": 49.075535,
                "lng": 3.640075,
                "name": "La Lombardie",
                "rank": 50,
                "tags": [
                    "restaurant"
                ],
                "nbDay": 2,
                "POIType": "restaurant"
            },
            {
                "type": "AMADEUS",
                "id": "C40BD3F5B9",
                "category": "SHOPPING",
                "lat": 49.253475,
                "lng": 4.029501,
                "name": "Galeries Lafayette",
                "rank": 100,
                "tags": [
                    "shopping",
                    "clothing",
                    "fashion",
                    "sightseeing",
                    "commercialplace",
                    "mall",
                    "professionalservices"
                ],
                "POIType": "POI",
                "nbDay": 2
            },
            {
                "type": "TOMTOM",
                "id": "250009034420044",
                "categories": [
                    "bed breakfast guest houses",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314002
                ],
                "name": "La Bulle à Payet",
                "lat": 49.059,
                "lng": 4.383,
                "POIType": "hostel",
                "nbDay": 2
            },
            {
                "type": "AMADEUS",
                "id": "F7B5C2BA44",
                "category": "SHOPPING",
                "lat": 49.157536,
                "lng": 7.02764,
                "name": "Optique de la Sarre",
                "rank": 200,
                "tags": [
                    "shopping",
                    "optical",
                    "health&medical",
                    "sightseeing",
                    "commercialplace"
                ],
                "POIType": "POI",
                "nbDay": 3
            },
            {
                "type": "AMADEUS",
                "id": "57337E41D6",
                "category": "RESTAURANT",
                "lat": 49.155445,
                "lng": 7.027505,
                "name": "Formul'Pizza",
                "rank": 200,
                "tags": [
                    "restaurant",
                    "fastfood",
                    "pizza",
                    "sightseeing",
                    "commercialplace"
                ],
                "nbDay": 3,
                "POIType": "restaurant"
            },
            {
                "type": "AMADEUS",
                "id": "4E8512B0D8",
                "category": "SIGHTS",
                "lat": 48.82425,
                "lng": 7.405193,
                "name": "Mairie",
                "rank": 200,
                "tags": [
                    "cityhall",
                    "sightseeing",
                    "commercialplace"
                ],
                "POIType": "POI",
                "nbDay": 3
            },
            {
                "type": "TOMTOM",
                "id": "250009041019251",
                "categories": [
                    "bed breakfast guest houses",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314002
                ],
                "name": "Chambres d'Hôtes",
                "lat": 48.74139,
                "lng": 7.58852,
                "POIType": "hostel",
                "nbDay": 3
            },
            {
                "type": "AMADEUS",
                "id": "372147BE01",
                "category": "SIGHTS",
                "lat": 48.832237,
                "lng": 7.651012,
                "name": "Kleinclaus Idéal Coiff",
                "rank": 300,
                "tags": [
                    "sightseeing",
                    "commercialplace",
                    "professionalservices",
                    "barbershop",
                    "hairsalon"
                ],
                "POIType": "POI",
                "nbDay": 4
            },
            {
                "type": "AMADEUS",
                "id": "71103C5696",
                "category": "RESTAURANT",
                "lat": 48.58266,
                "lng": 7.739334,
                "name": "Lacocina",
                "rank": 100,
                "tags": [
                    "vegetarian",
                    "restaurant",
                    "sightseeing",
                    "commercialplace",
                    "fastfood"
                ],
                "nbDay": 4,
                "POIType": "restaurant"
            },
            {
                "type": "AMADEUS",
                "id": "3DFD1D1893",
                "category": "SIGHTS",
                "lat": 48.56045,
                "lng": 7.753359,
                "name": "Stade de la Meinau",
                "rank": 100,
                "tags": [
                    "stadium",
                    "sports",
                    "sportclub",
                    "landmark",
                    "historicplace",
                    "club",
                    "sightseeing",
                    "sights",
                    "historic",
                    "gym",
                    "attraction",
                    "activities",
                    "tourguide",
                    "shopping",
                    "transport"
                ],
                "POIType": "POI",
                "nbDay": 4
            }
        ],
        "codeCommuneA": "75056",
        "codeCommuneB": "67482",
        "from": {
            "lat": 48.8589,
            "lng": 2.347
        },
        "to": {
            "lat": 48.5691,
            "lng": 7.7621
        },
        "nbDays": 4,
        "name": "Test",
        "type": "AToB"
    }
};

beforeAll(async () => {
    jwt_token = btoa(JSON.stringify(await getUser(1)))

    // Save road trip before all in order to road trip can be updated, getted and deleted
    return request(app)
        .post("/trip_manager")
        .set('Authorization', 'Bearer ' + jwt_token)
        .send(roadtrip)
        .expect(201)
        .then(res => testRT = res.body)
})

describe("Test save roadtrip", () => {

    test("Test to save a roadtrip", () => {
        return request(app)
            .post("/trip_manager")
            .set('Authorization', 'Bearer ' + jwt_token)
            .send(roadtrip)
            .expect(201)
    })

    test("Test to save a roadtrip with no authorizations", () => {
        return request(app)
            .post("/trip_manager")
            .send(roadtrip)
            .expect(401)
            .then(res => {
            })
    })
})

describe("Test to get roadtrips", () => {

    beforeAll(async () => {
        jwt_token = btoa(JSON.stringify({
            id: 1,
            email: "test@test.com",
            password: "1234",
            firstname: "test",
            lastname: "test",
            gender: null,
            roles: ['USER', 'ADMIN'],
            registerToken: null,
            passwordToken: null
        }))
    })

    test("Test to get all roadtrips", () => {
        return request(app)
            .get("/trip_manager")
            .set('Authorization', 'Bearer ' + jwt_token)
            .expect(200)
            .then(res => {
            })
    })

    test("Test to get all roadtrips with no authorizations", () => {
        return request(app)
            .get("/trip_manager")
            .expect(401)
            .then(res => {
            })
    })

    test("Test to get roadtrips from id", () => {
        return request(app)
            .get("/trip_manager/" + testRT._id)
            .set('Authorization', 'Bearer ' + jwt_token)
            .expect(200)
            .then(res => {
            })
    })

    test("Test to get roadtrips from a user", () => {
        return request(app)
            .get("/trip_manager/user/1")
            .set('Authorization', 'Bearer ' + jwt_token)
            .expect(200)
            .then(res => {
            })
    })

    test("Test to get roadtrips from a user with no authorizations", () => {
        return request(app)
            .get("/trip_manager/user/1")
            .expect(401)
            .then(res => {
            })
    })
})

describe("Test to modify a roadtrip", () => {

    test("Test to patch roadtrip name", () => {
        return request(app)
            .patch("/trip_manager/" + testRT._id)
            .set('Authorization', 'Bearer ' + jwt_token)
            .send({
                "name": "Test 2"
            })
            .expect(200)
            .then(res => {
            })
    })

    test("Test to patch roadtrip name with no authorization", () => {
        return request(app)
            .patch("/trip_manager/" + testRT._id)
            .send({
                "name": "Test 2"
            })
            .expect(401)
            .then(res => {
            })
    })
})

describe("Test to delete a roadtrip", () => {
    test("Test to delete a roadtrip with no authorization", () => {
        return request(app)
            .delete("/trip_manager/" + testRT._id)
            .expect(401)
            .then(res => {
            })
    })
})

afterAll(() => {
    // Delete road trip after all, to be sure that is already created
    return request(app)
        .delete("/trip_manager/" + testRT._id)
        .set('Authorization', 'Bearer ' + jwt_token)
        .expect(204)
})