module.exports = async function getCityNameByCode(codeCommune) {
    return {
        75056: "Paris",
        67482: "Strasbourg"
    }[codeCommune] ?? null
}