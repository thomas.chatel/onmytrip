const { initSubscribers } = require("./subscribers")
const logger = require("./common_libs/logger")
const app = require("./app");

initSubscribers();

app.listen(process.env.PORT || 3000, () =>
    logger.info(
        `trip_manager_api server started and listening on port ${process.env.PORT || 3000}`
    )
)