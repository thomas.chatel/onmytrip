module.exports = ({id,category,lat,lng,name,rank,tags}) => ({
    type: "AMADEUS",
    id,category,lat,lng,name,rank,tags
})