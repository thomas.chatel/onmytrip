module.exports = ({id, poi: {categories, categorySet, name}, position: {lat, lon: lng} }) => ({
    type: "TOMTOM",
    id, categories, categorySet: categorySet.map(({id}) => id), name, lat, lng
})