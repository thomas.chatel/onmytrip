const request = require("../common_libs/request");
const jsonToUrlEncoded = require("../common_libs/jsonToUrlEncoded");
const CustomError = require("../common_libs/CustomError");

let access_token = null;

const { AMADEUS_CLIENT_ID, AMADEUS_CLIENT_SECRET, AMADEUS_URL_OAUTH2_LOGIN, AMADEUS_URL_GET_POI } = process.env;
const LIMIT_BY_PAGE = parseInt(process.env.AMADEUS_LIMIT_BY_PAGE);
const NB_FIRST_PAGES = process.env.AMADEUS_NB_FIRST_PAGES === "all" ? process.env.AMADEUS_NB_FIRST_PAGES : parseInt(process.env.AMADEUS_NB_FIRST_PAGES);

function get_access_token() {
	return request(AMADEUS_URL_OAUTH2_LOGIN, {
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		method: "POST",
		body: jsonToUrlEncoded({
			grant_type: "client_credentials",
			client_id: AMADEUS_CLIENT_ID,
			client_secret: AMADEUS_CLIENT_SECRET
		})
	}).then(({status,body}) => {
		if (status === 401)
			throw new Error("Connection to API failed");
		if (status !== 200)
			throw new Error("Invalid API response "+status+" \n"+body);
		return JSON.parse(body).access_token;
	});
}

const debugging = false;
const debug = msg => debugging && console.log(msg);

const maxRetry = 20;

async function getPoi(latitude,longitude,radius,offset = 0,total=null, nbRetried = 0) {
	debug("Start offset "+offset+"/"+(total??"??"));
	let token_renewed = false;
	if (access_token === null) {
		access_token = await get_access_token();
		token_renewed = true;
	}
	let res;
	try {
		res = await request(AMADEUS_URL_GET_POI + "?" + jsonToUrlEncoded({
			latitude,
			longitude,
			radius,
			"page[limit]": LIMIT_BY_PAGE,
			"page[offset]": offset
		}), {
			headers: {
				'Authorization': 'Bearer ' + access_token
			}
		}, 1500)
	} catch (e) {
		if (e.name === "timeout") {
			if (nbRetried === maxRetry)
				throw new CustomError(e, {
					latitude,
					longitude,
					radius,
					limit: LIMIT_BY_PAGE,
					offset
				}, 413);
			debug(`Offset ${offset} timeout exceded ${nbRetried}/${maxRetry}, retry`);
			return getPoi(latitude, longitude, radius, offset, total, nbRetried + 1);
		}
		throw e;
	}
	const {status,body} = res;
	if (status === 500)
		throw new CustomError("Fatal error on API side \n"+body, {latitude, longitude, radius, limit: LIMIT_BY_PAGE, offset});

	if (status === 429) {
		if (nbRetried === maxRetry)
			throw new CustomError("The network rate limit is exceeded for API", null, 413);
		debug(`Offset ${offset} failed ${nbRetried}/${maxRetry}, retry`);
		return getPoi(latitude,longitude,radius,offset,total,nbRetried+1);
	}

	if (status === 401) {
		if (token_renewed)
			throw new Error("Can't connect to API");
		access_token = null;

		return getPoi(latitude,longitude,radius);
	}

	const {data,meta} = JSON.parse(body);
	total = meta.count;

	debug(offset+" getted");

	if (offset > 0)
		return data;

	const totalPages = Math.floor(total/LIMIT_BY_PAGE);
	const nbPagesToGet = (NB_FIRST_PAGES === "all" ?  totalPages : Math.min(totalPages,NB_FIRST_PAGES-1))

	await Promise.all((() => {
		const promises = [];
		for (let i=1;i<=nbPagesToGet;i++) {
			promises.push(getPoi(latitude,longitude,radius,i*LIMIT_BY_PAGE, total));
		}
		return promises;
	})()).then(responses => {
		for (const response of responses)
			for (const POI of response)
				data.push(POI);
	})


	debug("All getted")

	return data;
}

module.exports = {getPoi};
