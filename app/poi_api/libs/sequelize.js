const Sequelize = require("sequelize");

const { POSTGRES_DB, POSTGRES_USER, POSTGRES_PASSWORD, USERS_DB_HOST } = process.env;

const connection = new Sequelize(POSTGRES_DB, POSTGRES_USER, POSTGRES_PASSWORD, {
    host: USERS_DB_HOST,
    dialect: "postgres",
    logging: false
});

connection.authenticate();

module.exports = connection;
