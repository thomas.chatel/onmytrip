const sequelize = require("../libs/sequelize");
const fs = require("fs/promises");

const POI = require("../models/POI");
const SearchArea = require("../models/SearchArea");
const POI_SearchArea = require("../models/POI_SearchArea");

module.exports = async function migrate() {
    const pgsql_dir = __dirname+"/../pgsqls/";
    for (const file of await fs.readdir(pgsql_dir)) {
        console.log("Executing '"+file+"'");
        await sequelize.query(await fs.readFile(pgsql_dir+file).then(buffer => buffer.toString()))
    }

    for (const model of [SearchArea,POI,POI_SearchArea]) {
        console.log("sync "+model.name);
        await model.sync();
    }
}
