const request = require("../common_libs/request");
const jsonToUrlEncoded = require("../common_libs/jsonToUrlEncoded");
const CustomError = require("../common_libs/CustomError");

let {
    TOMTOM_URL_GET_POI,
    TOMTOM_URL_GET_CATEGORIES,
    TOMTOM_VERSION,
    TOMTOM_API_KEY,
    TOMTOM_HOSTEL_CATEGORY_NAME,
    TOMTOM_HOSTEL_CATEGORY_ID,
    TOMTOM_LANGUAGE,
    TOMTOM_LIMIT_BY_PAGE,
    TOMTOM_NB_FIRST_PAGES
} = process.env;

TOMTOM_URL_GET_POI = TOMTOM_URL_GET_POI.replace(":version", TOMTOM_VERSION)
TOMTOM_URL_GET_CATEGORIES = TOMTOM_URL_GET_CATEGORIES.replace(":version", TOMTOM_VERSION)
TOMTOM_HOSTEL_CATEGORY_ID = parseInt(TOMTOM_HOSTEL_CATEGORY_ID);

TOMTOM_LIMIT_BY_PAGE = parseInt(TOMTOM_LIMIT_BY_PAGE);
if (TOMTOM_LIMIT_BY_PAGE > 100)
    TOMTOM_LIMIT_BY_PAGE = 100
TOMTOM_NB_FIRST_PAGES = parseInt(TOMTOM_NB_FIRST_PAGES);
if (TOMTOM_NB_FIRST_PAGES > 20000/TOMTOM_LIMIT_BY_PAGE)
    TOMTOM_NB_FIRST_PAGES = 20000/TOMTOM_LIMIT_BY_PAGE

const nbMaxRetries = 10;

const debugging = false;
const debug = msg => debugging && console.log(msg);

async function getHostelPOIs(topLeftLat, topLeftLng, btmRightLat, btmRightLng, categorySet, ofs = 0, nbRetry = 0) {
    debug({ofs});
    const params = {
        key: TOMTOM_API_KEY,
        topLeft: [topLeftLat,topLeftLng].join(","),
        btmRight: [btmRightLat, btmRightLng].join(","),
        ofs,
        limit: TOMTOM_LIMIT_BY_PAGE,
        categorySet: categorySet ?? TOMTOM_HOSTEL_CATEGORY_ID
    };
    let {status,body} = await request(TOMTOM_URL_GET_POI+"/"+TOMTOM_HOSTEL_CATEGORY_NAME+".json?"+jsonToUrlEncoded(params));

    if (status === 429) {
        if (nbRetry >= nbMaxRetries) {
            throw new CustomError("Connection limit exceeded for TOMTOM API", params, 413);
        }
        debug("Failed retry "+nbRetry);
        return getHostelPOIs(topLeftLat, topLeftLng, btmRightLat, btmRightLng,categorySet,ofs,nbRetry+1);
    }

    if (status !== 200) {
        throw new Error("Failed fetch TOMTOM (status: "+status+") body :\n"+body);
    }

    body = JSON.parse(body);

    if (ofs > 0)
        return body.results;

    let maxPages = Math.floor(Math.min(body.summary.totalResults,2000)/TOMTOM_LIMIT_BY_PAGE)
    if (body.summary.totalResults%TOMTOM_LIMIT_BY_PAGE !== 0)
        maxPages += 1;

    const nbPages = Math.min(maxPages,TOMTOM_NB_FIRST_PAGES);

    return Promise.all((() => {
        const promises = [];
        for (let i=1;i<nbPages;i++) {
            promises.push(getHostelPOIs(topLeftLat, topLeftLng, btmRightLat, btmRightLng,categorySet,i*TOMTOM_LIMIT_BY_PAGE));
        }
        return promises
    })()).then(responses => {
        const POIs = body.results;
        for (const response of responses)
            for (const POI of response)
                POIs.push(POI);
        return POIs;
    });
}

async function getSubCategories(categoryId) {
    categoryId = parseInt(categoryId);
    const {body, status} = await request(TOMTOM_URL_GET_CATEGORIES+"?"+jsonToUrlEncoded({
        key: TOMTOM_API_KEY,
        language: TOMTOM_LANGUAGE
    }));

    if (status !== 200) {
        throw new Error("Failed fetch TOMTOM (status: "+status+") body :\n"+body);
    }

    const categories = JSON.parse(body).poiCategories;

    let subCategoriesId = null;
    const subCategories = [];

    for (const categorie of categories) {
        if (subCategoriesId === null) {
            if (categorie.id === categoryId)
                subCategoriesId = categorie.childCategoryIds;
            continue;
        }

        if (subCategoriesId.includes(categorie.id))
            subCategories.push(categorie)
    }

    return subCategories;
}

module.exports = {getSubCategories,getHostelPOIs};
