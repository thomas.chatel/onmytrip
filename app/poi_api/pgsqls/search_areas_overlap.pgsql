CREATE OR REPLACE FUNCTION search_areas_overlap(lat_1 float, lng_1 float, radius_1 float, lat_2 float, lng_2 float, radius_2 float) returns BOOLEAN AS $$
    DECLARE
        D float;
        _d float;
        _d2 float;
        h float;
        S1 float;
        S2 float;
        S float;
        S_C1 float;
        S_C2 float;
        N_S float;
    BEGIN
        D := acos(sin(radians(lat_1))*sin(radians(lat_2))+cos(radians(lat_1))*cos(radians(lat_2))*cos(radians(lng_1-lng_2)))*6371;

        if D = 0 then
            RETURN true;
        end if;

        if D > radius_1 then
            RETURN false;
        end if;

        _d := ( pow(radius_1,2) + pow(D,2) - pow(radius_2,2) ) / (2*D);
        _d2 := D-_d;
        h := sqrt(pow(radius_1,2) - pow(_d,2));
        S1 := pow(radius_1,2) * acos(_d/radius_1) - _d * h;
        S2 := pow(radius_2,2) * acos(_d2/radius_2) - _d2 * h;
        S := S1+S2;
        S_C1 := PI() * pow(radius_1,2);
        S_C2 := PI() * pow(radius_2,2);
        N_S := S_C2 - S;

        RETURN N_S/S_C1 < 0.25;
    END;
$$ LANGUAGE plpgsql
