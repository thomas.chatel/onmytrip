CREATE OR REPLACE FUNCTION round_float(n float, p integer) returns float AS $$
    DECLARE
    BEGIN
        RETURN CAST(ROUND(CAST(n as numeric), p) as float);
    END;
$$ LANGUAGE plpgsql
