const { Model } = require("sequelize");
const sequelize = require("../libs/sequelize");

class POI_SearchArea extends Model {
}

POI_SearchArea.init(
    {
    },
    {
        tableName: "POI_SearchArea",
        timestamps: false,
        sequelize, // passing the `sequelize` instance is required
    }
);

module.exports = POI_SearchArea;
