const { Model, DataTypes } = require("sequelize");
const sequelize = require("../libs/sequelize");
const SearchArea = require("./SearchArea");
const POI_SearchArea = require("./POI_SearchArea");

class POI extends Model {}

POI.init(
	{
		id: {
			type: DataTypes.STRING,
			primaryKey: true,
			allowNull: false
		},
		lat: {
			type: DataTypes.FLOAT,
			allowNull: false,
		},
		lng: {
			type: DataTypes.FLOAT,
			allowNull: false,
		},
		name: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		category: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		rank: {
			type: DataTypes.INTEGER,
			allowNull: false,
		},
		tags: {
			type: DataTypes.JSON,
			allowNull: false,
		}
	},
	{
		sequelize,
		modelName: "POI",
	}
);

POI.belongsToMany(SearchArea, {through: POI_SearchArea});
SearchArea.belongsToMany(POI, {through: POI_SearchArea});

module.exports = POI;
