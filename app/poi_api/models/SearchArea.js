const { Model, DataTypes } = require("sequelize");
const sequelize = require("../libs/sequelize");
class SearchArea extends Model {}

SearchArea.init(
	{
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true,
			allowNull: false
		},
		lat: {
			type: DataTypes.FLOAT,
			allowNull: false,
		},
		lng: {
			type: DataTypes.FLOAT,
			allowNull: false,
		},
		radius: {
			type: DataTypes.INTEGER,
			allowNull: false,
		}
	},
	{
		sequelize,
		modelName: "SearchArea",
	}
);

module.exports = SearchArea;
