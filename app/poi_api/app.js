const express = require("express");
const cors = require("cors");
const migrate = require("./libs/migrate");
const SearchArea = require("./models/SearchArea");
const POI = require("./models/POI");
const sequelize = require("./libs/sequelize");
const {where,col,fn,Op} = require("sequelize");
const {isFloat, isInteger} = require("./common_libs/converters");
const {getPoi} = require("./libs/APIConnectorAmadeus");
const {getSubCategories,getHostelPOIs} = require("./libs/APIConnectorTomTom");
const logger = require("./common_libs/logger");
const amadeusFormat = require("./pois_format/amadeus");
const tomtomFormat = require("./pois_format/tomtom");
const logErrorsMiddleware = require("./common_libs/logErrorsMiddleware");

let {SEARCH_AREA_EXPIRE_IN, TOMTOM_HOSTEL_CATEGORY_ID} = process.env;
SEARCH_AREA_EXPIRE_IN = parseInt(SEARCH_AREA_EXPIRE_IN);

migrate().then(() => {
	console.log("Database synchronized");
})

const app = express();

app.use(cors());
app.use(express.json());

app.get("/hostel_categories", (req, res, next) =>
	getSubCategories(TOMTOM_HOSTEL_CATEGORY_ID)
		.then((subCategories) => res.json(subCategories))
		.catch(e => next(e))
)

app.get("/poi/hostel", (req, res, next) => {
	let {topLeft,btmRight, categorySet} = req.query;

	let topLeftLat, topLeftLng, btmRightLat, btmRightLng;

	if (
		!topLeft ||
		!btmRight ||
		(
			([topLeftLat,topLeftLng] = topLeft.split(",").map(v => v.trim())) &&
			(
				!(topLeftLat = isFloat(topLeftLat)) ||
				!(topLeftLng = isFloat(topLeftLng))
			)
		) ||
		(
			([btmRightLat,btmRightLng] = btmRight.split(",").map(v => v.trim())) &&
			(
				!(btmRightLat = isFloat(btmRightLat)) ||
				!(btmRightLng = isFloat(btmRightLng))
			)
		))
		return res.sendStatus(400);


	getHostelPOIs(topLeftLat, topLeftLng, btmRightLat, btmRightLng, categorySet)
		.then(pois => res.json(pois.map(tomtomFormat)))
		.catch(e => next(e))
})

app.get("/poi", (req,res,next) => (async (req, res) => {
	let {lat,lng, radius} = req.query;

	if (
		!lat ||
		!lng ||
		!radius ||
		!(lat = isFloat(lat)) ||
		!(lng = isFloat(lng)) ||
		!(radius = isInteger(radius)))
		return res.sendStatus(400);

	lat = Math.round(lat*10**4)/10**4
	lng = Math.round(lng*10**4)/10**4

	await SearchArea.destroy({
		where: {
			createdAt: {
				[Op.lt]: new Date(Date.now() - SEARCH_AREA_EXPIRE_IN * 1000).toISOString()
			}
		}
	});

	await sequelize.query("DELETE FROM \"POIs\" P where (select count(*) FROM \"POI_SearchArea\" PS where P.\"id\" = PS.\"POIId\") = 0");

	let searchArea = await SearchArea.findOne({
		where: {
			[Op.and]: [
				where(col("radius"),radius),
				where(fn('search_areas_overlap', fn("round_float", col('lat'), 3), fn("round_float", col('lng'), 3), col('radius'), lat, lng, radius), true)
			]
		},
		include: POI
	});
	if (searchArea) {
		return res.json(searchArea.POIs.map(amadeusFormat));
	}

	const newPOIs = await getPoi(lat, lng, radius);

	searchArea = await SearchArea.create({
		lat,
		lng,
		radius
	});

	const existingPOIs = await Promise.all(newPOIs.map(async (newPOI) => {
		let existingPOI = await POI.findOne({where: {id: newPOI.id}});
		if (existingPOI === null) {
			try {
				existingPOI = await POI.create({
					id: newPOI.id,
					lat: newPOI.geoCode.latitude,
					lng: newPOI.geoCode.longitude,
					name: newPOI.name,
					category: newPOI.category,
					rank: newPOI.rank,
					tags: newPOI.tags
				})
			} catch (e) {
				if (e.name === "SequelizeUniqueConstraintError") {
					existingPOI = await POI.findOne({where: {id: newPOI.id}});
				} else {
					throw e;
				}
			}
		}

		await existingPOI.addSearchArea(searchArea);
		return existingPOI
	}))

	logger.info(`POIs found: ${existingPOIs.length}`);
	res.json(existingPOIs.map(amadeusFormat));
})(req,res).catch(e => next(e)));

app.use(logErrorsMiddleware);

app.listen(process.env.PORT || 3000);
