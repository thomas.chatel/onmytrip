const jwt = require('jsonwebtoken');

function generateAccessToken(user) {
    return jwt.sign({
            ...user,
            password: undefined,
            registerToken: undefined,
            passwordToken: undefined,
            updatedAt: undefined
        },
        process.env.ACCESS_TOKEN_SECRET, { expiresIn: '900s' });
}

function generateRefreshToken(user) {
    return jwt.sign({
            ...user,
            password: undefined,
            registerToken: undefined,
            passwordToken: undefined,
            updatedAt: undefined
        },
        process.env.REFRESH_TOKEN_SECRET, { expiresIn: '30d' });
}

function decodeAccessToken(token) {
    return jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        if(err) {
            throw err;
        }
        return user;
    })
}

function handleRefreshToken(token) {
    return jwt.verify(token, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
        if(err) {
            throw new Exception('Invalid token');
        }

        delete user.iat;
        delete user.exp;

        return generateAccessToken(user);
    })
}

module.exports = {
    generateAccessToken,
    generateRefreshToken,
    decodeAccessToken,
    handleRefreshToken
}
