const express = require("express");
const cors = require("cors");
const logger = require("./common_libs/logger");
const { generateAccessToken, generateRefreshToken, decodeAccessToken, handleRefreshToken } = require('./services/jwt.service');

const app = express();

app.use(cors());
app.use(express.json());

app.post('/access_token', (req, res) => {
    const access_token = generateAccessToken(req.body);
    const refresh_token = generateRefreshToken(req.body);
    res.status(200).send({ access_token, refresh_token })
});

app.get('/token', (req, res) => {
    const authHeader = req.headers.authorization;
    const token = authHeader && authHeader.split(' ')[1];

    try {
        const user = decodeAccessToken(token);
        res.status(200).send(user);
    } catch(err) {
        console.log(err);
        res.sendStatus(401);
    }
});


app.post('/refresh_token', (req, res) => {
    const authHeader = req.headers.authorization;
    const token = authHeader && authHeader.split(' ')[1];

    if(!token) {
        logger.warn(undefined, { returnCode: 401, method: 'refreshToken' })
        res.sendStatus(401);
    }

    try {
        const access_token = handleRefreshToken(token);
        logger.info(undefined, { returnCode: 200, method: 'refreshToken' })
        res.status(200).send({ access_token })
    } catch (err) {
        logger.warn(undefined, { returnCode: 401, stack: err, method: 'refreshToken' })
        res.sendStatus(401)
    }
});

app.listen(3000);