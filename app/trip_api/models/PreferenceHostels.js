const { Schema } = require("mongoose");
const { connect } = require("../Mongo");

const db = connect();

const PreferenceHostelsSchema = new Schema({
    userId: Number,
    preferences: [Number]
});

// @ts-ignore
module.exports = db.model('PreferenceHostels', PreferenceHostelsSchema);