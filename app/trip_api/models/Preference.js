const { Schema } = require("mongoose");
const { connect } = require("../Mongo");

const db = connect();

const PreferenceSchema = new Schema({
    userId: Number,
    pois: [String],
	restaurants: [String],
    hotels: [Number],
});

// @ts-ignore
module.exports = db.model('Preference', PreferenceSchema);