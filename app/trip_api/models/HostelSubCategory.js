const { Schema } = require("mongoose");
const { connect } = require("../Mongo");

const db = connect();

const HostelSubCategorySchema = new Schema({
	id: { type: Number, required: true },
	name: { type: String, required: true },
	name_lower: { type: String, required: true },
	synonyms: { type: Array, required: true }
});

// @ts-ignore
module.exports = db.model('HostelSubCategory', HostelSubCategorySchema);
