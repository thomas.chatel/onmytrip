const { Schema } = require("mongoose");
const { connect } = require("../Mongo");

const db = connect();

const TagSchema = new Schema({
	name: { type: String, required: true },
	translationFR: { type: String, required: true },
	categories: { type: Array, required: true }
});

// @ts-ignore
module.exports = db.model('Tag', TagSchema);
