const { Schema } = require("mongoose");
const { connect } = require("../Mongo");

const db = connect();

const CategorySchema = new Schema({
	name: { type: String, required: true }
});

// @ts-ignore
module.exports = db.model('Category', CategorySchema);
