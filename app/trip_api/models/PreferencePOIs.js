const { Schema } = require("mongoose");
const { connect } = require("../Mongo");

const db = connect();

const PreferencePOIsSchema = new Schema({
    userId: Number,
    preferences: [String]
});

// @ts-ignore
module.exports = db.model('PreferencePOIs', PreferencePOIsSchema);