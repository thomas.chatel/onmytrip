const initHostelSubCategories = require("../initHostelSubCategories");
const initTagsAndCategories = require("../initTagsAndCategories");
const request = require("supertest");
const app = require("../app");
const checkRoadTripResponse = require("./checkRoadtripResponse");

jest.setTimeout(30000)

jest.mock('../libs/getHostelsInRectangle', () => require("../mocks/getHostelsInRectangleAToBMocked"));

describe("Test AToB Route", () => {
    let jwt_token;
    beforeAll(async () => {
        await initHostelSubCategories();
        await initTagsAndCategories();
        jwt_token = btoa(JSON.stringify({
            id: 1,
            email: "test@test.com",
            password: "1234",
            firstname: "test",
            lastname: "test",
            gender: null,
            roles: ['USER'],
            registerToken: null,
            passwordToken: null
        }))
    })

    test("Test AToB without connection", () => {
        return request(app)
            .post("/AToB")
            .then(res => {
                expect(res.statusCode).toBe(401);
            })
    })

    test("Test AToB with bad inputs", () => {
        return Promise.all([
            {},
            {
                "from": "48.9241, 2.025",
                "to": "Ananas",
                "nbDays": 4,
                "nbStepsByDay": [
                    [1, 2],
                    ["all", 4]
                ],
                "minimumDurationByStep": "01:00",
            },
            {
                "from": "48.9241, 2.025",
                "nbDays": 4,
                "nbStepsByDay": [
                    [1, 2],
                    ["all", 4]
                ],
                "minimumDurationByStep": "01:00",
            },
            {
                "from": "48.9241, 2.025",
                "to": "46.56020538378356, 4.911140916662901"
            },
            {
                "from": "48.9241, 2.025",
                "to": "48.9544, 2.3172",
                "nbDays": "Toto",
                "nbStepsByDay": [
                    [1, 2],
                    ["all", 4]
                ],
                "minimumDurationByStep": "01:00",
            },
            {
                "from": "48.9241, 2.025",
                "to": "48.9544, 2.3172",
                "nbDays": 4,
                "nbStepsByDay": [
                    [1, 'abcd'],
                    ["all", 4]
                ],
                "minimumDurationByStep": "01:00",
            },
            {
                "from": "48.9241, 2.025",
                "to": "48.9544, 2.3172",
                "nbDays": 4,
                "nbStepsByDay": [
                    [1, 2],
                    ["all", 4]
                ],
                "minimumDurationByStep": "eufhezifh",
            }
        ].map(data =>
            request(app)
                .post("/AtoB")
                .set('Authorization', 'Bearer '+jwt_token)
                .send(data)
        )).then(responses => {
            [
                {
                    "violations":
                        [
                            {"propertyPath":"from","message":"Champs 'from' non spécifié"},
                            {"propertyPath":"to","message":"Champs 'to' non spécifié"},
                            {"propertyPath":"nbDays","message":"Champs 'nbDays' non spécifié"},
                            {"propertyPath":"nbStepsByDay","message":"Champs 'nbStepsByDay' non spécifié"},
                            {"propertyPath":"minimumDurationByStep","message":"Champs 'minimumDurationByStep' non spécifié"}
                        ]
                },
                {
                    "violations":
                        [
                            {"propertyPath":"to","message":"Point d'arrivé invalide"}
                        ]
                },
                {
                    "violations":
                        [
                            {"propertyPath":"to","message":"Champs 'to' non spécifié"}
                        ]
                },
                {
                    "violations":
                        [
                            {"propertyPath":"nbDays","message":"Champs 'nbDays' non spécifié"},
                            {"propertyPath":"nbStepsByDay","message":"Champs 'nbStepsByDay' non spécifié"},
                            {"propertyPath":"minimumDurationByStep","message":"Champs 'minimumDurationByStep' non spécifié"}
                        ]
                },
                {
                    "violations":
                        [
                            {"propertyPath":"nbDays","message":"Vous devez rentrer un entier entre 1 et 30 inclus"},
                            {"propertyPath":"nbStepsByDay","message":"Pas plus de 5 étapes par jour"}
                        ]
                },
                {
                    "violations":
                        [
                            {"propertyPath":"nbStepsByDay","message":"Pas plus de 5 étapes par jour"}
                        ]
                },
                {
                    "violations":
                        [
                            {"propertyPath":"minimumDurationByStep","message":"Vous devez spécifier une durée entre 1 demi heure et 3 heures"}
                        ]
                }
            ].forEach((expectedData,index) => {
                const res = responses[index];

                expect(res.statusCode).toBe(422);
                expect(JSON.parse(res.text)).toEqual(expectedData)
            })
        })
    })

    test("Test road trip invalid in time", () => {
        return request(app)
            .post("/AToB")
            .set('Authorization', 'Bearer '+jwt_token)
            .send({
                "from": "48.9241, 2.025",
                "to": "48.9544, 2.3172",
                "nbDays": 1,
                "nbStepsByDay": [
                    ["all", 5]
                ],
                "minimumDurationByStep": "03:00",
            })
            .then(res => {
                expect(res.statusCode).toBe(409);
            })
    })

    test("Test road trip with not found route", () => {
        return request(app)
            .post("/AToB")
            .set('Authorization', 'Bearer '+jwt_token)
            .send({
                "from": "48.9241, 2.025",
                "to": "43.2803, 5.3806",
                "nbDays": 2,
                "nbStepsByDay": [
                    [1, 2],
                    ["all", 4]
                ],
                "minimumDurationByStep": "01:00",
            })
            .then(res => {
                expect(res.statusCode).toBe(404);
            })
    })

    test("Test road trip without error", () => {
        return request(app)
            .post("/AToB")
            .set('Authorization', 'Bearer '+jwt_token)
            .send({
                "from": "48.9241, 2.025",
                "to": "48.9544, 2.3172",
                "nbDays": 2,
                "nbStepsByDay": [
                    [1, 2],
                    ["all", 4]
                ],
                "minimumDurationByStep": "01:00",
            })
            .then(res => {
                expect(res.statusCode).toBe(200);
                checkRoadTripResponse(JSON.parse(res.text), 'AToB');
            })
    })

    test("Test road trip with preferences", () => {
        const interests = [
            "park",
            "landmark",
            "historicplace",
            "historic",
            "garden",
            "attraction",
            "activities"
        ]
        const interestsRestaurant = [
            "restaurant",
            "tea",
            "sightseeing",
            "tourguide",
            "shopping",
            "attraction",
        ]
        const categoriesHostel = [
            7314003,
            7314002
        ]

        return request(app)
            .post("/AToB")
            .set('Authorization', 'Bearer '+jwt_token)
            .send({
                "from": "48.9241, 2.025",
                "to": "48.9544, 2.3172",
                "nbDays": 2,
                "nbStepsByDay": [
                    [1, 2],
                    ["all", 4]
                ],
                "minimumDurationByStep": "01:00",
                interests,
                interestsRestaurant,
                categoriesHostel
            })
            .then(res => {
                expect(res.statusCode).toBe(200);

                const json = JSON.parse(res.text);
                checkRoadTripResponse(json, 'AToB');

                expect(json.road.some(POI => !interests.some(interest => POI.tags.includes(interest)))).toBe(false)
                expect(json.restaurantPositions.some(({restaurants}) =>
                    restaurants.some(POI => !interestsRestaurant.some(interest => POI.tags.includes(interest)))
                )).toBe(false)
                expect(json.hostelPositions.some(({hostels}) =>
                    hostels.some(POI => !categoriesHostel.some(interest => POI.categorySet.includes(interest)))
                )).toBe(false)
            })
    })
})