module.exports = function checkAToBResponse(json, type /*AToB or cityTour*/) {
    const POIAmadeusCheck = {
        type: expect.any(String),
        id: expect.any(String),
        category: expect.any(String),
        lat: expect.any(Number),
        lng: expect.any(Number),
        name: expect.any(String),
        rank: expect.any(Number),
        tags: expect.any(Array),
        POIType: expect.any(String),
        nbDay: expect.any(Number)
    };

    const POITOMTOMCheck = {
        type: expect.any(String),
        id: expect.any(String),
        categories: expect.any(Array),
        categorySet: expect.any(Array),
        lat: expect.any(Number),
        lng: expect.any(Number),
        name: expect.any(String),
        POIType: expect.any(String),
        ...(type === "AToB" ? {nbDay: expect.any(Number)} : {})
    };

    expect(json).toEqual({
        ...(type === "cityTour" ? {contour: expect.any(Array)} : {}),
        road: expect.any(Array),
        restaurantPositions: expect.any(Array),
        [type === "cityTour" ? 'hostels' : 'hostelPositions']: expect.any(Array)
    });
    expect(json.road).toMatchObject((() => {
        let arr = [];
        for (let i=0;i<json.road.length;i++) {
            arr.push(POIAmadeusCheck)
        }
        return arr;
    })());
    expect(json.restaurantPositions).toMatchObject((() => {
        let arr = [];
        for (let i=0;i<json.restaurantPositions.length;i++) {
            arr.push({
                POIIndex: expect.any(Number),
                restaurants: expect.any(Array)
            })
            expect(json.restaurantPositions[i].restaurants).toMatchObject((() => {
                let arr = [];
                for (let j=0;j<json.restaurantPositions[i].restaurants.length;j++) {
                    arr.push(POIAmadeusCheck)
                }
                return arr;
            })())
        }
        return arr;
    })());

    if (type === "cityTour")
        expect(json.hostels).toMatchObject((() => {
            let arr = [];
            for (let i=0;i<json.hostels.length;i++) {
                arr.push(POITOMTOMCheck)
            }
            return arr;
        })());
    else
        expect(json.hostelPositions).toMatchObject((() => {
            let arr = [];
            for (let i=0;i<json.hostelPositions.length;i++) {
                arr.push({
                    POIIndex: expect.any(Number),
                    hostels: expect.any(Array)
                })
                expect(json.hostelPositions[i].hostels).toMatchObject((() => {
                    let arr = [];
                    for (let j=0;j<json.hostelPositions[i].hostels.length;j++) {
                        arr.push(POITOMTOMCheck)
                    }
                    return arr;
                })())
            }
            return arr;
        })())
}