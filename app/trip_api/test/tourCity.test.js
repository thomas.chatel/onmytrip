const initHostelSubCategories = require("../initHostelSubCategories");
const initTagsAndCategories = require("../initTagsAndCategories");
const request = require("supertest");
const app = require("../app");
const checkRoadTripResponse = require("./checkRoadtripResponse");

jest.setTimeout(30000);

jest.mock('../libs/getHostelsInRectangle', () => require("../mocks/getHostelsInRectangleCityTourMocked"));

describe("Test tourcity route", () => {
    let jwt_token;
    beforeAll(async () => {
        await initHostelSubCategories();
        await initTagsAndCategories();
        jwt_token = btoa(JSON.stringify({
            id: 1,
            email: "test@test.com",
            password: "1234",
            firstname: "test",
            lastname: "test",
            gender: null,
            roles: ['USER'],
            registerToken: null,
            passwordToken: null
        }))
    })

    test("Test tourcity without connection", () => {
        return request(app)
            .post("/tourCity")
            .then(res => {
                expect(res.statusCode).toBe(401);
            })
    })

    test("Test tourcity with bad inputs", () => {
        return Promise.all([
            {},
            {
                "codeCommune": "87085",
                "nbStepsByDay": [
                    ["all", 2],
                    ["3", 3]
                ],
                "minimumDurationByStep": "01:00"
            },
            {
                "codeCommune": "87085",
                "nbDays": "abcd",
                "nbStepsByDay": [
                    ["all", 2],
                    ["3", 3]
                ],
                "minimumDurationByStep": "01:00"
            },
            {
                "codeCommune": "87085",
                "nbDays": 3,
                "nbStepsByDay": [
                    ["all", 2],
                    ["abcd", 3]
                ]
            },
            {
                "codeCommune": "87085",
                "nbDays": 3,
                "nbStepsByDay": [
                    ["all", 2],
                    [64, 3]
                ],
                "minimumDurationByStep": "01:00"
            },
            {
                "codeCommune": "87085",
                "nbDays": 3,
                "nbStepsByDay": [
                    ["all", 2],
                    ["3", 3]
                ],
                "minimumDurationByStep": "trucmachin"
            }
        ].map(data =>
            request(app)
                .post("/tourCity")
                .set('Authorization', 'Bearer '+jwt_token)
                .send(data)
        )).then(responses => {

            [
                {
                    "violations":
                        [
                            {"propertyPath":"codeCommune","message":"Champs 'codeCommune' non spécifié"},
                            {"propertyPath":"nbDays","message":"Champs 'nbDays' non spécifié"},
                            {"propertyPath":"nbStepsByDay","message":"Champs 'nbStepsByDay' non spécifié"},
                            {"propertyPath":"minimumDurationByStep","message":"Champs 'minimumDurationByStep' non spécifié"}
                        ]
                },
                {
                    "violations":
                        [
                            {"propertyPath":"nbDays","message":"Champs 'nbDays' non spécifié"},
                            {"propertyPath":"nbStepsByDay","message":"Pas plus de 5 étapes par jour"}
                        ]
                },
                {
                    "violations":
                        [
                            {"propertyPath":"nbDays","message":"Vous devez rentrer un entier entre 1 et 30 inclus"},
                            {"propertyPath":"nbStepsByDay","message":"Pas plus de 5 étapes par jour"}
                        ]
                },
                {
                    "violations":
                        [
                            {"propertyPath":"nbStepsByDay","message":"Pas plus de 5 étapes par jour"},
                            {"propertyPath":"minimumDurationByStep","message":"Champs 'minimumDurationByStep' non spécifié"}
                        ]
                },
                {
                    "violations":
                        [
                            {"propertyPath":"nbStepsByDay","message":"Pas plus de 5 étapes par jour"}
                        ]
                },
                {
                    "violations":
                        [
                            {"propertyPath":"minimumDurationByStep","message":"Vous devez spécifier une durée entre 1 demi heure et 3 heures"}
                        ]
                }
            ].forEach((expectedData,index) => {
                const res = responses[index];

                expect(res.statusCode).toBe(422);
                expect(JSON.parse(res.text)).toEqual(expectedData)
            })
        })
    })

    test("Test get city tour with not found city", () => {
        return request(app)
            .post("/tourCity")
            .set('Authorization', 'Bearer '+jwt_token)
            .send({
                "codeCommune": "12345",
                "nbDays": 3,
                "nbStepsByDay": [
                    ["all", 2],
                    ["3", 3]
                ],
                "minimumDurationByStep": "01:00"
            })
            .then(res => {
                expect(res.statusCode).toBe(404);
            })
    })

    test("Test get city tour with not applicable duration", () => {
        return request(app)
            .post("/tourCity")
            .set('Authorization', 'Bearer '+jwt_token)
            .send({
                "codeCommune": "87085",
                "nbDays": 1,
                "nbStepsByDay": [
                    ["all", 5]
                ],
                "minimumDurationByStep": "03:00"
            })
            .then(res => {
                expect(res.statusCode).toBe(409);
            })
    })

    test("Test city tour without errors", () => {
        return request(app)
            .post("/tourCity")
            .set('Authorization', 'Bearer '+jwt_token)
            .send({
                "codeCommune": "87085",
                "nbDays": 3,
                "nbStepsByDay": [
                    ["all", 2],
                    ["3", 3]
                ],
                "minimumDurationByStep": "01:00"
            })
            .then(res => {
                expect(res.statusCode).toBe(200);

                checkRoadTripResponse(JSON.parse(res.text), 'cityTour');
            })
    })

    test("Test city tour with preference", () => {
        const interests = [
            "park",
            "landmark",
            "historicplace",
            "historic",
            "garden",
            "attraction",
            "activities"
        ]
        const interestsRestaurant = [
            "restaurant",
            "tea",
            "sightseeing",
            "tourguide",
            "shopping",
            "attraction",
        ]
        const categoriesHostel = [
            7314003,
            7314002
        ]

        return request(app)
            .post("/tourCity")
            .set('Authorization', 'Bearer '+jwt_token)
            .send({
                "codeCommune": "87085",
                "nbDays": 3,
                "nbStepsByDay": [
                    ["all", 2],
                    ["3", 3]
                ],
                "minimumDurationByStep": "01:00",
                interests,
                interestsRestaurant,
                categoriesHostel
            })
            .then(res => {
                expect(res.statusCode).toBe(200);

                const json = JSON.parse(res.text);

                checkRoadTripResponse(json,'cityTour');

                expect(json.road.some(POI => !interests.some(interest => POI.tags.includes(interest)))).toBe(false)
                expect(json.restaurantPositions.some(({restaurants}) =>
                    restaurants.some(POI => !interestsRestaurant.some(interest => POI.tags.includes(interest)))
                )).toBe(false)
                expect(json.hostels.some(POI => !categoriesHostel.some(interest => POI.categorySet.includes(interest)))).toBe(false)
            })
    })
})