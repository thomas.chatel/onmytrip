const initHostelSubCategories = require("../initHostelSubCategories");
const request = require("supertest");
const app = require("../app");

describe("Test all preferences routes", () => {
    let jwt_token;
    beforeAll(async () => {
        jwt_token = btoa(JSON.stringify({
            id: 1,
            email: "test@test.com",
            password: "1234",
            firstname: "test",
            lastname: "test",
            gender: null,
            roles: ['USER'],
            registerToken: null,
            passwordToken: null
        }))
        await initHostelSubCategories();
    })

    test("Test get hostel categories without connection", () => {
        return request(app)
            .get("/hostel_categories/")
            .then(res => {
                expect(res.statusCode).toBe(401);
            })
    })

    test("Test get hostel categories", () => {
        return request(app)
            .get("/hostel_categories/")
            .set('Authorization', 'Bearer '+jwt_token)
            .then(res => {
                expect(res.statusCode).toBe(200);
                expect(JSON.parse(res.text).map(({name}) => name)).toEqual(expect.arrayContaining(['Cabanes','B&B/Chambre d\'Hôtes']))
            })
    })

    test("Test search hostel categories", () => {
        return request(app)
            .get("/hostel_categories/ca")
            .set('Authorization', 'Bearer '+jwt_token)
            .then(res => {
                expect(res.statusCode).toBe(200);
                expect(JSON.parse(res.text).map(({name}) => name)).toEqual(expect.arrayContaining(['Cabanes']))
            })
    })
})