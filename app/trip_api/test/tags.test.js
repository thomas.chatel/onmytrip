const initTagsAndCategories = require("../initTagsAndCategories");
const request = require("supertest");
const app = require("../app");

describe("Test tags routes", () => {
    let jwt_token;
    beforeAll(async () => {
        jwt_token = btoa(JSON.stringify({
            id: 1,
            email: "test@test.com",
            password: "1234",
            firstname: "test",
            lastname: "test",
            gender: null,
            roles: ['USER'],
            registerToken: null,
            passwordToken: null
        }))
        await initTagsAndCategories();
    })

    test("Test get translations without connection", () => {
        return request(app)
            .get("/tags/translations")
            .then(res => {
                expect(res.statusCode).toBe(401);
            })
    })

    test("Test get translations", () => {
        return request(app)
            .get("/tags/translations")
            .set('Authorization', 'Bearer '+jwt_token)
            .then(res => {
                expect(res.statusCode).toBe(200);

                const json = JSON.parse(res.text);
                const allAreString = !Object.entries(json).some(([key,value]) => parseInt(key).toString() === key || key === "NaN" || typeof(value) !== "string");
                expect(allAreString).toBe(true);
            })
    })

    test("Test get all restaurants tags", () => {
       return request(app)
           .get("/tags/restaurant")
           .set('Authorization', 'Bearer '+jwt_token)
           .then(res => {
               expect(res.statusCode).toBe(200);
               expect(JSON.parse(res.text)).toEqual(expect.arrayContaining(["sightseeing","restaurant","sights","tourguide","landmark","historicplace"]))
           })
    });

    test("Test search restaurants tags", () => {
        return request(app)
            .get("/tags/restaurant/search/si")
            .set('Authorization', 'Bearer '+jwt_token)
            .then(res =>{
                expect(res.statusCode).toBe(200);
                expect(JSON.parse(res.text)).toEqual(expect.arrayContaining(["landmark"]))
            })
    });

    test("Test get all tags", () => {
        return request(app)
            .get("/tags")
            .set('Authorization', 'Bearer '+jwt_token)
            .then(res => {
                expect(res.statusCode).toBe(200);
                expect(JSON.parse(res.text)).toEqual(expect.arrayContaining(["sightseeing","tourguide","restaurant","sights","park","landmark","historicplace","historic"]))
            })
    })

    test("Test search tags", () => {
        return request(app)
            .get("/tags/search/hi")
            .set('Authorization', 'Bearer '+jwt_token)
            .then(res => {
                expect(res.statusCode).toBe(200);
                expect(JSON.parse(res.text)).toEqual(expect.arrayContaining(["historic","historicplace"]))
            })
    })
})