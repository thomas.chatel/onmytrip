const initHostelSubCategories = require("../initHostelSubCategories");
const request = require("supertest");
const app = require("../app");

describe("Test preferences routes", () => {
    let jwt_token = null;
    beforeAll(() => {
        jwt_token = btoa(JSON.stringify({
            id: 1,
            email: "test@test.com",
            password: "1234",
            firstname: "test",
            lastname: "test",
            gender: null,
            roles: ['USER'],
            registerToken: null,
            passwordToken: null
        }))
    })
    test("Test get poi tags without connection", () => {
        return request(app)
            .get("/preferences/1/pois")
            .then(res => {
                expect(res.statusCode).toBe(401);
            })
    })
    test("Test get poi tags on bad formed user id", () =>  {
        return request(app)
            .get("/preferences/abc/pois")
            .set('Authorization', 'Bearer '+jwt_token)
            .then(res => {
                expect(res.statusCode).toBe(400);
            })
    })
    test("Test get poi tags on wrong user id", () =>  {
        return request(app)
            .get("/preferences/2/pois")
            .set('Authorization', 'Bearer '+jwt_token)
            .then(res => {
                expect(res.statusCode).toBe(403);
            })
    })
    test("Test get poi tags", () => {
        return request(app)
            .get("/preferences/1/pois")
            .set('Authorization', 'Bearer '+jwt_token)
            .then(res => {
                expect(res.statusCode).toBe(200);
                expect(JSON.parse(res.text)).toEqual([]);
            })
    })
    test("Test put poi tags with nothing preferences", () => {
        return request(app)
            .put("/preferences/1/pois")
            .set('Authorization', 'Bearer '+jwt_token)
            .then(res => {
                expect(res.statusCode).toBe(400);
            })
    })
    test("Test put poi tags with bad formed user id", () => {
        return request(app)
            .put("/preferences/abcd/pois")
            .send({preferences: ['coucou']})
            .set('Authorization', 'Bearer '+jwt_token)
            .then(res => {
                expect(res.statusCode).toBe(400);
            })
    })
    test("Test put poi tags with wrong user id", () => {
        return request(app)
            .put("/preferences/2/pois")
            .send({preferences: ['coucou']})
            .set('Authorization', 'Bearer '+jwt_token)
            .then(res => {
                expect(res.statusCode).toBe(403);
            })
    })
    test("Test put poi/restaurants/hostels tags and get them", () => {
        const tomtomPreferencesExample = [12,2,14];
        const amadeusPreferencesExemple = ['skrzypczyk','test','toto']
        return Promise.all(['pois','hostels','restaurants'].map(type =>
            request(app)
                .put("/preferences/1/"+type)
                .send({preferences: type === 'hostels' ? tomtomPreferencesExample : amadeusPreferencesExemple})
                .set('Authorization', 'Bearer '+jwt_token)
                .then(res => {
                    expect(res.statusCode).toBe(200);

                    return request(app)
                        .get("/preferences/1/"+type)
                        .set('Authorization', 'Bearer '+jwt_token)
                })
                .then(res => {
                    expect(res.statusCode).toBe(200);
                    expect(JSON.parse(res.text)).toEqual(expect.arrayContaining(type === 'hostels' ? tomtomPreferencesExample : amadeusPreferencesExemple))
                })
        ))
    })

})