const app = require("./app");
const logger = require("./common_libs/logger");
const initTagsAndCategories = require("./initTagsAndCategories");
const initHostelSubCategories = require("./initHostelSubCategories");

initTagsAndCategories()
initHostelSubCategories()

app.listen(process.env.PORT || 3000, () =>
    logger.info(
        `Trip api server started and listening on port ${process.env.PORT || 3000}`
    )
)