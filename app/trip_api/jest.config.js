module.exports = async () => ({
    moduleNameMapper: {
        "(.*)/common_libs/security": "$1/common_libs/securityMocked",
        "(.*)/libs/tagsAndCategories/(getHostelSubCategories|getTagsAndCategories)": "$1/mocks/tagsAndCategories/$2Mocked",
        "(.*)/(getRoute|getPOIsAlongTheRoad|findPOIsInCity|getCity)": "$1/../mocks/$2Mocked",
    }
})