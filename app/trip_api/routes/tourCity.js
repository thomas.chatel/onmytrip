const CityTourValidator = require("../forms/CityTourValidator")
const getCity = require("../libs/getCity")
const logger = require("../common_libs/logger")
const metropolisSelectPOIs = require("../libs/metropolisSelectPOIs")
const isTripPossibleInDuration = require("../libs/isTripPossibleInDuration")
const simulatedAnnealingSortPOIs = require("../libs/simulatedAnnealingSortPOIs")
const findPOIsInCity = require("../libs/findPOIsInCity")
const findHostelsInCity = require("../libs/findHostelsInCity")
const metropolisSelectRestaurantsInCity = require("../libs/metropolisSelectRestaurantsInCity")
const attributeNbDayToChosenPOIs = require("../libs/attributeNbDayToChosenPOIs")
const { Router } = require("express")

const router = Router()

router.post("/", (req, res, next) =>
  (async (req, res) => {
    const cityTourValidator = new CityTourValidator(req.body)
    let violations
    if ((violations = cityTourValidator.validate()) !== true) {
      logger.error(undefined, { user: req.user.id, returnCode: 422, method: 'cityTour' })
      return res.status(422).json({ violations })
    }

    const {
      codeCommune,
      nbDays,
      nbStepsByDay,
      minimumDurationByStep,
      interests,
      interestsRestaurant,
      categoriesHostel
    } = cityTourValidator.getFields()
    logger.info(
      `Received request to create a city tour for ${nbDays} days in ${codeCommune}`
    )

    const { status, body } = await getCity(codeCommune);

    if (status !== 200) {
      logger.error(undefined, { user: req.user.id, returnCode: status, method: 'cityTour' })
      return res.sendStatus(status)
    }

    let totalNbSteps

    if (
      !(totalNbSteps = isTripPossibleInDuration(
        nbStepsByDay,
        nbDays,
        minimumDurationByStep
      ))
    ) {
      logger.warn(
        `Trip is not possible in ${nbDays} days with ${nbStepsByDay} steps by day and ${minimumDurationByStep} minimum duration by step`,
        { user: req.user.id, returnCode: 409, method: 'cityTour' }
      )
      return res.sendStatus(409)
    }

    const contour = body.contour.coordinates

    const center = {
      lat: body.centre.coordinates[1],
      lng: body.centre.coordinates[0]
    }

    const { POIs, minRank, maxRank } = await findPOIsInCity(center, contour);

    const chosenPOIs = metropolisSelectPOIs(
      totalNbSteps,
      POIs,
      interests,
      minRank,
      maxRank
    )

    if (chosenPOIs.length === 0) {
      logger.warn(`No POIs found in ${codeCommune}`, { user: req.user.id, returnCode: 404, method: 'cityTour' });
      return res.sendStatus(404);
    }

    const road = simulatedAnnealingSortPOIs(chosenPOIs)

    attributeNbDayToChosenPOIs(nbDays, nbStepsByDay, road)

    const hostels = await findHostelsInCity(
      center,
      contour,
      chosenPOIs,
      categoriesHostel
    )

    const restaurantPositions = metropolisSelectRestaurantsInCity(
      nbDays,
      nbStepsByDay,
      chosenPOIs,
      POIs,
      interestsRestaurant
    )

    logger.info(
      `City tour created for userId=${req.user
        .id} with ${chosenPOIs.length} POIs`,
        { user: req.user.id, returnCode: 200, method: 'cityTour' }
    )

    res.json({ contour, road, hostels, restaurantPositions })
  })(req, res).catch(e => next(e))
)

module.exports = router
