const getRoute = require("../libs/getRoute");
const logger = require("../common_libs/logger");
const AToBValidator = require("../forms/AtoBValidator");
const getPOIsAlongTheRoad = require("../libs/getPOIsAlongTheRoad");
const findRestaurants = require("../libs/findRestaurants");
const findHostels = require("../libs/findHostels");
const isTripPossibleInDuration = require("../libs/isTripPossibleInDuration");
const metropolisSelectPOIs = require("../libs/metropolisSelectPOIs");
const simulatedAnnealingSortPOIs = require("../libs/simulatedAnnealingSortPOIs");
const attributeNbDayToChosenPOIs = require("../libs/attributeNbDayToChosenPOIs");
const {Router} = require("express");
const router = Router();

router.post("/", (req, res, next) =>
    (async (req, res) => {
        const aToBValidator = new AToBValidator(req.body)
        let violations
        if ((violations = aToBValidator.validate()) !== true) {
            logger.error(undefined, { user: req.user.id, returnCode: 422, method: 'AtoB' })
            return res.status(422).json({violations})
        }

        const {
            from,
            to,
            nbDays,
            nbStepsByDay,
            minimumDurationByStep,
            interests,
            interestsRestaurant,
            categoriesHostel
        } = aToBValidator.getFields()
        logger.info(
            `Received request to create a tour between ${from.lat};${from.lng} and ${to.lat};${to.lng} for ${nbDays} days`
        )

        const {routeRes, status} = await getRoute(from,to);

        if (status !== 200) {
            logger.error(undefined, { user: req.user.id, returnCode: 500, method: 'AtoB' })
            return res.sendStatus(500)
        }

        if (routeRes.statusCode === 400) {
            logger.error(routeRes.message, { user: req.user.id, returnCode: 404, method: 'AtoB' })
            return res.sendStatus(404);
        }

        const routeTime = routeRes.features[0].properties.time

        let totalNbSteps
        if (
            !(totalNbSteps = isTripPossibleInDuration(
                nbStepsByDay,
                nbDays,
                minimumDurationByStep,
                routeTime
            ))
        ) {
            logger.warn(
                `Trip is not possible in ${nbDays} days with ${nbStepsByDay} steps by day and ${minimumDurationByStep} minimum duration by step`,
                { user: req.user.id, returnCode: 409, method: 'AtoB'}
            )
            return res.sendStatus(409)
        }

        const road = routeRes.features[0].geometry.coordinates[0]

        const {POIs, minRank, maxRank} = await getPOIsAlongTheRoad(road);

        logger.info(`Found ${POIs.length} POIs along the road`)

        let chosenPOIs = metropolisSelectPOIs(
            totalNbSteps,
            POIs,
            interests,
            minRank,
            maxRank,
            road
        )

        if (chosenPOIs.length === 0) {
            logger.warn(`No POIs found`, {from,to,interests,interestsRestaurant,categoriesHostel, returnCode: 400, method: 'AtoB'})
            return res.sendStatus(404);
        }

        chosenPOIs = simulatedAnnealingSortPOIs(chosenPOIs, from, to)

        attributeNbDayToChosenPOIs(nbDays, nbStepsByDay, chosenPOIs)

        const restaurantPositions = findRestaurants(
            nbDays,
            nbStepsByDay,
            chosenPOIs,
            POIs,
            interestsRestaurant
        )

        const hostelPositions = await findHostels(
            nbDays,
            nbStepsByDay,
            chosenPOIs,
            categoriesHostel,
            road
        )

        logger.info(
            `RoadTrip created for userId=${req.user.id} with ${chosenPOIs.length} steps`, { user: req.user.id, returnCode: 200, method: 'AtoB' })
        res.json({road: chosenPOIs, restaurantPositions, hostelPositions})
    })(req, res).catch(e => next(e))
)

module.exports = router
