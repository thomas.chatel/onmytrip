const {Router} = require("express");
const HostelSubCategory = require("../models/HostelSubCategory");
const {LIMIT_TAGS} = require("../config");
const router = Router();

router.get("/", (req, res, next) =>
    HostelSubCategory.find()
        .then(categories => res.json(categories))
        .catch(e => next(e))
)

router.get("/:search", (req, res, next) =>
    HostelSubCategory.find({
        $or: [
            { name_lower: { $regex: req.params.search.toLowerCase() } },
            { synonyms: { $regex: req.params.search.toLowerCase() } }
        ]
    })
        .limit(LIMIT_TAGS)
        .then(categories => res.json(categories))
        .catch(e => next(e))
)

module.exports = router;