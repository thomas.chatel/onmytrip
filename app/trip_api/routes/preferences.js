const PreferencePOIs = require("../models/PreferencePOIs");
const PreferenceHostels = require("../models/PreferenceHostels");
const PreferenceRestaurants = require("../models/PreferenceRestaurants");
const {LANGUAGE_TAGS} = require("../config");
const CustomError = require("../common_libs/CustomError");
const {isInteger} = require("../common_libs/converters");
const logger = require("../common_libs/logger")
const {Router} = require("express");
const fs = require("fs/promises");

const router = Router();

const AmadeusTypes = ['POI','restaurant']

const modelsByType = {
    hostel: PreferenceHostels,
    POI: PreferencePOIs,
    restaurant: PreferenceRestaurants
}

let translationsTags = {};

fs.readFile(__dirname + "/../translationsTags.json")
    .then(chunk => chunk.toString())
    .then(str => JSON.parse(str))
    .then(result => {
        translationsTags = result;
    })

const savePreferences = (req,res,next,type) => {
    const model = modelsByType[type]
    const {preferences} = req.body;
    let {id} = req.params;
    if (!preferences || !id || !(id = isInteger(id))) {
        logger.error(undefined, { user: req.user.id, returnCode: 400, method: 'savePreferences' })
        return res.sendStatus(400);
    }

    if (req.user.id !== id && !req.user.roles.includes("ADMIN")) {
        logger.warn(undefined, { user: req.user.id, returnCode: 403, method: 'savePreferences' })
        return res.sendStatus(403);
    }

    model.findOne({ userId: id })
        .then(
            preferenceObj => preferenceObj === null ?
                model.create({
                    userId: id,
                    preferences
                }) : ((preferenceObj.preferences = preferences) | preferenceObj.save())
        )
        .then(() => logger.info(undefined, { user: req.user.id, returnCode: 200, method: 'savePreferences' }) | res.sendStatus(200))
        .catch(e => next(new CustomError(e, {id, preferences})));
}

const getPreferences = (req,res,next,type) => {
    const { id } = req.params;

    const model = modelsByType[type];

    const AmadeusType = AmadeusTypes.includes(type);

    if(!id || !Number.isInteger(Number(id))) {
        logger.error(undefined, { user: req.user.id, returnCode: 400, method: 'getPreferences' })
        return res.sendStatus(400);
    }

    if(req.user.id === Number(id) || req.user.roles.includes("ADMIN"))
        return model.findOne({userId: id})
            .then(preferenceObj =>
                logger.info(undefined, { user: req.user.id, returnCode: 200, method: 'getPreferences' }) |
                res.status(200).json(
                    preferenceObj ?
                        preferenceObj.preferences :
                        []
                ))
            .catch(e => next(new CustomError(e, {userId: id})));
    logger.warn(undefined, { user: req.user.id, returnCode: 403, method: 'getPreferences' })
    return res.sendStatus(403);
}

router.get("/:id/pois",
    (req, res, next) =>
        getPreferences(req,res,next,'POI'));
router.put("/:id/pois",
    (req, res, next) =>
        savePreferences(req,res,next,'POI'));


router.get("/:id/hostels",
    (req, res, next) =>
        getPreferences(req,res,next,'hostel'));
router.put("/:id/hostels",
    (req, res, next) =>
        savePreferences(req,res,next,'hostel'));


router.get("/:id/restaurants",
    (req, res, next) =>
        getPreferences(req,res,next,'restaurant'));
router.put("/:id/restaurants",
    (req, res, next) =>
        savePreferences(req,res,next,'restaurant'));

module.exports = router;