const {Router} = require("express");
const Tag = require("../models/Tag");
const {RESTAURANT_CATEGORY, LIMIT_TAGS, categoriesToSkipForPoi, LANGUAGE_TAGS} = require("../config");
const Category = require("../models/Category");
const fs = require("fs/promises");
const router = Router();

let translationsTags = {};

fs.readFile(__dirname+"/../translationsTags.json")
    .then(chunk => chunk.toString())
    .then(str => JSON.parse(str))
    .then(result => {
        translationsTags = result;
    })

router.get("/translations", (req, res) =>
    res.status(200).json(translationsTags[LANGUAGE_TAGS])
)

router.get("/restaurant", (req, res, next) =>
    Tag.find({
        categories: RESTAURANT_CATEGORY
    })
        .then(tags => res.json(
                tags.map(({name}) => name)
            )
        )
        .catch(e => next(e))
)

router.get("/restaurant/search/:search", (req, res, next) =>
    Tag.find({
        ['translation' + LANGUAGE_TAGS.toUpperCase()]: {$regex: req.params.search.toLowerCase()},
        categories: RESTAURANT_CATEGORY
    })
        .limit(LIMIT_TAGS)
        .then(tags =>
            res.json(
                tags.map(({name}) => name)
            )
        )
        .catch(e => next(e))
)

router.get("/", (req, res, next) =>
    Category.find({name: {$nin: categoriesToSkipForPoi}})
        .then(tags => tags.map(({name}) => name))
        .then(categoriesToFind =>
            Tag.find({
                categories: {$in: categoriesToFind}
            })
        )
        .then(tags => tags.map(({name}) => name))
        .then(tags => res.json(tags))
        .catch(e => next(e))
)

router.get("/search/:search", (req, res, next) =>
    Category.find({name: {$nin: categoriesToSkipForPoi}})
        .then(tags => tags.map(({name}) => name))
        .then(categoriesToFind =>
            Tag.find({
                ['translation' + LANGUAGE_TAGS.toUpperCase()]: {$regex: req.params.search.toLowerCase()},
                categories: {$in: categoriesToFind}
            }).limit(LIMIT_TAGS)
        )
        .then(tags =>
            tags.map(({name}) => name)
        )
        .then(tags => res.json(tags))
        .catch(e => next(e))
)

module.exports = router;