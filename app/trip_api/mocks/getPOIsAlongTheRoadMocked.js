function getPOIsAlongTheRoad() {
    return {
        "minRank": 5,
        "maxRank": 50,
        "POIs": [
            {
                "type": "AMADEUS",
                "id": "1578638947",
                "category": "RESTAURANT",
                "lat": 48.836723,
                "lng": 2.252643,
                "name": "Frères Zhou",
                "rank": 30,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "1628582843",
                "category": "RESTAURANT",
                "lat": 48.966,
                "lng": 2.300669,
                "name": "Le Royal Shah Jahan",
                "rank": 15,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "13D98F7D12",
                "category": "SIGHTS",
                "lat": 48.84681,
                "lng": 2.337546,
                "name": "Jardin du Luxembourg",
                "rank": 5,
                "tags": [
                    "sightseeing",
                    "restaurant",
                    "tourguide",
                    "sights",
                    "park",
                    "landmark",
                    "historicplace",
                    "historic",
                    "garden",
                    "attraction",
                    "activities",
                    "hiking",
                    "beauty&spas",
                    "theater",
                    "commercialplace",
                    "nature"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "EC7AE15DF5",
                "category": "SIGHTS",
                "lat": 48.860825,
                "lng": 2.352633,
                "name": "Centre Pompidou",
                "rank": 5,
                "tags": [
                    "museum",
                    "sightseeing",
                    "restaurant",
                    "artgallerie",
                    "tourguide",
                    "sights",
                    "transport",
                    "activities",
                    "attraction",
                    "shopping",
                    "square",
                    "parking",
                    "professionalservices",
                    "bus",
                    "theater",
                    "events",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "521DE91E65",
                "category": "SIGHTS",
                "lat": 48.88672,
                "lng": 2.343002,
                "name": "Basilique du Sacré-Cœur de Montmartre",
                "rank": 5,
                "tags": [
                    "church",
                    "sightseeing",
                    "sights",
                    "temple",
                    "landmark",
                    "tourguide",
                    "restaurant",
                    "attraction",
                    "commercialplace",
                    "activities",
                    "professionalservices"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "AA8E41776E",
                "category": "SIGHTS",
                "lat": 48.852966,
                "lng": 2.349902,
                "name": "Cathédrale Notre-Dame de Paris",
                "rank": 5,
                "tags": [
                    "church",
                    "sightseeing",
                    "restaurant",
                    "tourguide",
                    "sights",
                    "temple",
                    "landmark",
                    "historicplace",
                    "historic",
                    "attraction",
                    "activities",
                    "professionalservices",
                    "sports",
                    "bike",
                    "rental",
                    "commercialplace",
                    "outdoorplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "E9C13A1913",
                "category": "RESTAURANT",
                "lat": 48.865093,
                "lng": 2.328464,
                "name": "Angelina",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "tea",
                    "sightseeing",
                    "tourguide",
                    "shopping",
                    "attraction",
                    "activities",
                    "bakery",
                    "commercialplace",
                    "transport",
                    "health&medical"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "7CBD79F4BD",
                "category": "SIGHTS",
                "lat": 48.864246,
                "lng": 2.324892,
                "name": "Jardin des Tuileries",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "landmark",
                    "sightseeing",
                    "garden",
                    "park",
                    "tourguide",
                    "sights",
                    "attraction",
                    "nature",
                    "commercialplace",
                    "professionalservices",
                    "hiking",
                    "beauty&spas",
                    "events",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "31CEFDF0ED",
                "category": "RESTAURANT",
                "lat": 48.87301,
                "lng": 2.33269,
                "name": "Galeries Lafayette Haussmann",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "shopping",
                    "tourguide",
                    "mall",
                    "clothing",
                    "fashion",
                    "attraction",
                    "activities",
                    "parking",
                    "professionalservices",
                    "taxi",
                    "liquor"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "49A0A389A7",
                "category": "RESTAURANT",
                "lat": 48.871944,
                "lng": 2.331666,
                "name": "Opéra National de Paris - Palais Garnier",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "events",
                    "attraction",
                    "activities",
                    "operahouse",
                    "sightseeing",
                    "commercialplace",
                    "theater",
                    "sights",
                    "schools",
                    "tourguide",
                    "landmark",
                    "musicvenue",
                    "hiking",
                    "bar",
                    "publicplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "827AC5CF93",
                "category": "RESTAURANT",
                "lat": 48.85677,
                "lng": 2.345903,
                "name": "Île de la Cité",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "tourguide",
                    "attraction",
                    "professionalservices",
                    "landmark",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "535BF3BF6D",
                "category": "RESTAURANT",
                "lat": 48.85028,
                "lng": 2.34232,
                "name": "Bouillon Racine",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "shopping",
                    "transport",
                    "events",
                    "brasserie"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "17CB4FD44C",
                "category": "RESTAURANT",
                "lat": 48.8963,
                "lng": 2.26971,
                "name": "Le Petit Poucet",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "shopping",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "7F851C4CD9",
                "category": "RESTAURANT",
                "lat": 48.871902,
                "lng": 2.332513,
                "name": "L'Opéra Restaurant",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "pub"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "754DB404AC",
                "category": "RESTAURANT",
                "lat": 48.872845,
                "lng": 2.363544,
                "name": "Le Verre Volé",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "bar",
                    "commercialplace",
                    "seafood",
                    "pub",
                    "bistro"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "E565F32CAE",
                "category": "SIGHTS",
                "lat": 48.925682,
                "lng": 2.359429,
                "name": "Stade de France",
                "rank": 5,
                "tags": [
                    "stadium",
                    "sightseeing",
                    "tourguide",
                    "landmark",
                    "historicplace",
                    "sights",
                    "historic",
                    "activities",
                    "sports",
                    "events",
                    "shopping",
                    "restaurant",
                    "outdoorplace",
                    "commercialplace",
                    "attraction"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "9DBE48CBDA",
                "category": "RESTAURANT",
                "lat": 48.870876,
                "lng": 2.331753,
                "name": "Café de la Paix",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "shopping",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "33EA4449E4",
                "category": "RESTAURANT",
                "lat": 48.86621,
                "lng": 2.33801,
                "name": "Le Grand Véfour",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace",
                    "professionalservices",
                    "events",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "F835637BF8",
                "category": "RESTAURANT",
                "lat": 48.882683,
                "lng": 2.360138,
                "name": "Krishna Bhavan",
                "rank": 5,
                "tags": [
                    "vegetarian",
                    "restaurant",
                    "fastfood",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "35656172E5",
                "category": "RESTAURANT",
                "lat": 48.87344,
                "lng": 2.364025,
                "name": "Hôtel du Nord",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "pub",
                    "commercialplace",
                    "activities",
                    "professionalservices",
                    "breakfast",
                    "events"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "ED0E76248C",
                "category": "RESTAURANT",
                "lat": 48.903492,
                "lng": 2.33955,
                "name": "Ma Cocotte",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "shopping",
                    "transport",
                    "events",
                    "parking",
                    "regionalcuisine"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "A26BA72447",
                "category": "SIGHTS",
                "lat": 48.9349,
                "lng": 2.35829,
                "name": "Basilique Saint Denis",
                "rank": 5,
                "tags": [
                    "church",
                    "sightseeing",
                    "sights",
                    "restaurant",
                    "historicplace",
                    "historic",
                    "tourguide",
                    "museum",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "D2E4502804",
                "category": "SIGHTS",
                "lat": 48.882435,
                "lng": 2.340198,
                "name": "La Cigale",
                "rank": 5,
                "tags": [
                    "sightseeing",
                    "theater",
                    "activities",
                    "club",
                    "attraction",
                    "restaurant",
                    "commercialplace",
                    "musicvenue",
                    "events"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "6BDF364936",
                "category": "RESTAURANT",
                "lat": 48.871685,
                "lng": 2.3646,
                "name": "Chez Prune",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "bistro",
                    "bar",
                    "nightlife",
                    "sightseeing",
                    "shopping",
                    "bodega"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "BDC3953E9A",
                "category": "RESTAURANT",
                "lat": 48.968,
                "lng": 2.24435,
                "name": "Le Moulin de la Galette",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "17445C285B",
                "category": "RESTAURANT",
                "lat": 48.894592,
                "lng": 2.268021,
                "name": "La Guinguette de Neuilly",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "A80857CBBB",
                "category": "RESTAURANT",
                "lat": 48.871944,
                "lng": 2.353966,
                "name": "Paris New York",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "burger",
                    "fastfood",
                    "liquor",
                    "transport",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "3DAB132E74",
                "category": "SHOPPING",
                "lat": 48.882397,
                "lng": 2.266591,
                "name": "Chanel",
                "rank": 5,
                "tags": [
                    "luxurybybrand",
                    "shopping",
                    "clothing",
                    "attraction",
                    "activities",
                    "tourguide",
                    "fashion",
                    "shoe",
                    "sightseeing",
                    "commercialplace",
                    "professionalservices",
                    "boutique",
                    "jewelry",
                    "landmark"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "3D3613EC93",
                "category": "RESTAURANT",
                "lat": 48.908028,
                "lng": 2.344096,
                "name": "Chez Louisette",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "bar",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "C5CF4BB431",
                "category": "RESTAURANT",
                "lat": 48.901585,
                "lng": 2.304724,
                "name": "La Romantica",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "0DDEACEAD2",
                "category": "SIGHTS",
                "lat": 48.92988,
                "lng": 2.35391,
                "name": "Paris Walks",
                "rank": 5,
                "tags": [
                    "tourguide",
                    "attraction",
                    "outdoorplace",
                    "activities",
                    "professionalservices",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "6FCE60475A",
                "category": "RESTAURANT",
                "lat": 48.96868,
                "lng": 2.309511,
                "name": "Aux Saveurs d'Alice",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "EF23FD5E54",
                "category": "RESTAURANT",
                "lat": 48.885468,
                "lng": 2.234468,
                "name": "L'Escargot",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "9D78965996",
                "category": "RESTAURANT",
                "lat": 48.85762,
                "lng": 2.43416,
                "name": "La Cave",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "shopping",
                    "liquor",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B182BF2B5B",
                "category": "RESTAURANT",
                "lat": 48.937714,
                "lng": 2.244685,
                "name": "La Ferme d'Argenteuil",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "F17AA1ED68",
                "category": "RESTAURANT",
                "lat": 48.86472,
                "lng": 2.34724,
                "name": "Osteria Ruggera",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "transport"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "3FA242022F",
                "category": "RESTAURANT",
                "lat": 48.86817,
                "lng": 2.36204,
                "name": "Le Pachyderme",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "breakfast"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "DC22AD2217",
                "category": "RESTAURANT",
                "lat": 48.962227,
                "lng": 2.260301,
                "name": "Le Palais d'Agadir",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "FC2C74EB5D",
                "category": "RESTAURANT",
                "lat": 48.91223,
                "lng": 2.17702,
                "name": "Café'in",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "bar",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "74B72D047A",
                "category": "RESTAURANT",
                "lat": 48.84156,
                "lng": 2.219077,
                "name": "Le Garde Manger",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "bistro",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "E695E51C1A",
                "category": "SIGHTS",
                "lat": 48.9466,
                "lng": 2.434603,
                "name": "Musée de l'Air et de l'Espace",
                "rank": 5,
                "tags": [
                    "museum",
                    "restaurant",
                    "professionalservices",
                    "transport",
                    "bus"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "C377371EBC",
                "category": "RESTAURANT",
                "lat": 48.817654,
                "lng": 2.283681,
                "name": "L'Amandine",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "A64CF69191",
                "category": "RESTAURANT",
                "lat": 48.87858,
                "lng": 2.414971,
                "name": "Le Triton",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "activities",
                    "events",
                    "musicvenue",
                    "sightseeing",
                    "commercialplace",
                    "fastfood",
                    "theater",
                    "shopping",
                    "transport",
                    "rental",
                    "cheap"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "ADEA886CF3",
                "category": "RESTAURANT",
                "lat": 48.83999,
                "lng": 2.245385,
                "name": "Chez Madeleine",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "shopping",
                    "patio",
                    "garden"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "991CAB4C94",
                "category": "RESTAURANT",
                "lat": 48.97177,
                "lng": 2.25736,
                "name": "Allo Angelo",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "fastfood",
                    "pizza"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "4492BF1BA0",
                "category": "RESTAURANT",
                "lat": 48.83752,
                "lng": 2.248151,
                "name": "Shiki",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "bar",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "F292D931DE",
                "category": "RESTAURANT",
                "lat": 48.83637,
                "lng": 2.251768,
                "name": "Sanki",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "CAC98FEF25",
                "category": "RESTAURANT",
                "lat": 48.818275,
                "lng": 2.322693,
                "name": "Le Physalis",
                "rank": 5,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "DF441B64C8",
                "category": "RESTAURANT",
                "lat": 48.823887,
                "lng": 2.30431,
                "name": "Le Timbre Poste",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "gastropub",
                    "sightseeing",
                    "pub",
                    "bar",
                    "commercialplace",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "D72A92BA55",
                "category": "SIGHTS",
                "lat": 48.886402,
                "lng": 2.255689,
                "name": "Pont de Neuilly",
                "rank": 5,
                "tags": [
                    "bridge",
                    "sightseeing",
                    "landmark",
                    "historicplace",
                    "sights",
                    "historic",
                    "transport",
                    "bus",
                    "train"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "8541B205A2",
                "category": "RESTAURANT",
                "lat": 48.88517,
                "lng": 2.26318,
                "name": "Le Ventadour",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "professionalservices"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "8C51A58B3B",
                "category": "SHOPPING",
                "lat": 48.864723,
                "lng": 2.336901,
                "name": "Didier Ludot",
                "rank": 5,
                "tags": [
                    "sightseeing",
                    "shopping",
                    "clothing",
                    "fashion",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "3CC3F47D74",
                "category": "SIGHTS",
                "lat": 48.86577,
                "lng": 2.321118,
                "name": "Place de la Concorde",
                "rank": 10,
                "tags": [
                    "sights",
                    "sightseeing",
                    "landmark",
                    "bridge",
                    "tourguide",
                    "square",
                    "restaurant",
                    "attraction",
                    "fountain",
                    "statue",
                    "commercialplace",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "0736828224",
                "category": "SIGHTS",
                "lat": 48.85492,
                "lng": 2.34523,
                "name": "Sainte Chapelle",
                "rank": 10,
                "tags": [
                    "church",
                    "sightseeing",
                    "temple",
                    "landmark",
                    "tourguide",
                    "sights",
                    "restaurant",
                    "museum",
                    "historicplace",
                    "historic",
                    "mosque",
                    "synagogue",
                    "buddhist",
                    "attraction",
                    "activities",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "26C043A0D6",
                "category": "RESTAURANT",
                "lat": 48.82858,
                "lng": 2.28186,
                "name": "Big Fernand",
                "rank": 5,
                "tags": [
                    "burger",
                    "fastfood",
                    "hotdogs",
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "76BE373504",
                "category": "RESTAURANT",
                "lat": 48.853725,
                "lng": 2.380827,
                "name": "Septime",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "tapas",
                    "bar",
                    "sightseeing",
                    "nightlife",
                    "shopping",
                    "commercialplace",
                    "transport",
                    "liquor"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "D37D14F68F",
                "category": "RESTAURANT",
                "lat": 48.87081,
                "lng": 2.303062,
                "name": "Ladurée",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "tea",
                    "sightseeing",
                    "bakery",
                    "shopping",
                    "liquor"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "7F1B367573",
                "category": "SIGHTS",
                "lat": 48.8828,
                "lng": 2.3432,
                "name": "Le Trianon",
                "rank": 10,
                "tags": [
                    "sightseeing",
                    "tourguide",
                    "events",
                    "activities",
                    "attraction",
                    "shopping",
                    "transport",
                    "musicvenue"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "32E0D8BE18",
                "category": "SIGHTS",
                "lat": 48.880383,
                "lng": 2.355167,
                "name": "Gare du Nord",
                "rank": 10,
                "tags": [
                    "sightseeing",
                    "transport",
                    "train",
                    "restaurant",
                    "metro",
                    "trainstation",
                    "commercialplace",
                    "attraction",
                    "activities",
                    "tourguide",
                    "bus",
                    "parking",
                    "sports",
                    "bike",
                    "rental"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "F91BB2CD8D",
                "category": "NIGHTLIFE",
                "lat": 48.87541,
                "lng": 2.36001,
                "name": "Café A",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "bar",
                    "nightlife",
                    "pub",
                    "tourguide",
                    "sightseeing",
                    "activities",
                    "attraction"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "8FF989265C",
                "category": "NIGHTLIFE",
                "lat": 48.88138,
                "lng": 2.34241,
                "name": "Artisan",
                "rank": 10,
                "tags": [
                    "bar",
                    "nightlife",
                    "tourguide",
                    "sightseeing",
                    "restaurant",
                    "attraction",
                    "activities",
                    "shopping",
                    "transport",
                    "pub"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "0C6884520C",
                "category": "RESTAURANT",
                "lat": 48.86645,
                "lng": 2.32647,
                "name": "Le Soufflé",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "04C537F7DB",
                "category": "RESTAURANT",
                "lat": 48.879364,
                "lng": 2.334599,
                "name": "Les Canailles",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "patio",
                    "garden"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "562BCC27C0",
                "category": "NIGHTLIFE",
                "lat": 48.87325,
                "lng": 2.35298,
                "name": "New Morning",
                "rank": 10,
                "tags": [
                    "activities",
                    "tourguide",
                    "cheap",
                    "sightseeing",
                    "musicvenue",
                    "restaurant",
                    "club",
                    "events",
                    "attraction",
                    "shopping",
                    "transport",
                    "bar",
                    "nightclub"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "E106C76DCD",
                "category": "RESTAURANT",
                "lat": 48.87228,
                "lng": 2.36506,
                "name": "Siseng",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "pub",
                    "shopping",
                    "transport"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B28728CF0B",
                "category": "RESTAURANT",
                "lat": 48.90316,
                "lng": 2.228776,
                "name": "Chez Cochon",
                "rank": 10,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "96BFF4FD88",
                "category": "RESTAURANT",
                "lat": 48.87791,
                "lng": 2.36513,
                "name": "El Nopal",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "fastfood",
                    "taco",
                    "commercialplace",
                    "transport",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "82663D0DB2",
                "category": "SIGHTS",
                "lat": 48.908134,
                "lng": 2.333661,
                "name": "Espace 1789",
                "rank": 10,
                "tags": [
                    "theater",
                    "activities",
                    "cine",
                    "sightseeing",
                    "commercialplace",
                    "musicvenue"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "5E6BC190E7",
                "category": "RESTAURANT",
                "lat": 48.881092,
                "lng": 2.340233,
                "name": "La Table des Anges",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "bar",
                    "commercialplace",
                    "shopping",
                    "transport"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "36CEDD2C45",
                "category": "RESTAURANT",
                "lat": 48.905407,
                "lng": 2.47793,
                "name": "Buffalo Grill",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "steakhouse",
                    "sightseeing",
                    "commercialplace",
                    "bar",
                    "grills"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "3E118817F7",
                "category": "RESTAURANT",
                "lat": 48.912807,
                "lng": 2.335321,
                "name": "Le Coq de la Maison Blanche",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "brewery",
                    "shopping",
                    "transport",
                    "patio",
                    "garden",
                    "events"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "90C542C5C9",
                "category": "RESTAURANT",
                "lat": 48.852028,
                "lng": 2.223356,
                "name": "Quai Ouest",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B95F10C103",
                "category": "RESTAURANT",
                "lat": 48.884674,
                "lng": 2.403608,
                "name": "Le Pouilly Reuilly",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "bar",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "2E2E3CCA90",
                "category": "NIGHTLIFE",
                "lat": 48.97286,
                "lng": 2.262923,
                "name": "EMB Sannois",
                "rank": 10,
                "tags": [
                    "events",
                    "activities",
                    "musicvenue",
                    "tourguide",
                    "bar",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "522DAD7089",
                "category": "RESTAURANT",
                "lat": 48.988766,
                "lng": 2.319722,
                "name": "Al Vicolo",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "bar",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "BB13ECC6F5",
                "category": "RESTAURANT",
                "lat": 48.835667,
                "lng": 2.233189,
                "name": "La Salle à Manger",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "1BF54E20D7",
                "category": "SIGHTS",
                "lat": 48.852234,
                "lng": 2.417922,
                "name": "Ubisoft",
                "rank": 10,
                "tags": [
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "32AA08AB6C",
                "category": "RESTAURANT",
                "lat": 48.844032,
                "lng": 2.236454,
                "name": "A Tavola",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "shopping",
                    "professionalservices",
                    "transport"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "AD2BB18C8D",
                "category": "RESTAURANT",
                "lat": 48.88046,
                "lng": 2.2961,
                "name": "Le Petit Verdot",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "pub",
                    "bar",
                    "transport"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "03FCCD0DD7",
                "category": "SIGHTS",
                "lat": 48.879177,
                "lng": 2.352,
                "name": "Paroisse Saint-Vincent-de-Paul",
                "rank": 10,
                "tags": [
                    "church",
                    "sightseeing",
                    "sights",
                    "historicplace",
                    "historic",
                    "attraction",
                    "road",
                    "college",
                    "university"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B4A4C580E2",
                "category": "SHOPPING",
                "lat": 48.951427,
                "lng": 2.243699,
                "name": "Riboulding",
                "rank": 10,
                "tags": [
                    "shopping",
                    "kids",
                    "games",
                    "clothing",
                    "fashion",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "43F92D9EE6",
                "category": "RESTAURANT",
                "lat": 48.864845,
                "lng": 2.37302,
                "name": "Asian Wok",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "thai"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "9A216FCB97",
                "category": "RESTAURANT",
                "lat": 48.817093,
                "lng": 2.321715,
                "name": "L'Opportuniste",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "shopping",
                    "activities",
                    "events",
                    "transport",
                    "patio",
                    "garden"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "01BE33A282",
                "category": "RESTAURANT",
                "lat": 48.956146,
                "lng": 2.270227,
                "name": "Le Moulin d'Orgemont",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "sightseeing"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "93AB7A09B0",
                "category": "SIGHTS",
                "lat": 48.863773,
                "lng": 2.34006,
                "name": "Spa THEMAE Paris",
                "rank": 10,
                "tags": [
                    "attraction",
                    "activities",
                    "tourguide",
                    "beauty&spas",
                    "health&medical",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "39E6CD1829",
                "category": "SIGHTS",
                "lat": 48.87542,
                "lng": 2.31048,
                "name": "Musée Jacquemart André",
                "rank": 15,
                "tags": [
                    "museum",
                    "restaurant",
                    "tourguide",
                    "sightseeing",
                    "sights",
                    "attraction",
                    "professionalservices",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "31451ED4F2",
                "category": "RESTAURANT",
                "lat": 48.850277,
                "lng": 2.344722,
                "name": "Rue Du Sommerard",
                "rank": 10,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "27A2979D8D",
                "category": "RESTAURANT",
                "lat": 48.871944,
                "lng": 2.343162,
                "name": "Mersea",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "seafood",
                    "fastfood",
                    "huntingandfishing"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "F0607DF3A9",
                "category": "SHOPPING",
                "lat": 48.852657,
                "lng": 2.346962,
                "name": "Shakespeare and Company",
                "rank": 15,
                "tags": [
                    "library",
                    "shopping",
                    "activities",
                    "attraction",
                    "tourguide",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "825C8054A3",
                "category": "RESTAURANT",
                "lat": 48.86598,
                "lng": 2.336669,
                "name": "Kunitoraya",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "sushi",
                    "ramen",
                    "sightseeing",
                    "transport"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "8E0ACA101B",
                "category": "SIGHTS",
                "lat": 48.855724,
                "lng": 2.345146,
                "name": "Palais de Justice de Paris",
                "rank": 15,
                "tags": [
                    "sightseeing",
                    "sights",
                    "historicplace",
                    "historic",
                    "restaurant",
                    "attraction",
                    "activities",
                    "tourguide",
                    "commercialplace",
                    "landmark"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "E4A8CDEB7F",
                "category": "SIGHTS",
                "lat": 48.866108,
                "lng": 2.312454,
                "name": "Grand Palais",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "sights",
                    "landmark",
                    "museum",
                    "tourguide",
                    "sightseeing",
                    "artgallerie",
                    "historicplace",
                    "historic",
                    "attraction",
                    "activities",
                    "transport",
                    "bus",
                    "theater",
                    "commercialplace",
                    "musicvenue"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "F5679BCB4B",
                "category": "SIGHTS",
                "lat": 48.857483,
                "lng": 2.337078,
                "name": "Institut de France",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "tourguide",
                    "landmark",
                    "sightseeing",
                    "sights",
                    "historicplace",
                    "historic",
                    "attraction",
                    "activities",
                    "education",
                    "college",
                    "university"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "D4E79A14A7",
                "category": "SIGHTS",
                "lat": 48.880898,
                "lng": 2.333279,
                "name": "Musée de la Vie Romantique",
                "rank": 15,
                "tags": [
                    "museum",
                    "tourguide",
                    "sightseeing",
                    "sights",
                    "attraction",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "F71489E496",
                "category": "RESTAURANT",
                "lat": 48.87419,
                "lng": 2.37265,
                "name": "Le Galopin",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "transport"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "AB49E506EB",
                "category": "NIGHTLIFE",
                "lat": 48.86831,
                "lng": 2.366089,
                "name": "Favela Chic",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "bar",
                    "club",
                    "nightclub",
                    "nightlife",
                    "pub",
                    "sightseeing",
                    "activities",
                    "tapas",
                    "attraction"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "BB9CB6B95C",
                "category": "RESTAURANT",
                "lat": 48.8823,
                "lng": 2.33965,
                "name": "La Fourmi",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "bar",
                    "attraction",
                    "brewery",
                    "activities",
                    "transport"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "033D562F07",
                "category": "RESTAURANT",
                "lat": 48.871674,
                "lng": 2.368088,
                "name": "Le Petit Cambodge",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "sightseeing"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "FD5A35F3D4",
                "category": "NIGHTLIFE",
                "lat": 48.88165,
                "lng": 2.33745,
                "name": "Lulu White",
                "rank": 15,
                "tags": [
                    "bar",
                    "nightlife",
                    "sightseeing",
                    "club",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "F9BD85E350",
                "category": "SIGHTS",
                "lat": 48.8715,
                "lng": 2.330282,
                "name": "Musée du Parfum Fragonard",
                "rank": 15,
                "tags": [
                    "museum",
                    "restaurant",
                    "tourguide",
                    "sightseeing",
                    "sights",
                    "activities",
                    "shopping",
                    "attraction",
                    "perfume",
                    "clothing",
                    "fashion",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "306FA2D646",
                "category": "RESTAURANT",
                "lat": 48.93342,
                "lng": 2.298684,
                "name": "L'Ambassade des Terroirs",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "23F0483070",
                "category": "RESTAURANT",
                "lat": 48.90739,
                "lng": 2.240952,
                "name": "Le Saint Joseph",
                "rank": 15,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "79BA46BCBC",
                "category": "RESTAURANT",
                "lat": 48.87217,
                "lng": 2.35968,
                "name": "Da Mimmo",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "fastfood",
                    "pizza",
                    "sightseeing",
                    "transport",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "E74CB98FD4",
                "category": "RESTAURANT",
                "lat": 48.88421,
                "lng": 2.25957,
                "name": "La Boutarde",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "F3393513A3",
                "category": "SIGHTS",
                "lat": 48.9349,
                "lng": 2.35829,
                "name": "Basilica Cathedral of Saint Denis",
                "rank": 15,
                "tags": [
                    "church",
                    "temple",
                    "sightseeing",
                    "attraction",
                    "activities",
                    "tourguide",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "2FBD64D8AB",
                "category": "NIGHTLIFE",
                "lat": 48.852264,
                "lng": 2.188687,
                "name": "Golf De Saint Cloud",
                "rank": 15,
                "tags": [
                    "golf",
                    "events",
                    "club",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "AD8587EFED",
                "category": "RESTAURANT",
                "lat": 48.963554,
                "lng": 2.297461,
                "name": "La Palmeraie",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "4F92F03097",
                "category": "NIGHTLIFE",
                "lat": 48.905136,
                "lng": 2.339157,
                "name": "Mains d'Oeuvres",
                "rank": 15,
                "tags": [
                    "activities",
                    "professionalservices",
                    "musicvenue",
                    "artgallerie",
                    "bar",
                    "restaurant",
                    "sightseeing",
                    "commercialplace",
                    "theater"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "50F23D9E87",
                "category": "RESTAURANT",
                "lat": 48.9091,
                "lng": 2.332005,
                "name": "La Puce",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "A69CA0A4E8",
                "category": "RESTAURANT",
                "lat": 48.96782,
                "lng": 2.305906,
                "name": "Da Giovanni",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "FCC7CABFCC",
                "category": "RESTAURANT",
                "lat": 48.989193,
                "lng": 2.318901,
                "name": "La Terrasse",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "creperie"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "5A84113D5A",
                "category": "SIGHTS",
                "lat": 48.946674,
                "lng": 2.434124,
                "name": "Air and Space Museum",
                "rank": 15,
                "tags": [
                    "museum",
                    "tourguide",
                    "attraction",
                    "activities",
                    "sightseeing",
                    "commercialplace",
                    "shopping",
                    "restaurant",
                    "cheap",
                    "events"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "8C6F677C9C",
                "category": "SIGHTS",
                "lat": 48.841667,
                "lng": 2.421667,
                "name": "Lac de Saint Mandé",
                "rank": 15,
                "tags": [
                    "lake",
                    "landmark",
                    "historicplace",
                    "sightseeing",
                    "sights",
                    "historic",
                    "attraction",
                    "nature",
                    "outdoorplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "17CAC0E3D5",
                "category": "NIGHTLIFE",
                "lat": 48.858368,
                "lng": 2.434087,
                "name": "Le Chinois",
                "rank": 15,
                "tags": [
                    "bar",
                    "events",
                    "activities",
                    "professionalservices",
                    "shopping",
                    "transport"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "84E4F3AF3A",
                "category": "RESTAURANT",
                "lat": 48.969723,
                "lng": 2.262977,
                "name": "Yashito",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "fastfood",
                    "hotdogs",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "2B0251355C",
                "category": "RESTAURANT",
                "lat": 48.852287,
                "lng": 2.423804,
                "name": "L'Atelier",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "DCD9DFB0D3",
                "category": "SIGHTS",
                "lat": 48.826576,
                "lng": 2.225209,
                "name": "Goom Radio",
                "rank": 15,
                "tags": [
                    "transport",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "71E00CABE6",
                "category": "RESTAURANT",
                "lat": 48.843166,
                "lng": 2.236692,
                "name": "Violette et François",
                "rank": 15,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "EF25879A72",
                "category": "RESTAURANT",
                "lat": 48.83159,
                "lng": 2.25546,
                "name": "Volfoni",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "bar",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "63F1BC5FF6",
                "category": "SIGHTS",
                "lat": 48.84002,
                "lng": 2.224639,
                "name": "ESSCA",
                "rank": 15,
                "tags": [
                    "schools",
                    "university",
                    "sightseeing",
                    "commercialplace",
                    "college",
                    "education"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "57F73C7C25",
                "category": "SIGHTS",
                "lat": 48.813534,
                "lng": 2.297784,
                "name": "Axione",
                "rank": 15,
                "tags": [
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "A45C428A42",
                "category": "RESTAURANT",
                "lat": 48.86474,
                "lng": 2.34817,
                "name": "Kodama",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "tea",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "26298CFEC1",
                "category": "SHOPPING",
                "lat": 48.821335,
                "lng": 2.312576,
                "name": "Monoprix",
                "rank": 15,
                "tags": [
                    "shopping",
                    "supermarket",
                    "food",
                    "mall",
                    "sightseeing",
                    "commercialplace",
                    "liquor"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "742AD56AA0",
                "category": "RESTAURANT",
                "lat": 48.823402,
                "lng": 2.28901,
                "name": "Le Petit Vanves",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "grills",
                    "bar",
                    "shopping",
                    "regionalcuisine"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "7BCF2B8C47",
                "category": "RESTAURANT",
                "lat": 48.8622,
                "lng": 2.340048,
                "name": "La couleur des Blés",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "bakery"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B7B5F7E242",
                "category": "RESTAURANT",
                "lat": 48.83552,
                "lng": 2.406,
                "name": "McDonald's",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "fastfood",
                    "hotdogs",
                    "burger",
                    "sandwich",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "4A016E0260",
                "category": "SHOPPING",
                "lat": 48.864735,
                "lng": 2.347617,
                "name": "en selle Marcel",
                "rank": 15,
                "tags": [
                    "bike",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "9442FB017A",
                "category": "RESTAURANT",
                "lat": 48.851704,
                "lng": 2.356713,
                "name": "Berthillon",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "tourguide",
                    "sightseeing",
                    "shopping",
                    "icecream",
                    "fastfood",
                    "transport",
                    "tea",
                    "attraction",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "AB3D9357E0",
                "category": "NIGHTLIFE",
                "lat": 48.86298,
                "lng": 2.364,
                "name": "Candelaria",
                "rank": 30,
                "tags": [
                    "bar",
                    "restaurant",
                    "pub",
                    "sightseeing",
                    "nightlife",
                    "attraction",
                    "commercialplace",
                    "club",
                    "activities",
                    "fastfood"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "2F90DF9226",
                "category": "RESTAURANT",
                "lat": 48.857292,
                "lng": 2.357423,
                "name": "Les Philosophes",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "shopping",
                    "transport",
                    "patio",
                    "garden",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "54B0AD2C56",
                "category": "RESTAURANT",
                "lat": 48.844913,
                "lng": 2.373478,
                "name": "Le Train Bleu",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "brewery",
                    "pub",
                    "bar",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "5CC12248C3",
                "category": "SHOPPING",
                "lat": 48.851074,
                "lng": 2.324685,
                "name": "Le Bon Marché",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "shopping",
                    "sightseeing",
                    "tourguide",
                    "commercialplace",
                    "mall",
                    "food",
                    "health&medical",
                    "supermarket",
                    "attraction",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "07B3108E44",
                "category": "RESTAURANT",
                "lat": 48.860386,
                "lng": 2.293561,
                "name": "Bateaux Parisiens",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "boat",
                    "tourguide",
                    "professionalservices",
                    "attraction",
                    "activities",
                    "transport",
                    "shopping",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "D84C1059B8",
                "category": "SIGHTS",
                "lat": 48.869648,
                "lng": 2.308196,
                "name": "Avenue des Champs Élysées",
                "rank": 30,
                "tags": [
                    "sights",
                    "sightseeing",
                    "landmark",
                    "restaurant",
                    "tourguide",
                    "historicplace",
                    "historic",
                    "street",
                    "shopping",
                    "mall",
                    "road",
                    "latte",
                    "attraction",
                    "activities",
                    "parking",
                    "transport"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "0EF68EC5E6",
                "category": "RESTAURANT",
                "lat": 48.85304,
                "lng": 2.338731,
                "name": "Le Procope",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "professionalservices",
                    "bar",
                    "commercialplace",
                    "transport",
                    "events",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "C6E3B70C77",
                "category": "NIGHTLIFE",
                "lat": 48.8692,
                "lng": 2.332068,
                "name": "Harry's New York Bar",
                "rank": 30,
                "tags": [
                    "bar",
                    "nightlife",
                    "sightseeing",
                    "tourguide",
                    "activities",
                    "attraction",
                    "pub",
                    "professionalservices",
                    "club",
                    "commercialplace",
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "F127325321",
                "category": "RESTAURANT",
                "lat": 48.844627,
                "lng": 2.341685,
                "name": "Les Papilles",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "bistro",
                    "sightseeing",
                    "bar",
                    "commercialplace",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "7F0581661E",
                "category": "SIGHTS",
                "lat": 48.864586,
                "lng": 2.296674,
                "name": "Palais de Tokyo",
                "rank": 30,
                "tags": [
                    "tourguide",
                    "sightseeing",
                    "museum",
                    "restaurant",
                    "sights",
                    "historicplace",
                    "historic",
                    "palace",
                    "artgallerie",
                    "library",
                    "attraction",
                    "activities",
                    "transport",
                    "shopping",
                    "cheap",
                    "bus",
                    "commercialplace",
                    "culturalcenter"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B38EEF1109",
                "category": "RESTAURANT",
                "lat": 48.866207,
                "lng": 2.328473,
                "name": "Carré des Feuillants",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "EE93787BD3",
                "category": "RESTAURANT",
                "lat": 48.853966,
                "lng": 2.332713,
                "name": "Café de Flore",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "nightlife",
                    "sightseeing",
                    "tourguide",
                    "attraction",
                    "commercialplace",
                    "fastfood",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "0D67E644ED",
                "category": "SIGHTS",
                "lat": 48.85654,
                "lng": 2.342508,
                "name": "Place Dauphine",
                "rank": 30,
                "tags": [
                    "sightseeing",
                    "tourguide",
                    "sights",
                    "historicplace",
                    "historic",
                    "restaurant",
                    "attraction",
                    "square",
                    "activities",
                    "commercialplace",
                    "beauty&spas",
                    "park"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "89EE626242",
                "category": "SIGHTS",
                "lat": 48.856674,
                "lng": 2.339038,
                "name": "Monnaie de Paris",
                "rank": 30,
                "tags": [
                    "museum",
                    "sightseeing",
                    "sights",
                    "historicplace",
                    "historic",
                    "restaurant",
                    "commercialplace",
                    "shopping",
                    "transport"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "15E3B8BA88",
                "category": "RESTAURANT",
                "lat": 48.86617,
                "lng": 2.32709,
                "name": "L'Ardoise",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "transport",
                    "bistro"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "A1657C2872",
                "category": "SIGHTS",
                "lat": 48.86605,
                "lng": 2.32327,
                "name": "Jeu de Paume",
                "rank": 30,
                "tags": [
                    "museum",
                    "artgallerie",
                    "tourguide",
                    "sightseeing",
                    "restaurant",
                    "sights",
                    "attraction",
                    "events",
                    "activities",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "D124FAD899",
                "category": "RESTAURANT",
                "lat": 48.86644,
                "lng": 2.33201,
                "name": "L'Absinthe",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "pub",
                    "bistro",
                    "transport"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "57DEEF897F",
                "category": "RESTAURANT",
                "lat": 48.8664,
                "lng": 2.33174,
                "name": "Le Pain Quotidien",
                "rank": 30,
                "tags": [
                    "vegetarian",
                    "restaurant",
                    "breakfast",
                    "shopping",
                    "transport",
                    "patio",
                    "garden",
                    "bakery"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "557282A6E4",
                "category": "RESTAURANT",
                "lat": 48.86604,
                "lng": 2.3353,
                "name": "Aki",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "sushi",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "EDCBF93AF3",
                "category": "RESTAURANT",
                "lat": 48.86602,
                "lng": 2.32504,
                "name": "Flottes",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "bar",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "AC25A04B7B",
                "category": "RESTAURANT",
                "lat": 48.86621,
                "lng": 2.33947,
                "name": "Galerie Vivienne",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sights",
                    "tourguide",
                    "shopping",
                    "attraction",
                    "historicplace",
                    "latte",
                    "activities",
                    "landmark",
                    "mall",
                    "artgallerie",
                    "professionalservices",
                    "building",
                    "transport"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "604D254932",
                "category": "RESTAURANT",
                "lat": 48.866604,
                "lng": 2.335808,
                "name": "Higuma",
                "rank": 30,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "C2F47EACBD",
                "category": "RESTAURANT",
                "lat": 48.870937,
                "lng": 2.333777,
                "name": "Le Grand Café Capucines",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "seafood"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "DA1D82C749",
                "category": "RESTAURANT",
                "lat": 48.866367,
                "lng": 2.338369,
                "name": "Willi's Wine Bar",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "bar",
                    "transport",
                    "professionalservices",
                    "shopping",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "16674C2E2C",
                "category": "RESTAURANT",
                "lat": 48.868,
                "lng": 2.32561,
                "name": "Goumard",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "seafood",
                    "sightseeing",
                    "commercialplace",
                    "transport",
                    "events",
                    "parking",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "2D285587FD",
                "category": "SHOPPING",
                "lat": 48.87267,
                "lng": 2.33085,
                "name": "Uniqlo",
                "rank": 30,
                "tags": [
                    "clothing",
                    "shopping",
                    "fashion",
                    "sightseeing",
                    "commercialplace",
                    "kids"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "5F7AFDDDBD",
                "category": "RESTAURANT",
                "lat": 48.87948,
                "lng": 2.352189,
                "name": "Chez Michel",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "shopping",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "1023D99EFE",
                "category": "RESTAURANT",
                "lat": 48.872665,
                "lng": 2.363525,
                "name": "La Patache",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "bar",
                    "activities",
                    "transport",
                    "events",
                    "shopping",
                    "brewery"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "D4AD4E358C",
                "category": "NIGHTLIFE",
                "lat": 48.8741,
                "lng": 2.375239,
                "name": "Café Chéri",
                "rank": 30,
                "tags": [
                    "sightseeing",
                    "bar",
                    "restaurant",
                    "commercialplace",
                    "attraction",
                    "pub",
                    "activities",
                    "events"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B4FB38F620",
                "category": "RESTAURANT",
                "lat": 48.88277,
                "lng": 2.359309,
                "name": "Dishny",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "transport",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B11BB58D37",
                "category": "SIGHTS",
                "lat": 48.85715,
                "lng": 2.34084,
                "name": "Vedettes du Pont Neuf",
                "rank": 30,
                "tags": [
                    "tourguide",
                    "sightseeing",
                    "attraction",
                    "activities",
                    "professionalservices",
                    "commercialplace",
                    "transport",
                    "boat",
                    "education",
                    "shopping",
                    "sights",
                    "historicplace",
                    "historic"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "AF068CFD69",
                "category": "RESTAURANT",
                "lat": 48.87218,
                "lng": 2.366182,
                "name": "Le Cambodge",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "fastfood"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "F9FD9AC2AD",
                "category": "RESTAURANT",
                "lat": 48.87771,
                "lng": 2.33739,
                "name": "Sizin",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "grills",
                    "transport",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "A5A62E32DC",
                "category": "RESTAURANT",
                "lat": 48.878075,
                "lng": 2.330117,
                "name": "Bourgogne Sud",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "publicplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "51161176A9",
                "category": "RESTAURANT",
                "lat": 48.8795,
                "lng": 2.33708,
                "name": "Les Affranchis",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "shopping",
                    "professionalservices"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "9B02D962C2",
                "category": "RESTAURANT",
                "lat": 48.870586,
                "lng": 2.365333,
                "name": "La Marine",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "bistro",
                    "shopping",
                    "bar"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "DDB8DA5854",
                "category": "RESTAURANT",
                "lat": 48.8811,
                "lng": 2.34147,
                "name": "La Pizzetta",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "fastfood",
                    "sightseeing",
                    "commercialplace",
                    "transport",
                    "shopping",
                    "pizza"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "38508E5DBD",
                "category": "RESTAURANT",
                "lat": 48.87862,
                "lng": 2.36419,
                "name": "Le Chansonnier",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "activities",
                    "events",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "5CA5F84C0B",
                "category": "RESTAURANT",
                "lat": 48.87188,
                "lng": 2.366473,
                "name": "Chez Marie Louise",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "bistro"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "0AA60D3E33",
                "category": "SIGHTS",
                "lat": 48.87479,
                "lng": 2.35371,
                "name": "Le Manoir de Paris",
                "rank": 30,
                "tags": [
                    "sightseeing",
                    "activities",
                    "attraction",
                    "landmark",
                    "historicplace",
                    "sights",
                    "historic",
                    "theater",
                    "tourguide",
                    "shopping",
                    "cheap"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "1279035C76",
                "category": "RESTAURANT",
                "lat": 48.8807,
                "lng": 2.334863,
                "name": "Bus Palladium",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "events",
                    "activities",
                    "attraction",
                    "club",
                    "dance",
                    "musicvenue"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "89146D618E",
                "category": "RESTAURANT",
                "lat": 48.87135,
                "lng": 2.32705,
                "name": "Le Comptoir Baulois",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "shopping",
                    "food",
                    "bar"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "5AEC4D3E6A",
                "category": "RESTAURANT",
                "lat": 48.871475,
                "lng": 2.328025,
                "name": "Les Bacchantes",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "transport",
                    "shopping",
                    "bar"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "6CCAB73C11",
                "category": "RESTAURANT",
                "lat": 48.87053,
                "lng": 2.218387,
                "name": "Au Père Lapin",
                "rank": 30,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "ED44C62F9F",
                "category": "RESTAURANT",
                "lat": 48.91116,
                "lng": 2.324988,
                "name": "Block'Out",
                "rank": 30,
                "tags": [
                    "sports",
                    "restaurant",
                    "activities",
                    "gym",
                    "health&medical",
                    "attraction",
                    "tourguide",
                    "beauty&spas",
                    "sightseeing",
                    "commercialplace",
                    "shopping",
                    "cheap",
                    "climbing",
                    "pub"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "4EEB0D596B",
                "category": "SHOPPING",
                "lat": 48.902332,
                "lng": 2.34204,
                "name": "Marché aux puces de Saint Ouen",
                "rank": 30,
                "tags": [
                    "market",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "F1140AFF86",
                "category": "RESTAURANT",
                "lat": 48.84878,
                "lng": 2.42192,
                "name": "Thaun Kroûn",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "thai"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "8E543B5649",
                "category": "RESTAURANT",
                "lat": 49.033016,
                "lng": 2.355071,
                "name": "Le clovis",
                "rank": 30,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "8ED7D9CFF3",
                "category": "RESTAURANT",
                "lat": 48.968586,
                "lng": 2.304047,
                "name": "Fouquet's Enghien les Bains",
                "rank": 30,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "9E79430370",
                "category": "RESTAURANT",
                "lat": 48.853413,
                "lng": 2.434724,
                "name": "Le Jardin de Montreuil",
                "rank": 30,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "BB08D314C5",
                "category": "RESTAURANT",
                "lat": 48.93545,
                "lng": 2.35819,
                "name": "Le Mets du Roy",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "fastfood",
                    "pizza",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "990E2C82EB",
                "category": "SHOPPING",
                "lat": 48.859,
                "lng": 2.436448,
                "name": "Folies d'Encre Chantefable",
                "rank": 30,
                "tags": [
                    "library",
                    "shopping",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "509644C7ED",
                "category": "RESTAURANT",
                "lat": 48.85515,
                "lng": 2.422966,
                "name": "My Food",
                "rank": 30,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "952434F2C9",
                "category": "RESTAURANT",
                "lat": 48.909733,
                "lng": 2.447691,
                "name": "La Molisana",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace",
                    "fastfood",
                    "pizza"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "707C6C6ECC",
                "category": "SIGHTS",
                "lat": 48.853188,
                "lng": 2.36915,
                "name": "Place de la Bastille",
                "rank": 50,
                "tags": [
                    "sightseeing",
                    "sights",
                    "landmark",
                    "tourguide",
                    "historicplace",
                    "historic",
                    "attraction",
                    "square",
                    "activities",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "D3DB61B0F8",
                "category": "RESTAURANT",
                "lat": 48.863495,
                "lng": 2.343843,
                "name": "Au Pied de Cochon",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "transport",
                    "patio",
                    "garden",
                    "bar",
                    "commercialplace",
                    "brasserie"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "A84073DB56",
                "category": "RESTAURANT",
                "lat": 48.86487,
                "lng": 2.381824,
                "name": "La Fine Mousse",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "gastropub",
                    "bar",
                    "pub",
                    "nightlife",
                    "liquor",
                    "attraction",
                    "shopping",
                    "transport",
                    "patio",
                    "garden",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "C387269113",
                "category": "RESTAURANT",
                "lat": 48.868034,
                "lng": 2.325981,
                "name": "Le Baudelaire",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "nightlife",
                    "bar"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "9DA30EF612",
                "category": "RESTAURANT",
                "lat": 48.8719,
                "lng": 2.340029,
                "name": "Le Cardinal",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "breakfast",
                    "pub",
                    "bar",
                    "shopping",
                    "sports"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "5AB0B48A36",
                "category": "RESTAURANT",
                "lat": 48.878174,
                "lng": 2.332031,
                "name": "Momoka",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "transport",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "AB910E08B2",
                "category": "RESTAURANT",
                "lat": 48.86498,
                "lng": 2.335903,
                "name": "Takara",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "vegetarian",
                    "sushi"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "2A3696BD0A",
                "category": "RESTAURANT",
                "lat": 48.88021,
                "lng": 2.334707,
                "name": "Peco Peco",
                "rank": 50,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "76E7AA72A0",
                "category": "RESTAURANT",
                "lat": 48.990597,
                "lng": 2.16762,
                "name": "Le Triskell",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "creperie"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "512BB29303",
                "category": "RESTAURANT",
                "lat": 48.894306,
                "lng": 2.132616,
                "name": "La Maison de Kyoto",
                "rank": 15,
                "tags": [
                    "restaurant",
                    "sushi",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B3423EC54F",
                "category": "RESTAURANT",
                "lat": 49.032722,
                "lng": 2.339179,
                "name": "Mont Fuji",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "commercialplace",
                    "sightseeing"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "86CBCA40BA",
                "category": "RESTAURANT",
                "lat": 48.992428,
                "lng": 2.199382,
                "name": "Good Time",
                "rank": 30,
                "tags": [
                    "fastfood",
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "CF303D3E06",
                "category": "RESTAURANT",
                "lat": 48.87259,
                "lng": 2.35287,
                "name": "Brasserie Flo",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "brewery",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "4C4B5A3A6C",
                "category": "SIGHTS",
                "lat": 48.87796,
                "lng": 2.334488,
                "name": "Musée national Gustave Moreau",
                "rank": 30,
                "tags": [
                    "museum",
                    "sightseeing",
                    "artgallerie",
                    "sights",
                    "attraction"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "59F6A159E5",
                "category": "NIGHTLIFE",
                "lat": 48.878387,
                "lng": 2.33005,
                "name": "Casino de Paris",
                "rank": 30,
                "tags": [
                    "club",
                    "theater",
                    "activities",
                    "attraction",
                    "tourguide",
                    "events",
                    "casino"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "A29E2C8B39",
                "category": "RESTAURANT",
                "lat": 48.899174,
                "lng": 2.284298,
                "name": "Laure et Thomas Chartier",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "bistro",
                    "brasserie"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "7325FA866F",
                "category": "SHOPPING",
                "lat": 48.92392,
                "lng": 2.326596,
                "name": "Qwartz",
                "rank": 30,
                "tags": [
                    "shopping",
                    "mall",
                    "attraction",
                    "activities",
                    "tourguide",
                    "clothing",
                    "parking"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "59216B116E",
                "category": "SIGHTS",
                "lat": 48.903584,
                "lng": 2.363632,
                "name": "EICAR",
                "rank": 30,
                "tags": [
                    "university",
                    "college",
                    "schools",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "8FA6DD676B",
                "category": "RESTAURANT",
                "lat": 48.846817,
                "lng": 2.428114,
                "name": "La Cantine",
                "rank": 30,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "EEA3D77627",
                "category": "RESTAURANT",
                "lat": 48.853127,
                "lng": 2.423563,
                "name": "L'Amourette",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "professionalservices",
                    "transport",
                    "breakfast"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "0A0A327167",
                "category": "RESTAURANT",
                "lat": 48.90326,
                "lng": 2.480518,
                "name": "Le Chêne",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "bar",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "E14248A52C",
                "category": "SIGHTS",
                "lat": 48.94243,
                "lng": 2.246575,
                "name": "Basilique Saint Denys",
                "rank": 30,
                "tags": [
                    "sights",
                    "sightseeing",
                    "temple",
                    "church",
                    "landmark",
                    "historicplace",
                    "historic",
                    "shopping",
                    "attraction",
                    "activities",
                    "tourguide",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "0D36D045B1",
                "category": "SIGHTS",
                "lat": 48.841705,
                "lng": 2.220284,
                "name": "Alter Way",
                "rank": 30,
                "tags": [
                    "sightseeing",
                    "commercialplace",
                    "education"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "0DDA7710AB",
                "category": "SIGHTS",
                "lat": 48.841316,
                "lng": 2.223706,
                "name": "Pont de Saint Cloud",
                "rank": 30,
                "tags": [
                    "bridge",
                    "sightseeing",
                    "landmark",
                    "commercialplace",
                    "sights",
                    "historicplace",
                    "historic",
                    "transport",
                    "bus"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "7FF0AFDAD8",
                "category": "RESTAURANT",
                "lat": 48.83691,
                "lng": 2.24071,
                "name": "Samaya",
                "rank": 30,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "F9838C3D68",
                "category": "SIGHTS",
                "lat": 48.812885,
                "lng": 2.303995,
                "name": "NetPME",
                "rank": 30,
                "tags": [
                    "professionalservices",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "3DE9521D1F",
                "category": "RESTAURANT",
                "lat": 48.851948,
                "lng": 2.338622,
                "name": "L'Avant Comptoir",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "tapas",
                    "seafood",
                    "bar",
                    "cheap",
                    "bistro"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "7E18954DED",
                "category": "SIGHTS",
                "lat": 48.837284,
                "lng": 2.33192,
                "name": "Fondation Cartier pour l'Art Contemporain",
                "rank": 50,
                "tags": [
                    "luxurybybrand",
                    "museum",
                    "artgallerie",
                    "sightseeing",
                    "tourguide",
                    "sights",
                    "attraction",
                    "commercialplace",
                    "activities",
                    "shopping",
                    "transport"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "AFD25541A5",
                "category": "RESTAURANT",
                "lat": 48.86513,
                "lng": 2.330499,
                "name": "Uma",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "bar",
                    "activities",
                    "shopping",
                    "transport",
                    "events",
                    "professionalservices"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "223349C867",
                "category": "RESTAURANT",
                "lat": 48.86695,
                "lng": 2.32468,
                "name": "Lescure",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "transport",
                    "patio",
                    "garden"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "9AF280D2DE",
                "category": "RESTAURANT",
                "lat": 48.87322,
                "lng": 2.3387,
                "name": "Au Petit Riche",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "professionalservices",
                    "shopping",
                    "transport",
                    "events"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "7236433D8C",
                "category": "NIGHTLIFE",
                "lat": 48.86795,
                "lng": 2.32888,
                "name": "Bar Hemingway",
                "rank": 50,
                "tags": [
                    "bar",
                    "sightseeing",
                    "club",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "658A5C46D1",
                "category": "RESTAURANT",
                "lat": 48.87403,
                "lng": 2.36396,
                "name": "La Cantine de Quentin",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "coffee",
                    "tea",
                    "sightseeing"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "E998B51DBD",
                "category": "RESTAURANT",
                "lat": 48.876106,
                "lng": 2.358669,
                "name": "La Strasbourgeoise",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "fastfood"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "58363A0476",
                "category": "RESTAURANT",
                "lat": 48.99222,
                "lng": 2.169519,
                "name": "Ô Puits",
                "rank": 10,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "CFB0499DF1",
                "category": "SHOPPING",
                "lat": 48.99986,
                "lng": 2.18744,
                "name": "Decathlon",
                "rank": 15,
                "tags": [
                    "shopping",
                    "sport",
                    "sports",
                    "activities",
                    "bike",
                    "clothing",
                    "fashion",
                    "sightseeing",
                    "commercialplace",
                    "camping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "9F53CE9672",
                "category": "SHOPPING",
                "lat": 48.946953,
                "lng": 2.146179,
                "name": "L'Épicerie de Longueil",
                "rank": 30,
                "tags": [
                    "shopping",
                    "food",
                    "liquor"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "4E50CA73A8",
                "category": "SHOPPING",
                "lat": 48.9804,
                "lng": 2.182533,
                "name": "Pharmacie de la Frette Montigny",
                "rank": 30,
                "tags": [
                    "pharmacy",
                    "shopping",
                    "health&medical",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "88D044526F",
                "category": "RESTAURANT",
                "lat": 48.870705,
                "lng": 2.33878,
                "name": "Aux Lyonnais",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "shopping",
                    "transport",
                    "events"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "49C9AB1BB9",
                "category": "RESTAURANT",
                "lat": 48.897,
                "lng": 2.27047,
                "name": "Café de la Jatte",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace",
                    "fastfood"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "1C183FCC45",
                "category": "RESTAURANT",
                "lat": 48.89505,
                "lng": 2.28782,
                "name": "Absolu Café",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "brasserie",
                    "sightseeing",
                    "bar",
                    "commercialplace",
                    "breakfast"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "00A72739A5",
                "category": "RESTAURANT",
                "lat": 48.876972,
                "lng": 2.352167,
                "name": "Kashmir House",
                "rank": 30,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B84A737568",
                "category": "SIGHTS",
                "lat": 48.944912,
                "lng": 2.363505,
                "name": "Université Paris VIII Vincennes Saint Denis",
                "rank": 30,
                "tags": [
                    "university",
                    "college",
                    "schools",
                    "education",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "2F7A929C7F",
                "category": "SHOPPING",
                "lat": 48.902924,
                "lng": 2.376431,
                "name": "Le Millénaire",
                "rank": 30,
                "tags": [
                    "shopping",
                    "mall",
                    "sightseeing",
                    "commercialplace",
                    "restaurant",
                    "clothing",
                    "boat"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "40AD6E44A6",
                "category": "RESTAURANT",
                "lat": 48.957504,
                "lng": 2.309025,
                "name": "Wok Grill",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "904BF99168",
                "category": "SIGHTS",
                "lat": 48.920544,
                "lng": 2.342513,
                "name": "EDF Cap Ampère",
                "rank": 30,
                "tags": [
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "FE229DAEAB",
                "category": "SIGHTS",
                "lat": 48.89557,
                "lng": 2.40131,
                "name": "Centre National de la Danse",
                "rank": 30,
                "tags": [
                    "activities",
                    "professionalservices",
                    "events",
                    "dance",
                    "transport",
                    "bus",
                    "sightseeing"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "72972BE1FD",
                "category": "RESTAURANT",
                "lat": 48.83824,
                "lng": 2.240144,
                "name": "La Verrière",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "brewery",
                    "bar"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "0BE5CF93F2",
                "category": "RESTAURANT",
                "lat": 48.84216,
                "lng": 2.22036,
                "name": "Osaka Royal",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "EEDA9D037A",
                "category": "RESTAURANT",
                "lat": 48.95296,
                "lng": 2.260903,
                "name": "La Closerie d'Orgemont",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "A564376E2E",
                "category": "SHOPPING",
                "lat": 48.90569,
                "lng": 2.480507,
                "name": "Animalis Bondy",
                "rank": 30,
                "tags": [
                    "shopping",
                    "pet",
                    "professionalservices",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "81A009307F",
                "category": "RESTAURANT",
                "lat": 48.845383,
                "lng": 2.174163,
                "name": "Il Caravaggio",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "fastfood",
                    "pizza",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "32B70D4953",
                "category": "RESTAURANT",
                "lat": 48.84031,
                "lng": 2.24739,
                "name": "Le Gorgeon",
                "rank": 30,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "2B60C34DC6",
                "category": "SIGHTS",
                "lat": 48.82135,
                "lng": 2.289464,
                "name": "Mairie de Vanves",
                "rank": 30,
                "tags": [
                    "cityhall",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "C0150E208C",
                "category": "RESTAURANT",
                "lat": 48.864513,
                "lng": 2.345401,
                "name": "Comptoir de la Gastronomie",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "shopping",
                    "transport",
                    "market",
                    "outdoorplace",
                    "liquor",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "29AB6DD4F2",
                "category": "SIGHTS",
                "lat": 48.855137,
                "lng": 2.358925,
                "name": "Maison Européenne de la Photographie",
                "rank": 50,
                "tags": [
                    "museum",
                    "sightseeing",
                    "tourguide",
                    "sights",
                    "attraction",
                    "library",
                    "commercialplace",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "FEBD118E09",
                "category": "RESTAURANT",
                "lat": 48.8583,
                "lng": 2.30195,
                "name": "Les Cocottes",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace",
                    "transport",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "A1C6737D98",
                "category": "RESTAURANT",
                "lat": 48.865448,
                "lng": 2.328975,
                "name": "Kinugawa",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "transport",
                    "shopping",
                    "bar",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "891E7E0459",
                "category": "RESTAURANT",
                "lat": 48.856525,
                "lng": 2.342053,
                "name": "Le Caveau du Palais",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "bar",
                    "gastropub"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "DDB5A80E1F",
                "category": "RESTAURANT",
                "lat": 48.87359,
                "lng": 2.36494,
                "name": "Pink Flamingo",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "fastfood",
                    "pizza",
                    "sightseeing"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "75739CAD1B",
                "category": "RESTAURANT",
                "lat": 48.871113,
                "lng": 2.338271,
                "name": "Les Noces de Jeannette",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "transport",
                    "events",
                    "professionalservices",
                    "shopping",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "0D9967B42A",
                "category": "NIGHTLIFE",
                "lat": 48.86623,
                "lng": 2.331379,
                "name": "Le Rubis",
                "rank": 50,
                "tags": [
                    "bar",
                    "nightlife",
                    "restaurant",
                    "shopping",
                    "transport",
                    "activities",
                    "pub"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "EE1CE31C62",
                "category": "RESTAURANT",
                "lat": 48.87018,
                "lng": 2.36574,
                "name": "Sésame",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "breakfast",
                    "shopping",
                    "fastfood"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "51A27F668A",
                "category": "RESTAURANT",
                "lat": 49.024925,
                "lng": 2.324512,
                "name": "Le Siam",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "bar",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "DF1239EC81",
                "category": "RESTAURANT",
                "lat": 49.0021,
                "lng": 2.18546,
                "name": "Hoki Sushi",
                "rank": 15,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "34A33530DE",
                "category": "RESTAURANT",
                "lat": 48.97047,
                "lng": 2.180619,
                "name": "Aux Pirates De Saint Malo",
                "rank": 30,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B82BAA6B20",
                "category": "NIGHTLIFE",
                "lat": 48.9722,
                "lng": 2.25622,
                "name": "Le Must",
                "rank": 30,
                "tags": [
                    "bar"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "C954D8C362",
                "category": "RESTAURANT",
                "lat": 48.87986,
                "lng": 2.33566,
                "name": "L'Affineur'Affiné",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "shopping",
                    "cheese",
                    "food"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "354935BF1A",
                "category": "NIGHTLIFE",
                "lat": 48.87108,
                "lng": 2.373828,
                "name": "La Java",
                "rank": 30,
                "tags": [
                    "sightseeing",
                    "club",
                    "activities",
                    "bar",
                    "nightclub",
                    "events",
                    "dance",
                    "attraction",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "5A3E1DAB44",
                "category": "RESTAURANT",
                "lat": 48.9051,
                "lng": 2.301941,
                "name": "La Bonne Table",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "seafood",
                    "bar",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "6CC0CDF5B6",
                "category": "RESTAURANT",
                "lat": 48.89256,
                "lng": 2.26391,
                "name": "Les Pieds dans l'Eau",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "pub"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "E541A131EF",
                "category": "SIGHTS",
                "lat": 48.895393,
                "lng": 2.286071,
                "name": "Parc de la Planchette",
                "rank": 30,
                "tags": [
                    "park",
                    "attraction",
                    "activities",
                    "tourguide",
                    "beauty&spas"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "E6BC25AF4A",
                "category": "SIGHTS",
                "lat": 48.956207,
                "lng": 2.34146,
                "name": "Université Paris 13",
                "rank": 30,
                "tags": [
                    "university",
                    "college",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "060BAE310A",
                "category": "RESTAURANT",
                "lat": 48.96848,
                "lng": 2.30423,
                "name": "Pavillon Du Lac",
                "rank": 30,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "A1717A9EE4",
                "category": "SHOPPING",
                "lat": 48.90474,
                "lng": 2.32195,
                "name": "DanOn",
                "rank": 30,
                "tags": [
                    "liquor"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "DAAD985A24",
                "category": "SHOPPING",
                "lat": 48.857925,
                "lng": 2.438966,
                "name": "La Parole Errante",
                "rank": 30,
                "tags": [
                    "transport",
                    "shopping",
                    "professionalservices",
                    "activities",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "81E2201B6E",
                "category": "RESTAURANT",
                "lat": 48.85394,
                "lng": 2.42062,
                "name": "O Gib",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "activities",
                    "events",
                    "shopping",
                    "transport",
                    "patio",
                    "garden",
                    "pub",
                    "musicvenue"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "EA1F2A8414",
                "category": "RESTAURANT",
                "lat": 48.840508,
                "lng": 2.228908,
                "name": "Brasserie Jean Baptiste",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "brasserie",
                    "brewery",
                    "bar",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "F02A09DDF9",
                "category": "RESTAURANT",
                "lat": 48.844616,
                "lng": 2.234308,
                "name": "Tant qu'il y aura des Bretons",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "creperie"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B702FD9F19",
                "category": "SIGHTS",
                "lat": 48.814205,
                "lng": 2.290013,
                "name": "Gymnase Marcel Cerdan",
                "rank": 30,
                "tags": [
                    "gym",
                    "health&medical",
                    "sightseeing",
                    "commercialplace",
                    "sports"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "80BC9B7135",
                "category": "RESTAURANT",
                "lat": 48.81907,
                "lng": 2.319197,
                "name": "La Quincaillerie Générale",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "brewery",
                    "brasserie"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "CD376B6E95",
                "category": "NIGHTLIFE",
                "lat": 48.8604,
                "lng": 2.34055,
                "name": "Le Fumoir",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "bar",
                    "sightseeing",
                    "nightlife",
                    "tourguide",
                    "activities",
                    "pub",
                    "attraction",
                    "transport",
                    "events",
                    "club",
                    "patio",
                    "garden",
                    "shopping",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "1FBC640828",
                "category": "RESTAURANT",
                "lat": 48.85879,
                "lng": 2.358595,
                "name": "Le Marais",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "activities",
                    "attraction",
                    "tourguide",
                    "bar",
                    "sightseeing",
                    "sights",
                    "historicplace",
                    "historic"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "1D010A92BE",
                "category": "RESTAURANT",
                "lat": 48.85167,
                "lng": 2.31835,
                "name": "Coutume Café",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "shopping",
                    "commercialplace",
                    "transport",
                    "health&medical"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "2FC4400DCD",
                "category": "RESTAURANT",
                "lat": 48.86622,
                "lng": 2.332804,
                "name": "Gwadar",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "professionalservices"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "E643F2FC9D",
                "category": "SIGHTS",
                "lat": 48.879475,
                "lng": 2.339913,
                "name": "Arnaud Delmontel",
                "rank": 50,
                "tags": [
                    "sightseeing",
                    "bakery",
                    "shopping",
                    "restaurant",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "FEE88BC606",
                "category": "RESTAURANT",
                "lat": 48.8663,
                "lng": 2.3267,
                "name": "Ferdi",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "burger",
                    "fastfood"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "29D0D2376A",
                "category": "SIGHTS",
                "lat": 48.865887,
                "lng": 2.334337,
                "name": "Avenue de l'Opéra",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "road",
                    "street",
                    "sightseeing",
                    "landmark",
                    "sights",
                    "historicplace",
                    "historic"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "AD32FF8450",
                "category": "SIGHTS",
                "lat": 48.854286,
                "lng": 2.344713,
                "name": "Pont Saint Michel",
                "rank": 50,
                "tags": [
                    "bridge",
                    "sightseeing",
                    "parking",
                    "commercialplace",
                    "attraction",
                    "transport",
                    "train",
                    "trainstation",
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "907C7C9E53",
                "category": "SHOPPING",
                "lat": 48.870018,
                "lng": 2.327288,
                "name": "La Maison du Chocolat",
                "rank": 50,
                "tags": [
                    "shopping",
                    "chocolate",
                    "transport",
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "C066DFE5ED",
                "category": "RESTAURANT",
                "lat": 48.948452,
                "lng": 2.148836,
                "name": "Le Tastevin",
                "rank": 10,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "223E086811",
                "category": "SIGHTS",
                "lat": 48.95694,
                "lng": 2.204874,
                "name": "Aerokart",
                "rank": 15,
                "tags": [
                    "activities",
                    "sports",
                    "trail",
                    "attraction",
                    "tourguide",
                    "stadium",
                    "sightseeing",
                    "commercialplace",
                    "karting"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "E946F20EAD",
                "category": "RESTAURANT",
                "lat": 48.96073,
                "lng": 2.20509,
                "name": "La Trattoria d'Anna",
                "rank": 30,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "123ECE4DC9",
                "category": "SIGHTS",
                "lat": 48.920708,
                "lng": 2.220268,
                "name": "Pont de Bezons",
                "rank": 30,
                "tags": [
                    "bridge",
                    "restaurant",
                    "fastfood",
                    "transport",
                    "bus",
                    "train",
                    "tram",
                    "sightseeing"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "191EDDDE98",
                "category": "SIGHTS",
                "lat": 48.870705,
                "lng": 2.363023,
                "name": "Alhambra",
                "rank": 30,
                "tags": [
                    "sightseeing",
                    "activities",
                    "tourguide",
                    "events",
                    "musicvenue",
                    "attraction",
                    "shopping",
                    "transport",
                    "commercialplace",
                    "professionalservices",
                    "theater"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "3F520A9FBE",
                "category": "SIGHTS",
                "lat": 48.8737,
                "lng": 2.36291,
                "name": "Les Vinaigriers",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "landmark",
                    "sights",
                    "historicplace",
                    "historic",
                    "shopping",
                    "street"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "682D42CE58",
                "category": "RESTAURANT",
                "lat": 48.91783,
                "lng": 2.27603,
                "name": "Le Chefson",
                "rank": 30,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B529E0648B",
                "category": "RESTAURANT",
                "lat": 48.9108,
                "lng": 2.299725,
                "name": "Le Van Gogh",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "moderncuisine",
                    "parking"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "12A79C9A45",
                "category": "RESTAURANT",
                "lat": 48.914494,
                "lng": 2.337265,
                "name": "Chez Serge",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "fastfood",
                    "bar",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "68E7E07E8C",
                "category": "RESTAURANT",
                "lat": 48.889748,
                "lng": 2.16354,
                "name": "Les Rives de La Courtille",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "pub",
                    "bar",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "352719DEF9",
                "category": "SHOPPING",
                "lat": 48.9701,
                "lng": 2.30768,
                "name": "Le Cacaotier",
                "rank": 30,
                "tags": [
                    "food",
                    "shopping",
                    "kids",
                    "attraction",
                    "activities",
                    "tourguide",
                    "chocolate"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "9AD968B557",
                "category": "RESTAURANT",
                "lat": 48.962,
                "lng": 2.370465,
                "name": "Le Rawal",
                "rank": 30,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "6DEB45FE84",
                "category": "SIGHTS",
                "lat": 48.91362,
                "lng": 2.331786,
                "name": "Alstom",
                "rank": 30,
                "tags": [
                    "sightseeing",
                    "commercialplace",
                    "professionalservices"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "651250F46C",
                "category": "RESTAURANT",
                "lat": 48.865814,
                "lng": 2.44663,
                "name": "Le Mange Disc",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "bar",
                    "pub",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "6CBDB29A81",
                "category": "RESTAURANT",
                "lat": 48.828304,
                "lng": 2.235603,
                "name": "Doddy's Coffee",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "fastfood",
                    "burger",
                    "breakfast",
                    "activities",
                    "donuts"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "2E58DA970A",
                "category": "RESTAURANT",
                "lat": 48.86928,
                "lng": 2.37154,
                "name": "Le Chateaubriand",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "bistro",
                    "sightseeing",
                    "shopping",
                    "transport"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "FF516C46C9",
                "category": "RESTAURANT",
                "lat": 48.857986,
                "lng": 2.357996,
                "name": "Le Colimaçon",
                "rank": 50,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "CD41467567",
                "category": "RESTAURANT",
                "lat": 48.86558,
                "lng": 2.33689,
                "name": "Ellsworth",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "moderncuisine",
                    "sightseeing",
                    "bar",
                    "events",
                    "club",
                    "shopping",
                    "transport"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "1A712BD771",
                "category": "RESTAURANT",
                "lat": 48.85696,
                "lng": 2.341829,
                "name": "Ma Salle à Manger",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "bistro"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "84645CB079",
                "category": "NIGHTLIFE",
                "lat": 48.86927,
                "lng": 2.36822,
                "name": "Palais des Glaces",
                "rank": 50,
                "tags": [
                    "theater",
                    "activities",
                    "attraction",
                    "sightseeing",
                    "commercialplace",
                    "club",
                    "events",
                    "musicvenue",
                    "tourguide",
                    "shopping",
                    "transport"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B2EA5CDD8C",
                "category": "RESTAURANT",
                "lat": 48.87184,
                "lng": 2.33391,
                "name": "Murphy's House",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "pub",
                    "activities",
                    "attraction",
                    "transport",
                    "events",
                    "bar",
                    "club",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "381CFEE298",
                "category": "NIGHTLIFE",
                "lat": 48.87323,
                "lng": 2.36079,
                "name": "Gravity Bar",
                "rank": 50,
                "tags": [
                    "tapas",
                    "restaurant",
                    "bar",
                    "pub",
                    "club",
                    "nightlife",
                    "sightseeing",
                    "activities",
                    "transport",
                    "patio",
                    "garden"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "6945F2EE01",
                "category": "RESTAURANT",
                "lat": 48.88339,
                "lng": 2.33436,
                "name": "Les Trois Baudets",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "activities",
                    "events",
                    "musicvenue",
                    "pub",
                    "transport",
                    "theater"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "16BD679744",
                "category": "RESTAURANT",
                "lat": 48.945217,
                "lng": 2.144552,
                "name": "La Plancha",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B54AC4636E",
                "category": "RESTAURANT",
                "lat": 48.9886,
                "lng": 2.20763,
                "name": "El Rancho",
                "rank": 15,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "05B4989D98",
                "category": "RESTAURANT",
                "lat": 49.034355,
                "lng": 2.214421,
                "name": "La Palmeraie Jardins de Marakech",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "bar",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B38CED2B84",
                "category": "RESTAURANT",
                "lat": 48.98335,
                "lng": 2.202514,
                "name": "La Montagne",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "6772C17A8A",
                "category": "RESTAURANT",
                "lat": 48.97393,
                "lng": 2.188961,
                "name": "Le Diplomate",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            }
        ]
    }
}

module.exports = getPOIsAlongTheRoad;
