const request = require("../common_libs/request");
const jsonToUrlEncoded = require("../common_libs/jsonToUrlEncoded");

module.exports = async (_, __, categoriesHostel) => {
    return {
        status: 200,
        body: JSON.stringify([
            {
                "type": "TOMTOM",
                "id": "250009042522912",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Comfort Hotel Paris Montmartre",
                "lat": 48.88402,
                "lng": 2.33426
            },
            {
                "type": "TOMTOM",
                "id": "250009036645936",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Hôtel Jardin de Villiers",
                "lat": 48.88407,
                "lng": 2.31503
            },
            {
                "type": "TOMTOM",
                "id": "250009036864351",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Hôtel des Deux Avenues",
                "lat": 48.87986,
                "lng": 2.29871
            },
            {
                "type": "TOMTOM",
                "id": "250009006631808",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Hôtel Savoy",
                "lat": 48.88465,
                "lng": 2.32476
            },
            {
                "type": "TOMTOM",
                "id": "250009042479798",
                "categories": [
                    "bed breakfast guest houses",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314002
                ],
                "name": "Églantine et Nicolas",
                "lat": 48.88605,
                "lng": 2.33852
            },
            {
                "type": "TOMTOM",
                "id": "250009006517890",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Hôtel Maubeuge Gare du Nord",
                "lat": 48.88035,
                "lng": 2.35135
            },
            {
                "type": "TOMTOM",
                "id": "250009005588893",
                "categories": [
                    "bed breakfast guest houses",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314002
                ],
                "name": "Nouvel Hotel",
                "lat": 48.88654,
                "lng": 2.34889
            },
            {
                "type": "TOMTOM",
                "id": "250009004799101",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Grand Hotel De Turin",
                "lat": 48.88064,
                "lng": 2.33976
            },
            {
                "type": "TOMTOM",
                "id": "250009042458772",
                "categories": [
                    "bed breakfast guest houses",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314002
                ],
                "name": "Courcelles Parc",
                "lat": 48.88097,
                "lng": 2.31483
            },
            {
                "type": "TOMTOM",
                "id": "250009036688676",
                "categories": [
                    "hostel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314004
                ],
                "name": "Peace & Love",
                "lat": 48.8826,
                "lng": 2.36906
            },
            {
                "type": "TOMTOM",
                "id": "250009036656415",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Hôtel Carlton's",
                "lat": 48.88203,
                "lng": 2.34093
            },
            {
                "type": "TOMTOM",
                "id": "250009036813323",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Grand Hôtel Magenta",
                "lat": 48.88082,
                "lng": 2.35173
            },
            {
                "type": "TOMTOM",
                "id": "250007000175710",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Hôtel Mantana",
                "lat": 48.87974,
                "lng": 2.35827
            },
            {
                "type": "TOMTOM",
                "id": "250009006922747",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Hôtel Clichy",
                "lat": 48.88295,
                "lng": 2.32846
            },
            {
                "type": "TOMTOM",
                "id": "250009036540424",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Orfea",
                "lat": 48.8804,
                "lng": 2.35749
            },
            {
                "type": "TOMTOM",
                "id": "250009036822066",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Hôtel Le Villiers",
                "lat": 48.88543,
                "lng": 2.29347
            },
            {
                "type": "TOMTOM",
                "id": "250009042403742",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Vestay George V",
                "lat": 48.86773,
                "lng": 2.30271
            },
            {
                "type": "TOMTOM",
                "id": "250009006948378",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Hôtel Excelsior République",
                "lat": 48.8688,
                "lng": 2.36013
            },
            {
                "type": "TOMTOM",
                "id": "250009041020441",
                "categories": [
                    "bed breakfast guest houses",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314002
                ],
                "name": "My Home for You Luxury B&B",
                "lat": 48.87103,
                "lng": 2.34695
            },
            {
                "type": "TOMTOM",
                "id": "250009036608412",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Hôtel Chambellan Morgane",
                "lat": 48.86976,
                "lng": 2.29747
            },
            {
                "type": "TOMTOM",
                "id": "250009040071645",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Hipotel Paris Gambetta République",
                "lat": 48.8675,
                "lng": 2.39661
            },
            {
                "type": "TOMTOM",
                "id": "250009036852473",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Hôtel Kléber",
                "lat": 48.86872,
                "lng": 2.29207
            },
            {
                "type": "TOMTOM",
                "id": "250009042390462",
                "categories": [
                    "bed breakfast guest houses",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314002
                ],
                "name": "Chambre d'Hôte Paris Centre",
                "lat": 48.87017,
                "lng": 2.35131
            },
            {
                "type": "TOMTOM",
                "id": "250009036799345",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Atrium Hotel Paris",
                "lat": 48.86703,
                "lng": 2.22098
            },
            {
                "type": "TOMTOM",
                "id": "250009036552473",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Citadines Trocadéro Paris",
                "lat": 48.86635,
                "lng": 2.28555
            },
            {
                "type": "TOMTOM",
                "id": "250009036867170",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Hôtel Cambon",
                "lat": 48.86641,
                "lng": 2.32521
            },
            {
                "type": "TOMTOM",
                "id": "250009036634853",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Hotel Des Tuileries",
                "lat": 48.86614,
                "lng": 2.33156
            },
            {
                "type": "TOMTOM",
                "id": "250009036550545",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Hôtel Amarante Beau Manoir",
                "lat": 48.87106,
                "lng": 2.32352
            },
            {
                "type": "TOMTOM",
                "id": "250009036525590",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Hôtel Leonard de Vinci",
                "lat": 48.86706,
                "lng": 2.37421
            },
            {
                "type": "TOMTOM",
                "id": "250009036690052",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Hôtel Élysées Bassano",
                "lat": 48.86893,
                "lng": 2.29745
            },
            {
                "type": "TOMTOM",
                "id": "250009036633983",
                "categories": [
                    "hotel",
                    "hotel/motel"
                ],
                "categorySet": [
                    7314003
                ],
                "name": "Hôtel Claridge",
                "lat": 48.86852,
                "lng": 2.30367
            }
        ].filter(({categorySet}) =>
            !categoriesHostel || categoriesHostel.length === 0 || categoriesHostel.some(id => categorySet.includes(id))
        ))
    }
}