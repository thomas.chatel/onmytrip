async function findPOIsInCityMocked(_, __) {
    return {
        "POIs": [
            {
                "type": "AMADEUS",
                "id": "C589E1C8E8",
                "category": "RESTAURANT",
                "lat": 45.828598,
                "lng": 1.257789,
                "name": "Les Petits Ventres",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "sightseeing"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "760C1655B1",
                "category": "RESTAURANT",
                "lat": 45.87116,
                "lng": 1.277798,
                "name": "Les Tables du Bistrot",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "bistro",
                    "sightseeing",
                    "regionalcuisine",
                    "bar",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "CBD1048292",
                "category": "SIGHTS",
                "lat": 45.832684,
                "lng": 1.253242,
                "name": "Musée National Adrien Dubouché",
                "rank": 5,
                "tags": [
                    "museum",
                    "sights",
                    "tourguide",
                    "sightseeing",
                    "attraction",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "780A75A288",
                "category": "RESTAURANT",
                "lat": 45.82151,
                "lng": 1.230189,
                "name": "Le Vanteaux",
                "rank": 5,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "497E8382B2",
                "category": "SIGHTS",
                "lat": 45.83039,
                "lng": 1.261013,
                "name": "Pavillon du Verdurier",
                "rank": 10,
                "tags": [
                    "sightseeing",
                    "sights",
                    "landmark",
                    "historicplace",
                    "historic",
                    "artgallerie",
                    "restaurant",
                    "museum",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "DEABD85C12",
                "category": "NIGHTLIFE",
                "lat": 45.833115,
                "lng": 1.255923,
                "name": "Brasserie Michard",
                "rank": 10,
                "tags": [
                    "brasserie",
                    "sightseeing",
                    "brewery",
                    "bar",
                    "pub",
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B95907BFDF",
                "category": "SIGHTS",
                "lat": 45.82871,
                "lng": 1.266199,
                "name": "Cathedrale saint Etienne",
                "rank": 15,
                "tags": [
                    "church",
                    "attraction",
                    "activities",
                    "tourguide"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "24921B289E",
                "category": "RESTAURANT",
                "lat": 45.831432,
                "lng": 1.254248,
                "name": "Le Bistrot Gourmand",
                "rank": 15,
                "tags": [
                    "bistro",
                    "restaurant",
                    "pub",
                    "professionalservices"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "1A711C4D5E",
                "category": "RESTAURANT",
                "lat": 45.82955,
                "lng": 1.25018,
                "name": "La Table du Couvent",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "shopping",
                    "food"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "480AD5DB41",
                "category": "RESTAURANT",
                "lat": 45.832985,
                "lng": 1.261734,
                "name": "La Vache au Plafond",
                "rank": 10,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "CEAD90F0D8",
                "category": "SIGHTS",
                "lat": 45.836414,
                "lng": 1.266968,
                "name": "Gare de Limoges Bénédictins",
                "rank": 15,
                "tags": [
                    "sightseeing",
                    "restaurant",
                    "transport",
                    "train",
                    "trainstation",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "AC90C01B0A",
                "category": "NIGHTLIFE",
                "lat": 45.85397,
                "lng": 1.28751,
                "name": "Brasserie Michard",
                "rank": 30,
                "tags": [
                    "brewery",
                    "pub",
                    "sightseeing"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "1D4768FB9D",
                "category": "RESTAURANT",
                "lat": 45.83057,
                "lng": 1.254683,
                "name": "Le Versailles",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "bistro",
                    "brasserie"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "EE7A29714C",
                "category": "RESTAURANT",
                "lat": 45.83274,
                "lng": 1.264628,
                "name": "Le Bistrot Jourdan",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "brewery",
                    "pub"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "1133BABB09",
                "category": "RESTAURANT",
                "lat": 45.82439,
                "lng": 1.257277,
                "name": "Le Cheverny",
                "rank": 30,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "A5177E56E6",
                "category": "RESTAURANT",
                "lat": 45.8285,
                "lng": 1.27184,
                "name": "Le Pont Saint Etienne",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "landmark"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "80EBC87FA6",
                "category": "SHOPPING",
                "lat": 45.831715,
                "lng": 1.261667,
                "name": "Galeries Lafayette",
                "rank": 30,
                "tags": [
                    "shopping",
                    "clothing",
                    "fashion",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "8D054B4BF2",
                "category": "RESTAURANT",
                "lat": 45.838657,
                "lng": 1.258443,
                "name": "La Maison des Saveurs",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "professionalservices"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "524C0D444E",
                "category": "RESTAURANT",
                "lat": 45.831726,
                "lng": 1.254081,
                "name": "L'Orangeraie",
                "rank": 50,
                "tags": [
                    "fastfood",
                    "pizza",
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "0C8C1CA4DB",
                "category": "RESTAURANT",
                "lat": 45.833252,
                "lng": 1.255738,
                "name": "La Firenza",
                "rank": 30,
                "tags": [
                    "fastfood",
                    "pizza",
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "F38F7E1BD7",
                "category": "SIGHTS",
                "lat": 45.8285,
                "lng": 1.267,
                "name": "Jardin botanique de l'Evêché",
                "rank": 50,
                "tags": [
                    "garden",
                    "beauty&spas",
                    "fountain",
                    "statue",
                    "square"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "D0B7C978F3",
                "category": "NIGHTLIFE",
                "lat": 45.82961,
                "lng": 1.26548,
                "name": "L'Irlandais",
                "rank": 50,
                "tags": [
                    "sightseeing",
                    "pub",
                    "bar",
                    "restaurant",
                    "commercialplace",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "C7B694A73F",
                "category": "RESTAURANT",
                "lat": 45.8285,
                "lng": 1.26104,
                "name": "Mamie Bigoude",
                "rank": 30,
                "tags": [
                    "restaurant",
                    "creperie"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "AC33FA5374",
                "category": "RESTAURANT",
                "lat": 45.83324,
                "lng": 1.26438,
                "name": "O'Brien Tavern",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "pub",
                    "bar",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "90BA71C922",
                "category": "RESTAURANT",
                "lat": 45.8882,
                "lng": 1.29084,
                "name": "La Toscana",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "fastfood",
                    "pizza"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "95E85FE0A2",
                "category": "SIGHTS",
                "lat": 45.844303,
                "lng": 1.254759,
                "name": "Parc Victor Thuillat",
                "rank": 50,
                "tags": [
                    "park",
                    "sightseeing",
                    "sights",
                    "historicplace",
                    "historic",
                    "outdoorplace",
                    "beauty&spas",
                    "commercialplace",
                    "landmark"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "AD6E8A2A3B",
                "category": "RESTAURANT",
                "lat": 45.828518,
                "lng": 1.259168,
                "name": "Le 27",
                "rank": 50,
                "tags": [
                    "restaurant",
                    "sightseeing"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "DA7B11B390",
                "category": "NIGHTLIFE",
                "lat": 45.86208,
                "lng": 1.279514,
                "name": "Zénith de Limoges",
                "rank": 50,
                "tags": [
                    "events",
                    "activities",
                    "musicvenue"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "ED1186AFC2",
                "category": "RESTAURANT",
                "lat": 45.83162,
                "lng": 1.258023,
                "name": "La Bibliothèque",
                "rank": 50,
                "tags": [
                    "brasserie",
                    "restaurant",
                    "activities",
                    "pub"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "DC3880A5FE",
                "category": "RESTAURANT",
                "lat": 45.837708,
                "lng": 1.256483,
                "name": "Le Boeuf a la Mode",
                "rank": 100,
                "tags": [
                    "restaurant",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "F2DCFC2A5E",
                "category": "NIGHTLIFE",
                "lat": 45.830498,
                "lng": 1.267399,
                "name": "Le Buckingham",
                "rank": 100,
                "tags": [
                    "club",
                    "bar",
                    "nightclub",
                    "events",
                    "dance",
                    "sightseeing",
                    "commercialplace",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B444D44EA2",
                "category": "SIGHTS",
                "lat": 45.829433,
                "lng": 1.256722,
                "name": "Halles Centrales",
                "rank": 100,
                "tags": [
                    "sightseeing",
                    "market",
                    "shopping",
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "7DC0675620",
                "category": "RESTAURANT",
                "lat": 45.832165,
                "lng": 1.257029,
                "name": "Le Chalet",
                "rank": 100,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "3E02ECF03E",
                "category": "SIGHTS",
                "lat": 45.836243,
                "lng": 1.263738,
                "name": "Plan Régional de Professionnalisation des Acteurs Touristiques du Limousin",
                "rank": 100,
                "tags": [
                    "schools",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "4FDD11A3BC",
                "category": "RESTAURANT",
                "lat": 45.82978,
                "lng": 1.25498,
                "name": "La Fabrique du Café",
                "rank": 100,
                "tags": [
                    "restaurant",
                    "shopping",
                    "professionalservices"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B20971B96A",
                "category": "RESTAURANT",
                "lat": 45.83038,
                "lng": 1.264656,
                "name": "L'Escapade du Gourmet",
                "rank": 100,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "435A6B633D",
                "category": "NIGHTLIFE",
                "lat": 45.82951,
                "lng": 1.265502,
                "name": "Au Bout du Monde",
                "rank": 100,
                "tags": [
                    "bar",
                    "musicvenue",
                    "sightseeing",
                    "commercialplace",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "CC6A14E415",
                "category": "SIGHTS",
                "lat": 45.826477,
                "lng": 1.260311,
                "name": "Hôtel de Ville de Limoges",
                "rank": 100,
                "tags": [
                    "cityhall",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "F5A351587D",
                "category": "RESTAURANT",
                "lat": 45.88743,
                "lng": 1.290681,
                "name": "Hall West",
                "rank": 100,
                "tags": [
                    "restaurant",
                    "steakhouse"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "9DF967A7A3",
                "category": "RESTAURANT",
                "lat": 45.83184,
                "lng": 1.25436,
                "name": "Jean Burger",
                "rank": 100,
                "tags": [
                    "burger",
                    "fastfood"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "389C3CED76",
                "category": "SIGHTS",
                "lat": 45.882336,
                "lng": 1.267158,
                "name": "Schneider Electric",
                "rank": 100,
                "tags": [
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "026616190A",
                "category": "RESTAURANT",
                "lat": 45.83021,
                "lng": 1.26488,
                "name": "L'Imaginarium",
                "rank": 100,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "E8DD280041",
                "category": "SIGHTS",
                "lat": 45.82799,
                "lng": 1.266369,
                "name": "Musée des Beaux Arts",
                "rank": 100,
                "tags": [
                    "sightseeing",
                    "artgallerie",
                    "sights",
                    "museum"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "5BC50B8AC8",
                "category": "RESTAURANT",
                "lat": 45.83288,
                "lng": 1.25573,
                "name": "Le Glacier",
                "rank": 100,
                "tags": [
                    "brasserie",
                    "icecream",
                    "bar",
                    "shopping",
                    "restaurant",
                    "pub",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "015EDA64B2",
                "category": "NIGHTLIFE",
                "lat": 45.8325,
                "lng": 1.25917,
                "name": "Les Artistes",
                "rank": 100,
                "tags": [
                    "brasserie",
                    "bar",
                    "brewery",
                    "pub",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "CC2047B820",
                "category": "RESTAURANT",
                "lat": 45.83006,
                "lng": 1.258472,
                "name": "La Parenthèse",
                "rank": 100,
                "tags": [
                    "sightseeing",
                    "restaurant",
                    "tea",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B665DC3A8C",
                "category": "RESTAURANT",
                "lat": 45.884052,
                "lng": 1.288991,
                "name": "Leon de Bruxelles",
                "rank": 100,
                "tags": [
                    "restaurant",
                    "seafood"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "E2418ECF66",
                "category": "RESTAURANT",
                "lat": 45.88858,
                "lng": 1.29084,
                "name": "Hippopotamus",
                "rank": 100,
                "tags": [
                    "restaurant",
                    "steakhouse",
                    "bar",
                    "grills"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "AE5013A53E",
                "category": "SIGHTS",
                "lat": 45.831596,
                "lng": 1.256154,
                "name": "Philippe Redon",
                "rank": 200,
                "tags": [
                    "sightseeing",
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "A895E2EADD",
                "category": "RESTAURANT",
                "lat": 45.82853,
                "lng": 1.258417,
                "name": "En bas d'la rue",
                "rank": 100,
                "tags": [
                    "restaurant",
                    "bistro",
                    "brasserie"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "8EA80A2B17",
                "category": "NIGHTLIFE",
                "lat": 45.828636,
                "lng": 1.25764,
                "name": "Le Duc Etienne",
                "rank": 200,
                "tags": [
                    "sightseeing",
                    "bar",
                    "pub",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "06970B7157",
                "category": "RESTAURANT",
                "lat": 45.81204,
                "lng": 1.244315,
                "name": "La Villa",
                "rank": 200,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "015EE44BCA",
                "category": "RESTAURANT",
                "lat": 45.82952,
                "lng": 1.256242,
                "name": "Le P'tit Bouchon",
                "rank": 200,
                "tags": [
                    "restaurant",
                    "bar",
                    "tapas",
                    "bistro"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "649E55A687",
                "category": "SHOPPING",
                "lat": 45.832336,
                "lng": 1.25802,
                "name": "Fnac Limoges",
                "rank": 200,
                "tags": [
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "E1687738B7",
                "category": "NIGHTLIFE",
                "lat": 45.828682,
                "lng": 1.260843,
                "name": "Le Barouf",
                "rank": 200,
                "tags": [
                    "bar",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "75814F1FCD",
                "category": "RESTAURANT",
                "lat": 45.82876,
                "lng": 1.2606,
                "name": "Ichiban Sushi",
                "rank": 200,
                "tags": [
                    "sushi",
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "818207B09F",
                "category": "SHOPPING",
                "lat": 45.835827,
                "lng": 1.259307,
                "name": "Centre Saint Martial",
                "rank": 200,
                "tags": [
                    "shopping",
                    "mall"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "BAAA7E1494",
                "category": "RESTAURANT",
                "lat": 45.832253,
                "lng": 1.259935,
                "name": "Pomme Cannelle",
                "rank": 200,
                "tags": [
                    "tea",
                    "restaurant",
                    "pub",
                    "creperie",
                    "regionalcuisine"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "C689A1758A",
                "category": "SIGHTS",
                "lat": 45.8317,
                "lng": 1.260209,
                "name": "Place de la République",
                "rank": 200,
                "tags": [
                    "square",
                    "landmark",
                    "historicplace",
                    "sightseeing",
                    "sights",
                    "historic"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "58A6C0A975",
                "category": "RESTAURANT",
                "lat": 45.83368,
                "lng": 1.254359,
                "name": "La Cuisine",
                "rank": 200,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "6036ED24BB",
                "category": "RESTAURANT",
                "lat": 45.820477,
                "lng": 1.231585,
                "name": "Quick Vanteaux",
                "rank": 200,
                "tags": [
                    "restaurant",
                    "fastfood"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "96F546F00A",
                "category": "SIGHTS",
                "lat": 45.83288,
                "lng": 1.255571,
                "name": "Place Denis Dussoubs",
                "rank": 200,
                "tags": [
                    "square",
                    "landmark",
                    "historicplace",
                    "sightseeing",
                    "sights",
                    "historic"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "673B4954A9",
                "category": "RESTAURANT",
                "lat": 45.88066,
                "lng": 1.290323,
                "name": "McDonald's",
                "rank": 200,
                "tags": [
                    "restaurant",
                    "fastfood"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "864E824A03",
                "category": "SIGHTS",
                "lat": 45.828606,
                "lng": 1.25766,
                "name": "Amphitryon",
                "rank": 200,
                "tags": [
                    "sightseeing",
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "54A39E6086",
                "category": "SIGHTS",
                "lat": 45.829784,
                "lng": 1.256891,
                "name": "Place de la Motte",
                "rank": 200,
                "tags": [
                    "square",
                    "sightseeing"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "499A5D2B94",
                "category": "SHOPPING",
                "lat": 45.893677,
                "lng": 1.281752,
                "name": "Family village",
                "rank": 200,
                "tags": [
                    "shopping",
                    "mall"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "D7A2AF00C1",
                "category": "SIGHTS",
                "lat": 45.80425,
                "lng": 1.277622,
                "name": "Chronodrive",
                "rank": 200,
                "tags": [
                    "supermarket",
                    "sightseeing",
                    "commercialplace",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "1490D0B653",
                "category": "RESTAURANT",
                "lat": 45.83871,
                "lng": 1.25838,
                "name": "Chez Monsieur Edouard",
                "rank": 200,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "361CF7B918",
                "category": "SIGHTS",
                "lat": 45.822514,
                "lng": 1.285936,
                "name": "Centre Culturel John Lennon",
                "rank": 200,
                "tags": [
                    "sightseeing",
                    "commercialplace",
                    "events"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "E9F4CB1D0B",
                "category": "RESTAURANT",
                "lat": 45.831993,
                "lng": 1.259452,
                "name": "Café République",
                "rank": 300,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "0E9F5146E1",
                "category": "SHOPPING",
                "lat": 45.8167,
                "lng": 1.229964,
                "name": "BU Lettres et Sciences Humaines",
                "rank": 300,
                "tags": [
                    "library",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "A942E3B7E6",
                "category": "SHOPPING",
                "lat": 45.832184,
                "lng": 1.258795,
                "name": "Opéra Théâtre de Limoges",
                "rank": 300,
                "tags": [
                    "theater",
                    "attraction",
                    "activities",
                    "tourguide",
                    "shopping",
                    "transport",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "BF589815D8",
                "category": "SIGHTS",
                "lat": 45.83088,
                "lng": 1.266604,
                "name": "Latitude 87",
                "rank": 300,
                "tags": [
                    "professionalservices",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "F03A7AD58A",
                "category": "SHOPPING",
                "lat": 45.82987,
                "lng": 1.264833,
                "name": "Didier Cariguel",
                "rank": 400,
                "tags": [
                    "home",
                    "shopping",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "E122E51B49",
                "category": "RESTAURANT",
                "lat": 45.831135,
                "lng": 1.253706,
                "name": "Le Churchill",
                "rank": 400,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "8258B0D5AF",
                "category": "SHOPPING",
                "lat": 45.87284,
                "lng": 1.268877,
                "name": "La Cervoiserie",
                "rank": 400,
                "tags": [
                    "liquor",
                    "shopping",
                    "sightseeing",
                    "commercialplace",
                    "food"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "77AAC78326",
                "category": "SIGHTS",
                "lat": 45.830425,
                "lng": 1.258604,
                "name": "Cour du Temple",
                "rank": 500,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "sights",
                    "square",
                    "gift",
                    "shopping"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "929D052968",
                "category": "SIGHTS",
                "lat": 45.83083,
                "lng": 1.266688,
                "name": "Patrick Bideau",
                "rank": 400,
                "tags": [
                    "barbershop",
                    "hairsalon",
                    "sightseeing",
                    "commercialplace",
                    "shopping",
                    "beauty&spas"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "300E54AE99",
                "category": "RESTAURANT",
                "lat": 45.833195,
                "lng": 1.264396,
                "name": "Mundo Latino",
                "rank": 500,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "C2787B9F2B",
                "category": "RESTAURANT",
                "lat": 45.831425,
                "lng": 1.259627,
                "name": "Columbus Café",
                "rank": 500,
                "tags": [
                    "restaurant",
                    "pub",
                    "shopping",
                    "fastfood"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B1F3BD8C5B",
                "category": "RESTAURANT",
                "lat": 45.830696,
                "lng": 1.262834,
                "name": "Le Geyracois",
                "rank": 500,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "13D3D59337",
                "category": "RESTAURANT",
                "lat": 45.81139,
                "lng": 1.281868,
                "name": "Buffalo Grill",
                "rank": 500,
                "tags": [
                    "restaurant",
                    "steakhouse",
                    "bar",
                    "grills"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "6E18802EE7",
                "category": "RESTAURANT",
                "lat": 45.88417,
                "lng": 1.285008,
                "name": "Tan Saigon",
                "rank": 500,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "DA6081FDD5",
                "category": "NIGHTLIFE",
                "lat": 45.83369,
                "lng": 1.26131,
                "name": "The Times Club",
                "rank": 500,
                "tags": [
                    "club",
                    "bar",
                    "nightclub",
                    "events",
                    "dance"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B335E7D817",
                "category": "RESTAURANT",
                "lat": 45.82942,
                "lng": 1.25602,
                "name": "au coin des halles",
                "rank": 500,
                "tags": [
                    "restaurant",
                    "breakfast"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "A584E8276C",
                "category": "RESTAURANT",
                "lat": 45.838318,
                "lng": 1.258675,
                "name": "Un Parfum D'oxalis",
                "rank": 500,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B261527051",
                "category": "RESTAURANT",
                "lat": 45.82778,
                "lng": 1.261052,
                "name": "Le Bistrot du Boucher",
                "rank": 500,
                "tags": [
                    "restaurant",
                    "bistro",
                    "brasserie",
                    "regionalcuisine"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "9146FBC600",
                "category": "RESTAURANT",
                "lat": 45.81339,
                "lng": 1.24109,
                "name": "Fast Good Café",
                "rank": 500,
                "tags": [
                    "restaurant",
                    "shopping",
                    "fastfood"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "AF5DCAA440",
                "category": "RESTAURANT",
                "lat": 45.840103,
                "lng": 1.256991,
                "name": "La Régalade",
                "rank": 500,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "70CDF06106",
                "category": "RESTAURANT",
                "lat": 45.83358,
                "lng": 1.264503,
                "name": "La Pitchouri",
                "rank": 500,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "BA384C4C00",
                "category": "SHOPPING",
                "lat": 45.87913,
                "lng": 1.291805,
                "name": "Cora",
                "rank": 500,
                "tags": [
                    "supermarket",
                    "food",
                    "shopping",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "CF6AA2E809",
                "category": "RESTAURANT",
                "lat": 45.83025,
                "lng": 1.26344,
                "name": "Hollywood Regal",
                "rank": 500,
                "tags": [
                    "restaurant",
                    "burger",
                    "fastfood"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "A0BA45437B",
                "category": "RESTAURANT",
                "lat": 45.831963,
                "lng": 1.25467,
                "name": "Azian",
                "rank": 500,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "817CB5D763",
                "category": "RESTAURANT",
                "lat": 45.828617,
                "lng": 1.265822,
                "name": "Limoges Fine Arts Museum",
                "rank": 500,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "3B98417EEA",
                "category": "NIGHTLIFE",
                "lat": 45.836887,
                "lng": 1.256095,
                "name": "Iron Gym",
                "rank": 500,
                "tags": [
                    "gym",
                    "sports",
                    "stadium",
                    "sportclub",
                    "club",
                    "health&medical"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "151D128D2D",
                "category": "SIGHTS",
                "lat": 45.830223,
                "lng": 1.254866,
                "name": "Place d'Aine",
                "rank": 500,
                "tags": [
                    "square",
                    "landmark",
                    "historicplace",
                    "sightseeing",
                    "sights",
                    "historic",
                    "transport",
                    "bus"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "7784C13DA2",
                "category": "SHOPPING",
                "lat": 45.841286,
                "lng": 1.233153,
                "name": "Hyper U",
                "rank": 500,
                "tags": [
                    "shopping",
                    "food",
                    "supermarket"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "FCF0BF708B",
                "category": "RESTAURANT",
                "lat": 45.828648,
                "lng": 1.260804,
                "name": "San Marco",
                "rank": 500,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "F58782427A",
                "category": "RESTAURANT",
                "lat": 45.83208,
                "lng": 1.25682,
                "name": "La princesse au petit pois",
                "rank": 500,
                "tags": [
                    "restaurant",
                    "bar",
                    "tapas"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "6974ED0F44",
                "category": "RESTAURANT",
                "lat": 45.8404,
                "lng": 1.258681,
                "name": "Musée des Distilleries limougeaudes",
                "rank": 500,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "3B66D27B42",
                "category": "SHOPPING",
                "lat": 45.893394,
                "lng": 1.280604,
                "name": "Alinéa",
                "rank": 500,
                "tags": [
                    "shopping",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "C57F1FEE43",
                "category": "NIGHTLIFE",
                "lat": 45.82418,
                "lng": 1.26737,
                "name": "La Fourmi",
                "rank": 500,
                "tags": [
                    "activities",
                    "events",
                    "musicvenue",
                    "pub",
                    "culturalcenter",
                    "bar",
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "699BF65B5F",
                "category": "RESTAURANT",
                "lat": 45.8855,
                "lng": 1.291107,
                "name": "Courtepaille",
                "rank": 500,
                "tags": [
                    "restaurant",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "6B74740CCC",
                "category": "RESTAURANT",
                "lat": 45.830597,
                "lng": 1.254926,
                "name": "Ma Maison",
                "rank": 500,
                "tags": [
                    "restaurant",
                    "activities",
                    "bar"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "55BADFDF11",
                "category": "SIGHTS",
                "lat": 45.83051,
                "lng": 1.257162,
                "name": "Église Saint Michel des Lions",
                "rank": 500,
                "tags": [
                    "church",
                    "sights",
                    "sightseeing",
                    "landmark",
                    "historicplace",
                    "historic",
                    "temple"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "7D55D42F86",
                "category": "RESTAURANT",
                "lat": 45.88516,
                "lng": 1.290751,
                "name": "La Toscana",
                "rank": 500,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "8ADD00E64D",
                "category": "RESTAURANT",
                "lat": 45.832905,
                "lng": 1.25577,
                "name": "Le Paris",
                "rank": 500,
                "tags": [
                    "restaurant",
                    "brewery"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "D1B7E9613F",
                "category": "RESTAURANT",
                "lat": 45.829617,
                "lng": 1.255742,
                "name": "Tacaps",
                "rank": 500,
                "tags": [
                    "fastfood",
                    "hotdogs",
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "14494D0DF4",
                "category": "RESTAURANT",
                "lat": 45.82739,
                "lng": 1.261883,
                "name": "Krisna",
                "rank": 500,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B86E71E293",
                "category": "RESTAURANT",
                "lat": 45.88302,
                "lng": 1.287174,
                "name": "KFC",
                "rank": 500,
                "tags": [
                    "restaurant",
                    "fastfood"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "AE9178BEA6",
                "category": "RESTAURANT",
                "lat": 45.81172,
                "lng": 1.25769,
                "name": "King Long",
                "rank": 500,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "98FD35027C",
                "category": "SIGHTS",
                "lat": 45.83506,
                "lng": 1.24495,
                "name": "Bernardaud",
                "rank": 1000,
                "tags": [
                    "sightseeing",
                    "sights",
                    "shopping",
                    "outlet"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "DFBB160A97",
                "category": "RESTAURANT",
                "lat": 45.829906,
                "lng": 1.256073,
                "name": "Mezzo Di Pasta",
                "rank": 1000,
                "tags": [
                    "fastfood",
                    "hotdogs",
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "236588F8ED",
                "category": "RESTAURANT",
                "lat": 45.83271,
                "lng": 1.255458,
                "name": "Côté Crêpes",
                "rank": 1000,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "0A5C09B255",
                "category": "NIGHTLIFE",
                "lat": 45.82877,
                "lng": 1.260638,
                "name": "L'Amicale des Parachutistes Belges",
                "rank": 500,
                "tags": [
                    "bar",
                    "activities"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "B19B31EC53",
                "category": "SIGHTS",
                "lat": 45.84004,
                "lng": 1.257726,
                "name": "Place Carnot",
                "rank": 1000,
                "tags": [
                    "transport",
                    "square",
                    "sightseeing",
                    "commercialplace",
                    "gasstation",
                    "bus"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "1A2E10EBA9",
                "category": "RESTAURANT",
                "lat": 45.82942,
                "lng": 1.265517,
                "name": "Crêperie de la Cathédrale",
                "rank": 1000,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "ABDCA70F53",
                "category": "RESTAURANT",
                "lat": 45.833244,
                "lng": 1.255208,
                "name": "Kyoto Loft",
                "rank": 1000,
                "tags": [
                    "restaurant"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "A1CA633A15",
                "category": "SHOPPING",
                "lat": 45.83145,
                "lng": 1.259347,
                "name": "Monoprix",
                "rank": 1000,
                "tags": [
                    "shopping",
                    "food",
                    "supermarket",
                    "sightseeing",
                    "commercialplace"
                ]
            },
            {
                "type": "AMADEUS",
                "id": "52006FD238",
                "category": "SIGHTS",
                "lat": 45.827736,
                "lng": 1.267333,
                "name": "Jardin de l'Évêché",
                "rank": 1000,
                "tags": [
                    "garden",
                    "beauty&spas"
                ]
            }
        ],
        "minRank": 5,
        "maxRank": 1000
    }
}

module.exports = findPOIsInCityMocked;
