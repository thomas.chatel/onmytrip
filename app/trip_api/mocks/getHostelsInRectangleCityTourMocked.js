const request = require("../common_libs/request");
const jsonToUrlEncoded = require("../common_libs/jsonToUrlEncoded");

module.exports = async (topLeft, btmRight, categoriesHostel) => {
    return (
        topLeft.lat === 45.928376 &&
        topLeft.lng === 1.146237 &&
        btmRight.lat === 45.788668 &&
        btmRight.lng === 1.317536
    ) ?
        {
            status: 200,
            body: JSON.stringify([
                {
                    "type": "TOMTOM",
                    "id": "250009009288505",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "À la Résidence",
                    "lat": 45.90269,
                    "lng": 1.29552
                },
                {
                    "type": "TOMTOM",
                    "id": "250009008185106",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "HotelF1 Limoges",
                    "lat": 45.88836,
                    "lng": 1.29114
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041095662",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Les Effrontés",
                    "lat": 45.83372,
                    "lng": 1.25953
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009312831",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Hôtel de la Paix",
                    "lat": 45.83163,
                    "lng": 1.26358
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009270504",
                    "categories": [
                        "hotel/motel",
                        "resort"
                    ],
                    "categorySet": [
                        7314005
                    ],
                    "name": "Résidence Temporaire Mobilogis",
                    "lat": 45.82675,
                    "lng": 1.25748
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041122970",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "P'titdéj Hotel Limoges Nord",
                    "lat": 45.88793,
                    "lng": 1.29084
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009289179",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "L'Hôtel L'Aiglon",
                    "lat": 45.84002,
                    "lng": 1.25654
                },
                {
                    "type": "TOMTOM",
                    "id": "250009040998688",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Domaine de la Chabroulie",
                    "lat": 45.79505,
                    "lng": 1.20722
                },
                {
                    "type": "TOMTOM",
                    "id": "250009042424910",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Tui Store",
                    "lat": 45.83029,
                    "lng": 1.25638
                },
                {
                    "type": "TOMTOM",
                    "id": "250009036821892",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Hôtel de Paris",
                    "lat": 45.83454,
                    "lng": 1.26232
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041071084",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Les Carpes",
                    "lat": 45.87234,
                    "lng": 1.18059
                },
                {
                    "type": "TOMTOM",
                    "id": "250009008105166",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Les Balladins Limoges",
                    "lat": 45.88089,
                    "lng": 1.28791
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041101535",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Pervenche",
                    "lat": 45.81379,
                    "lng": 1.31075
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041109208",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Coucou",
                    "lat": 45.86947,
                    "lng": 1.22782
                },
                {
                    "type": "TOMTOM",
                    "id": "250009035044008",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "B&B Hotels",
                    "lat": 45.83492,
                    "lng": 1.26801
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041122335",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Les Bayles",
                    "lat": 45.81202,
                    "lng": 1.22336
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009298869",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Le Relais Gourmet",
                    "lat": 45.87682,
                    "lng": 1.23764
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009270329",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Le Green Saint-Lazare",
                    "lat": 45.81188,
                    "lng": 1.28928
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041020437",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Les Cambuses",
                    "lat": 45.88638,
                    "lng": 1.26775
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041010697",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Beauséjour",
                    "lat": 45.82085,
                    "lng": 1.25484
                },
                {
                    "type": "TOMTOM",
                    "id": "250009035071249",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Hôtel Le Renaissance",
                    "lat": 45.8127,
                    "lng": 1.28081
                },
                {
                    "type": "TOMTOM",
                    "id": "250009006370208",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Première Classe Limoges Nord",
                    "lat": 45.88979,
                    "lng": 1.29103
                },
                {
                    "type": "TOMTOM",
                    "id": "250009036556289",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Appart'City Limoges",
                    "lat": 45.82111,
                    "lng": 1.26408
                },
                {
                    "type": "TOMTOM",
                    "id": "250009042463754",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Isatis Le Spa",
                    "lat": 45.83883,
                    "lng": 1.2571
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009295782",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Vacances Haute-Vienne",
                    "lat": 45.83628,
                    "lng": 1.26377
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009277218",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "La Terrasse d'Annie",
                    "lat": 45.91614,
                    "lng": 1.29905
                },
                {
                    "type": "TOMTOM",
                    "id": "250009034366453",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Hôtel Akena Limoges",
                    "lat": 45.83706,
                    "lng": 1.26473
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009309017",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Hôtel du Parc",
                    "lat": 45.86561,
                    "lng": 1.26999
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041061750",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Crézin",
                    "lat": 45.81496,
                    "lng": 1.3129
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041059560",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Les 2 Moulins",
                    "lat": 45.87494,
                    "lng": 1.2185
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041130811",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Bed and Breakfast",
                    "lat": 45.88863,
                    "lng": 1.2909
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009297503",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Hôtel d'Isly",
                    "lat": 45.83413,
                    "lng": 1.2631
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009314307",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Château De Fontgeaudrant",
                    "lat": 45.80123,
                    "lng": 1.26799
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009310110",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Hôtel des Beaux Arts",
                    "lat": 45.83136,
                    "lng": 1.25432
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009280380",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Le Relais Lamartine",
                    "lat": 45.83655,
                    "lng": 1.25555
                },
                {
                    "type": "TOMTOM",
                    "id": "250009007883048",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Fasthôtel",
                    "lat": 45.88709,
                    "lng": 1.29041
                },
                {
                    "type": "TOMTOM",
                    "id": "250009034928046",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "B&B Hotels Limoges (1)",
                    "lat": 45.88956,
                    "lng": 1.29139
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041061312",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Dunant",
                    "lat": 45.82497,
                    "lng": 1.24438
                },
                {
                    "type": "TOMTOM",
                    "id": "250009036667278",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Hôtel ibis Limoges Centre",
                    "lat": 45.83267,
                    "lng": 1.2551
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009294953",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Hôtel de Lyon",
                    "lat": 45.82605,
                    "lng": 1.26727
                },
                {
                    "type": "TOMTOM",
                    "id": "250008000897534",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "ibis Styles Limoges Centre",
                    "lat": 45.82376,
                    "lng": 1.26
                },
                {
                    "type": "TOMTOM",
                    "id": "250009036741851",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Hôtel du Golf",
                    "lat": 45.81121,
                    "lng": 1.28221
                },
                {
                    "type": "TOMTOM",
                    "id": "250009036671130",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Inter Hotel",
                    "lat": 45.83742,
                    "lng": 1.26193
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009302291",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Maison Diocesaine",
                    "lat": 45.82629,
                    "lng": 1.27303
                },
                {
                    "type": "TOMTOM",
                    "id": "250009003875151",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "The Originals City Limoges Sud-Feytiat",
                    "lat": 45.80969,
                    "lng": 1.29889
                },
                {
                    "type": "TOMTOM",
                    "id": "250009007931775",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Novotel Hotel Limoges",
                    "lat": 45.8672,
                    "lng": 1.27094
                },
                {
                    "type": "TOMTOM",
                    "id": "250008000027484",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Campanile Limoges Centre-Gare",
                    "lat": 45.83586,
                    "lng": 1.26217
                },
                {
                    "type": "TOMTOM",
                    "id": "250009040079898",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Le Marceau",
                    "lat": 45.83938,
                    "lng": 1.25934
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009315658",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Hôtel Boni",
                    "lat": 45.83629,
                    "lng": 1.26012
                },
                {
                    "type": "TOMTOM",
                    "id": "250009042522694",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Citôtel Jeanne d'Arc",
                    "lat": 45.8337,
                    "lng": 1.27476
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041046716",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Tranchepie",
                    "lat": 45.86966,
                    "lng": 1.14899
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041023401",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Dupain et Dubeurre",
                    "lat": 45.83737,
                    "lng": 1.26385
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041027987",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Les Acacias",
                    "lat": 45.91135,
                    "lng": 1.24518
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009297708",
                    "categories": [
                        "hostel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314004
                    ],
                    "name": "La Chapelle Saint-Martin",
                    "lat": 45.8927,
                    "lng": 1.17866
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009292395",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Amphitryon",
                    "lat": 45.82871,
                    "lng": 1.25768
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041020620",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Le Roussillon",
                    "lat": 45.81566,
                    "lng": 1.22256
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009302918",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Hôtel de France",
                    "lat": 45.83428,
                    "lng": 1.2636
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009300984",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Le Celtic",
                    "lat": 45.86295,
                    "lng": 1.23809
                },
                {
                    "type": "TOMTOM",
                    "id": "250009042470509",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Hôtel Familia",
                    "lat": 45.83707,
                    "lng": 1.26475
                },
                {
                    "type": "TOMTOM",
                    "id": "250009036772433",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Hôtel Arion Limoges",
                    "lat": 45.88629,
                    "lng": 1.29116
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041087751",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Le Poudrier",
                    "lat": 45.8555,
                    "lng": 1.31375
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009291320",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Les Alizés Hôtel",
                    "lat": 45.86175,
                    "lng": 1.17403
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041098545",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "L'Hermitage de Marie",
                    "lat": 45.83477,
                    "lng": 1.24775
                },
                {
                    "type": "TOMTOM",
                    "id": "250009034291324",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Hôtel Mercure Limoges Royal Limousin",
                    "lat": 45.83198,
                    "lng": 1.26102
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041107868",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "La Terrasse",
                    "lat": 45.83165,
                    "lng": 1.25882
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009297132",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Éco'Hôtel 87",
                    "lat": 45.81522,
                    "lng": 1.2772
                },
                {
                    "type": "TOMTOM",
                    "id": "250009036644842",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Art Hôtel Tendance",
                    "lat": 45.83816,
                    "lng": 1.26122
                },
                {
                    "type": "TOMTOM",
                    "id": "250009007940341",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "L'Atrium",
                    "lat": 45.83455,
                    "lng": 1.26818
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041049297",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Le Saint-Martial",
                    "lat": 45.83722,
                    "lng": 1.26165
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041103252",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Le Puy De L'eau",
                    "lat": 45.91117,
                    "lng": 1.24507
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009286097",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Home du Buisson",
                    "lat": 45.81091,
                    "lng": 1.23273
                },
                {
                    "type": "TOMTOM",
                    "id": "250009036805545",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Best Western Hôtel Richelieu",
                    "lat": 45.82526,
                    "lng": 1.25738
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009304980",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Domaine de Faugeras",
                    "lat": 45.85221,
                    "lng": 1.28944
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041031917",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Chez Le Bayle",
                    "lat": 45.78977,
                    "lng": 1.22858
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041093316",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Ponticaud",
                    "lat": 45.83088,
                    "lng": 1.26971
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041102899",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Les Arts",
                    "lat": 45.83583,
                    "lng": 1.2491
                },
                {
                    "type": "TOMTOM",
                    "id": "250009042446387",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Naïa Spa",
                    "lat": 45.83023,
                    "lng": 1.26076
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009265929",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Le Saint Exupéry",
                    "lat": 45.83614,
                    "lng": 1.26298
                },
                {
                    "type": "TOMTOM",
                    "id": "250009004331658",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Sure Hotel by Best Western Limoges Sud",
                    "lat": 45.80706,
                    "lng": 1.29867
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041116361",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "La Métaierie",
                    "lat": 45.79685,
                    "lng": 1.20586
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041050567",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "La Garde",
                    "lat": 45.88189,
                    "lng": 1.22302
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041012819",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Place De La République",
                    "lat": 45.83158,
                    "lng": 1.25915
                },
                {
                    "type": "TOMTOM",
                    "id": "250009008180333",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Campanile Limoges Nord",
                    "lat": 45.8654,
                    "lng": 1.26874
                },
                {
                    "type": "TOMTOM",
                    "id": "250009036573741",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Hôtel Jeanne d'Arc",
                    "lat": 45.8341,
                    "lng": 1.26555
                },
                {
                    "type": "TOMTOM",
                    "id": "250009036832970",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Kyriad Limoges Sud-Feytiat",
                    "lat": 45.81734,
                    "lng": 1.29695
                },
                {
                    "type": "TOMTOM",
                    "id": "250009000151821",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Hôtel de la Poste",
                    "lat": 45.83709,
                    "lng": 1.26618
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009298992",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Le Colibri",
                    "lat": 45.91432,
                    "lng": 1.29901
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009304016",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Hôtel Lion d'Or",
                    "lat": 45.83282,
                    "lng": 1.26457
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009316062",
                    "categories": [
                        "cabins lodges",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314007
                    ],
                    "name": "Le Poudrier",
                    "lat": 45.84495,
                    "lng": 1.29259
                },
                {
                    "type": "TOMTOM",
                    "id": "250009036624925",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "ibis budget",
                    "lat": 45.83859,
                    "lng": 1.2657
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041007184",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Chantegrelle",
                    "lat": 45.91432,
                    "lng": 1.24759
                },
                {
                    "type": "TOMTOM",
                    "id": "250009009246974",
                    "categories": [
                        "hotel",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314003
                    ],
                    "name": "Bienvenue Hotel Limoges",
                    "lat": 45.87559,
                    "lng": 1.2865
                },
                {
                    "type": "TOMTOM",
                    "id": "250009041117927",
                    "categories": [
                        "bed breakfast guest houses",
                        "hotel/motel"
                    ],
                    "categorySet": [
                        7314002
                    ],
                    "name": "Le Clos Jargot",
                    "lat": 45.82773,
                    "lng": 1.27474
                }
            ].filter(({categorySet}) =>
                !categoriesHostel ||
                categoriesHostel.length === 0 ||
                categorySet.some(categoryId => categoriesHostel.includes(categoryId))
            ))
        } :
        {
            status: 404,
            body: "Not found"
        }
}