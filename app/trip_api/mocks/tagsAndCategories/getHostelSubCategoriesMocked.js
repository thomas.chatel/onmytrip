module.exports = async function getHostelSubCategoriesMocked() {
    return [
        {
            "id": 7314002,
            "name": "B&B/Chambre d'Hôtes",
            "childCategoryIds": [],
            "synonyms": [
                "Couette & Café",
                "Café-Couette",
                "Chambres d'Hôtes",
                "B&B",
                "Chambre d'Hôtes",
                "Bed & Breakfast"
            ]
        },
        {
            "id": 7314007,
            "name": "Cabanes",
            "childCategoryIds": [],
            "synonyms": [
                "Hôtel Pavillonnaire",
                "Chalet",
                "Maisons de Bois",
                "Hôtel à Pavillons",
                "Pavillons de Montagne",
                "Gîte"
            ]
        }
    ]
}