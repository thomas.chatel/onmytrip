module.exports = function getTagsAndCategoriesMocked(translationsTags) {
    const tags = [
        {
            "name": "sightseeing",
            "categories": [
                "SIGHTS",
                "RESTAURANT",
                "NIGHTLIFE",
                "SHOPPING"
            ]
        },
        {
            "name": "restaurant",
            "categories": [
                "SIGHTS",
                "RESTAURANT",
                "NIGHTLIFE",
                "SHOPPING"
            ]
        },
        {
            "name": "tourguide",
            "categories": [
                "SIGHTS",
                "RESTAURANT",
                "NIGHTLIFE",
                "SHOPPING"
            ]
        },
        {
            "name": "sights",
            "categories": [
                "SIGHTS",
                "RESTAURANT",
                "SHOPPING"
            ]
        },
        {
            "name": "park",
            "categories": [
                "SIGHTS",
                "SHOPPING"
            ]
        },
        {
            "name": "landmark",
            "categories": [
                "SIGHTS",
                "SHOPPING",
                "RESTAURANT"
            ]
        },
        {
            "name": "historicplace",
            "categories": [
                "SIGHTS",
                "RESTAURANT",
                "SHOPPING",
                "NIGHTLIFE"
            ]
        },
        {
            "name": "historic",
            "categories": [
                "SIGHTS"
            ]
        }
    ].map(({name,categories}) => ({
        name,
        categories,
        ...Object.keys(translationsTags).reduce((acc, language) => ({
            ...acc,
            ['translation' + language.toUpperCase()]: translationsTags[language][name] ?? name
        }), {}),
    }))

    return {
        tags,
        categories: Object.keys(tags.reduce((acc,{categories}) => ({
            ...categories.reduce((acc,category) => ({
                ...acc,
                [category]: true
            }), acc)
        }), {}))
    }
}