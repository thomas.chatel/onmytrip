const HostelSubCategory = require("./models/HostelSubCategory");
const logger = require("./common_libs/logger");
const getHostelSubCategories = require("./libs/tagsAndCategories/getHostelSubCategories");

async function initHostelSubCategories() {
    const firstHostelSubCategory = await HostelSubCategory.findOne();

    if (firstHostelSubCategory)
        return;

    logger.info("Start fetching all hostel sub categories");

    const subCategories = await getHostelSubCategories();

    await Promise.all(subCategories.map(subCategory => HostelSubCategory.create({
        ...subCategory,
        name_lower: subCategory.name.toLowerCase(),
        synonyms: subCategory.synonyms.map(synonym => synonym.toLowerCase())
    })));

    logger.info("Finished getting all hostel sub categories");
}

module.exports = initHostelSubCategories;