const logger = require("./common_libs/logger");
const getTagsAndCategories = require("./libs/tagsAndCategories/getTagsAndCategories");
const Tag = require("./models/Tag");
const Category = require("./models/Category");
const fs = require("fs/promises");

async function initTagsAndCategories() {
    const firstTag = await Tag.findOne({});
    const firstCategory = await Category.findOne({});
    if (firstTag && firstCategory) return;

    await Tag.deleteMany();
    await Category.deleteMany();

    const translationsTags = await fs.readFile(__dirname + "/translationsTags.json")
        .then(chunk => chunk.toString())
        .then(str => JSON.parse(str));

    logger.info("Start getting tags and categories");

    const {tags,categories} = await getTagsAndCategories(translationsTags);

    await Promise.all(tags.map(tag => Tag.create(tag)));
    await Promise.all(categories.map(category => Category.create({name: category})))

    logger.info("Tags and categories registering finished");
}

module.exports = initTagsAndCategories;