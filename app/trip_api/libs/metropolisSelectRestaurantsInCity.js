const {RESTAURANT_CATEGORY,maxNbRestaurantsInChoice,POITYPE_RESTAURANT} = require("../config");
const calcDistance = require("./calcDistance");
const calcRestaurantPositions = require("./calcRestaurantPositions");
const rand = require("./rand");
const gauss = require("./gauss");

function metropolisSelectRestaurantsInCity(nbDays, nbStepsByDay,chosenPOIs,POIs,interestsRestaurant) {
    const restaurantPositions = calcRestaurantPositions(nbDays,nbStepsByDay);

    for (let i=0;i<restaurantPositions.length;i++) {
        const {POIIndex,restaurants} = restaurantPositions[i];

        let P = null;

        const A = chosenPOIs[POIIndex];
        const B = chosenPOIs[POIIndex+1];

        if (!A || !B)
            continue;

        const {lat: lat1, lng: lng1} = A;
        const {lat: lat2, lng: lng2} = B;

        const d12 = calcDistance(lat1,lng1,lat2,lng2);

        const latAvg = (lat1+lat2)/2;
        const lngAvg = (lng1+lng2)/2;

        const nbTriesByPOIId = {};
        while (restaurants.length < maxNbRestaurantsInChoice && POIs.length > 0) {
            const index = rand(0,POIs.length-1);
            const POI = POIs[index];

            if (
                POI.category !== RESTAURANT_CATEGORY ||
                (
                    interestsRestaurant instanceof Array &&
                    interestsRestaurant.length > 0 &&
                    !interestsRestaurant.some(interest => POI.tags.includes(interest))
                )
            ) {
                POIs.splice(index,1);
                continue;
            }

            const {lat: latR, lng: lngR} = POI;

            const d = calcDistance(latR,lngR, latAvg, lngAvg);
            const curP = gauss(d, d12/4);
            if (P === null || curP > P || Math.random() < curP/P || (nbTriesByPOIId[POI.id]??0) >= 1000*POIs.length) {
                if (P !== null) {
                    delete nbTriesByPOIId[POI.id];
                    POIs.splice(index,1);
                    restaurants.push({
                        ...POI,
                        nbDay: i+1,
                        POIType: POITYPE_RESTAURANT
                    });
                }
                P = curP;
            } else {
                nbTriesByPOIId[POI.id] = nbTriesByPOIId[POI.id] ? nbTriesByPOIId[POI.id]+1 : 1;
            }
        }
    }
    return restaurantPositions;
}

module.exports = metropolisSelectRestaurantsInCity;