const POIIsInPolygon = require("./POIIsInPolygon");
const getContourFarthestPoint = require("./getContourFarthestPoint");
const request = require("../common_libs/request");
const jsonToUrlEncoded = require("../common_libs/jsonToUrlEncoded");
const CustomError = require("../common_libs/CustomError");
const {POI_RADIUS} = process.env;

async function findPOIsInCity(center, contour) {
    const maxD = getContourFarthestPoint(center,contour);

    const radius = Math.min(1+Math.floor(maxD), parseInt(POI_RADIUS));

    let POIs = await request(`${process.env.POI_API_HOST}/poi?`+jsonToUrlEncoded({
        lat: center.lat,
        lng: center.lng,
        radius
    }), {port: 3000}).then(({status,body}) =>
        status === 200 ?
            JSON.parse(body) :
            (() => {throw new CustomError("Can't find city POIs", {center, radius}, status)})()
    );


    let minRank = null, maxRank = null;
    POIs = POIs.filter(({lat,lng,rank}) => {
        if (minRank === null || rank < minRank)
            minRank = rank;
        if (maxRank === null || rank > maxRank)
            maxRank = rank;
        return POIIsInPolygon(lat, lng, contour);
    })

    return {POIs, minRank, maxRank};
}

module.exports = findPOIsInCity;
