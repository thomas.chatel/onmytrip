const o = 5; // Ecart type
const u = 0; // Esperance

module.exports = (x, _o = o) => 1/(_o*Math.sqrt(2*Math.PI))*Math.exp(-1*((x-u)**2)/(2*_o**2))
