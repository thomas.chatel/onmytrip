const radians = require("./radians");

const calcDistance = (lat1,lng1,lat2,lng2) =>
	Math.acos(Math.sin(radians(lat1))*Math.sin(radians(lat2))+Math.cos(radians(lat1))*Math.cos(radians(lat2))*Math.cos(radians(lng1-lng2)))*6371

module.exports = calcDistance;
