const rand = require("./rand");
const gauss = require("./gauss");
const {categoriesToSkipForPoi, POITYPE_POI} = require("../config");
const calcDistanceBetweenPOIAndRoad = require("./calcDistanceBetweenPOIAndRoad");

function metropolisSelectPOIs(totalNbSteps,POIs,interests,minRank,maxRank,road = null) {
    const chosenPOIs = [];

    const POIsToCheck = POIs.filter(({category,tags}) =>
        !categoriesToSkipForPoi.includes(category) &&
        (!(interests instanceof Array) || interests.length === 0 || interests.some(interest => tags.includes(interest)))
    );

    const nbTriesByPOIId = {};

    let P = null;
    while (chosenPOIs.length < totalNbSteps && POIsToCheck.length > 0) {
        const POIIndex = rand(0,POIsToCheck.length-1)
        const POI = POIsToCheck[POIIndex];

        const {lat,lng} = POI;

        let curDP = 1;
        if (road) {
            const d = calcDistanceBetweenPOIAndRoad(road,lat,lng, true);
            curDP = gauss(d);
        }

        const curRP = 1 - ((POI.rank-minRank)/(maxRank-minRank)*1.001);
        const curP = curDP*curRP;
        if (P === null || curP > P || Math.random() <= curP/P || (nbTriesByPOIId[POI.id]??0) > 1000*POIsToCheck.length) {
            if (P !== null) {
                chosenPOIs.push({
                    ...POI,
                    POIType: POITYPE_POI
                });
                POIsToCheck.splice(POIIndex,1);
            }
            P = curP;
        } else {
            nbTriesByPOIId[POI.id] = nbTriesByPOIId[POI.id] ? nbTriesByPOIId[POI.id]+1 : 1;
        }
    }

    return chosenPOIs;
}

module.exports = metropolisSelectPOIs;