const calcDistanceBetweenPOIAndRoad = require("./calcDistanceBetweenPOIAndRoad");
const rand = require("./rand");
const gauss = require("./gauss");
const {maxNbHostelsInChoice, POITYPE_HOSTEL} = require("../config");

function metropolisSelectHostels(hostels,road) {
    const chooseHostels = [];

    const nbTriesByPOIId = {};

    let P = null
    while (chooseHostels.length < maxNbHostelsInChoice && hostels.length > 0) {
        const currentHostelIndex = rand(0,hostels.length-1);
        const currentHostel = hostels[currentHostelIndex];

        const {lat,lng} = currentHostel

        const d = calcDistanceBetweenPOIAndRoad(road,lat,lng);
        const curP = gauss(d);
        if (P === null || curP > P || Math.random() <= curP/P || (nbTriesByPOIId[currentHostel.id]??0) > 1000*hostels.length) {
            if (P !== null) {
                chooseHostels.push({
                    ...currentHostel,
                    POIType: POITYPE_HOSTEL
                })
                hostels.splice(currentHostelIndex,1);
            }
            P = curP
        } else {
            nbTriesByPOIId[currentHostel.id] = nbTriesByPOIId[currentHostel.id] ? nbTriesByPOIId[currentHostel.id]+1 : 1;
        }
    }
    return chooseHostels;
}

module.exports = metropolisSelectHostels;