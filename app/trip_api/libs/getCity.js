const request = require("../common_libs/request");

module.exports = function getCity(codeCommune) {
    return request(
        process.env.ROUTE_API_HOST + "/city/" + codeCommune,
        { port: 3000 }
    ).then(({status,body}) => ({
        status,
        body: status === 200 ? JSON.parse(body) : body
    }))
}