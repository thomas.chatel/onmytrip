const calcDistance = require("./calcDistance");
const rand = require("./rand");

function calcTotalDistance(chosenPOIs,from = null,to = null) {
    let totalDistance = 0;
    if (from) {
        totalDistance += calcDistance(from.lat, from.lng, chosenPOIs[0].lat, chosenPOIs[0].lng);
    }
    for (let i=0;i<chosenPOIs.length-1;i++) {
        totalDistance += calcDistance(chosenPOIs[i].lat,chosenPOIs[i].lng,chosenPOIs[i+1].lat,chosenPOIs[i+1].lng);
    }
    if (to) {
        totalDistance += calcDistance(chosenPOIs[chosenPOIs.length - 1].lat, chosenPOIs[chosenPOIs.length - 1].lng, to.lat, to.lng);
    }
    return totalDistance;
}

function selectionSort(chosenPOIs,from = null) {
    for (let i=from ? -1 : 0;i<chosenPOIs.length-1;i++) {
        const {lat: lat1, lng: lng1} = i === -1 ? from : chosenPOIs[i];
        let min = null;
        for (let j=i+1;j<chosenPOIs.length;j++) {
            const {lat: lat2, lng: lng2} = chosenPOIs[j];
            if (min === null || calcDistance(lat1, lng1, lat2, lng2))
                min = j;
        }
        if (min !== i+1) {
            let tmp = chosenPOIs[i+1];
            chosenPOIs[i+1] = chosenPOIs[min];
            chosenPOIs[min] = tmp;
        }
    }
}

function randomChanges(chosenPOIs) {
    let A = rand(0,chosenPOIs.length-1);
    let B = rand(0,chosenPOIs.length-1);
    while (A === B) {
        B = rand(0,chosenPOIs.length-1);
    }
    let tmp = chosenPOIs[A];
    chosenPOIs[A] = chosenPOIs[B];
    chosenPOIs[B] = tmp;
    return chosenPOIs;
}

function simulatedAnnealingSortPOIs(chosenPOIs, from = null, to = null) {
    if (chosenPOIs.length <= 1)
        return chosenPOIs;
    selectionSort(chosenPOIs,from);

    let T = 30;
    const nbIters = 500000;
    const tScale = T/nbIters;

    let len1 = calcTotalDistance(chosenPOIs,from,to);

    for (let i=0;i<nbIters;i++) {
        const newChosenPOIs = randomChanges([...chosenPOIs]);
        const len2 = calcTotalDistance(newChosenPOIs,from,to);
        let d;
        if (len2 <= len1 || ((d = len1-len2) && Math.random() <= Math.exp(d/T)) ) {
            len1 = len2;
            chosenPOIs = newChosenPOIs;
        }
        T -= tScale;
    }
    return chosenPOIs;
}

module.exports = simulatedAnnealingSortPOIs;