const {RESTAURANT_CATEGORY,maxNbRestaurantsInChoice,POITYPE_RESTAURANT} = require("../config");
const calcDistance = require("./calcDistance");
const calcRestaurantPositions = require("./calcRestaurantPositions");
const rand = require("./rand");

function findRestaurants(nbDays, nbStepsByDay,chosenPOIs,POIs,interestsRestaurant) {
    const restaurantPositions = calcRestaurantPositions(nbDays,nbStepsByDay);

    let nbRestaurants = 0;
    while (nbRestaurants < restaurantPositions.length*maxNbRestaurantsInChoice && POIs.length > 0) {
        const index = rand(0,POIs.length-1);
        const POI = POIs[index];
        const {lat: latR, lng: lngR} = POI;

        let restaurantPositionIndex = null;
        let restaurantPosition = null;
        if (
            POI.category === RESTAURANT_CATEGORY &&
            (
                !(interestsRestaurant instanceof Array) ||
                interestsRestaurant.length === 0 ||
                interestsRestaurant.some(interest => POI.tags.includes(interest))
            ) &&
            (restaurantPosition = restaurantPositions.find(({POIIndex},index) => {
                const A = chosenPOIs[POIIndex];
                const B = chosenPOIs[POIIndex+1];
                if (!A || !B)
                    return false;

                const {lat: lat1, lng: lng1} = A;
                const {lat: lat2, lng: lng2} = B;
                const d12 = calcDistance(lat1,lng1,lat2,lng2);
                const d1R = calcDistance(lat1,lng1,latR,lngR);
                const d2R = calcDistance(lat2,lng2,latR,lngR);

                return d1R <= d12 && d2R <= d12 && ((restaurantPositionIndex = index) || true)
            })) &&
            restaurantPosition.restaurants.length < maxNbRestaurantsInChoice
        ) {
            nbRestaurants += 1;
            restaurantPosition.restaurants.push({
                ...POI,
                nbDay: restaurantPositionIndex+1,
                POIType: POITYPE_RESTAURANT
            });
        }
        POIs.splice(index,1);
    }

    return restaurantPositions;
}

module.exports = findRestaurants;