const {coordinatesToGetTags, radiusToGetTags} = require("../../config");
const request = require("../../common_libs/request");
const jsonToUrlEncoded = require("../../common_libs/jsonToUrlEncoded");

module.exports = function getTagsAndCategories(translationsTags) {
    return Promise.all(coordinatesToGetTags.map(({lat,lng}) =>
        request(`${process.env.POI_API_HOST}/poi?`+jsonToUrlEncoded({
            lat, lng, radius: radiusToGetTags
        }), {
            port: 3000
        }).then(({body}) => JSON.parse(body))
    )).then(responses => {
        const categoriesObject = {};
        const tagsObject = {};
        for (const response of responses)
            for (const POI of response)
                for (const tag of POI.tags) {
                    if (tagsObject[tag])
                        tagsObject[tag].categories[POI.category] = true;
                    else
                        tagsObject[tag] = {
                            name: tag,
                            categories: {[POI.category]: true}
                        };
                    categoriesObject[POI.category] = true;

                }
        return {
            tags: Object.values(tagsObject).map(({name,categories}) => ({
                    name,
                    ...Object.keys(translationsTags).reduce((acc, language) => ({
                        ...acc,
                        ['translation' + language.toUpperCase()]: translationsTags[language][name] ?? name
                    }), {}),
                    categories: Object.keys(categories)
                })
            ),
            categories: Object.keys(categoriesObject)
        };
    });
}