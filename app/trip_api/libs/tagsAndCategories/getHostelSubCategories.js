const request = require("../../common_libs/request");

module.exports = function getHostelSubCategories() {
    return request(`${process.env.POI_API_HOST}/hostel_categories`,
        {port: 3000})
        .then(({body}) => JSON.parse(body));
}