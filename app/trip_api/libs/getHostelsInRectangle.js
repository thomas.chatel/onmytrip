const request = require("../common_libs/request");
const jsonToUrlEncoded = require("../common_libs/jsonToUrlEncoded");

module.exports = (topLeft,btmRight,categoriesHostel) =>
    request(`${process.env.POI_API_HOST}/poi/hostel?` + jsonToUrlEncoded({
        topLeft: [topLeft.lat, topLeft.lng].join(","),
        btmRight: [btmRight.lat, btmRight.lng].join(","),
        ...((categoriesHostel && categoriesHostel.length > 0) ? {categorySet: categoriesHostel.join(",")} : {})
    }), {port: 3000});