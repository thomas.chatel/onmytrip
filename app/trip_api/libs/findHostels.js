const getHostelsInRectangle = require("./getHostelsInRectangle");
const metropolisSelectHostels = require("./metropolisSelectHostels");
const calcDistance = require("./calcDistance");
const { kmToLat, kmToLng } = require("./latlngConverters");
const CustomError = require("../common_libs/CustomError");

async function findHostels(nbDays,nbStepsByDay,chosenPOIs,categoriesHostel,road) {
    const hostelPositions = [];
    let currentPOIIndex = 0;
    for (let nbDay=1;nbDay<=nbDays;nbDay++) {
        if (nbDay > 1) {
            let [A,B] = [chosenPOIs[currentPOIIndex-1], chosenPOIs[currentPOIIndex]]
            let {lat: lat1, lng: lng1} = A;
            let {lat: lat2, lng: lng2} = B;

            const latitudeAverage = (lat1+lat2)/2;
            const longitudeAverage = (lng1+lng2)/2;

            const vertical = calcDistance(lat1,longitudeAverage,lat2,longitudeAverage) >= calcDistance(latitudeAverage,lng1,latitudeAverage,lng2);
            if (
                (vertical && lat1 < lat2) ||
                (!vertical && lng1 > lng2)
            ) {
                [lat1,lat2] = [lat2,lat1];
                [lng1,lng2] = [lng2, lng1];
                [A,B] = [B,A]
            }


            const topLeft = {
                lat: vertical ? lat1 : latitudeAverage+Math.max(kmToLat(20),Math.abs(lat1-lat2)/2),
                lng: vertical ? longitudeAverage-Math.max(kmToLng(20,latitudeAverage),Math.abs(lng1-lng2)/2) : lng1
            }
            const btmRight = {
                lat: vertical ? lat2 : latitudeAverage-Math.max(kmToLat(20),Math.abs(lat1-lat2)/2),
                lng: vertical ? longitudeAverage+Math.max(kmToLng(20,latitudeAverage),Math.abs(lng1-lng2)/2) : lng2
            }
            const hostels = await getHostelsInRectangle(topLeft,btmRight,categoriesHostel)
                .then(({body,status}) =>
                    status === 200 ?
                        metropolisSelectHostels(JSON.parse(body), road) :
                        (() => { throw new CustomError("Can't find hostels in road trip", {topLeft,btmRight,categoriesHostel,status,body}, status) })()
            ).then(hostels => hostels.map(hostel => ({
                ...hostel,
                nbDay: nbDay-1
            })))
            hostelPositions.push({POIIndex: currentPOIIndex, hostels});
        }
        const nbSteps = nbStepsByDay[nbDay]??nbStepsByDay.all;
        currentPOIIndex += nbSteps;
    }
    return hostelPositions;
}

module.exports = findHostels;
