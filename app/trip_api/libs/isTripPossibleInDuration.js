const {nbActiveHourByDay} = require("../config");

function isTripPossibleInDuration(nbStepsByDay,nbDays,minimumDurationByStep,routeTime = null) {
    let totalNbSteps = 0;
    for (const [day,nbStep] of Object.entries(nbStepsByDay)) {
        if (day !== "all")
            totalNbSteps += nbStep
    }
    if (nbStepsByDay.all)
        totalNbSteps += nbStepsByDay.all * (nbDays-Object.keys(nbStepsByDay).length+1);

    const nbStepTimes = totalNbSteps*minimumDurationByStep;

    const totalTimes = routeTime ? routeTime+nbStepTimes : nbStepTimes;

    const totalRoadTripHours = nbDays*nbActiveHourByDay;

    return totalTimes/3600 <= totalRoadTripHours ? totalNbSteps : false;
}

module.exports = isTripPossibleInDuration;