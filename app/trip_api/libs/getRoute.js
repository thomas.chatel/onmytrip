const request = require("../common_libs/request");
const jsonToUrlEncoded = require("../common_libs/jsonToUrlEncoded")

module.exports = (from,to) =>
    request(
        process.env.ROUTE_API_HOST + "/route?" +
        jsonToUrlEncoded({
            lat1: from.lat,
            lng1: from.lng,
            lat2: to.lat,
            lng2: to.lng
        }),
        {
            port: 3000
        }
    ).then(({body: routeRes, status}) => ({
        routeRes: status === 200 ? JSON.parse(routeRes) : routeRes,
        status
    }))