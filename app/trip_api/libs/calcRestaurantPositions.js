function calcRestaurantPositions(nbDays,nbStepsByDay) {
    let currentPOIIndex = 0;

    const restaurantPositions = [];

    for (let n=1;n<=nbDays;n++) {
        let nbSteps = nbStepsByDay[n] ?? nbStepsByDay.all;
        const choosePOIIndex = currentPOIIndex + Math.floor(nbSteps/2)-1;
        restaurantPositions.push({
            POIIndex: choosePOIIndex,
            restaurants: []
        })

        currentPOIIndex += nbSteps;
    }
    return restaurantPositions;
}

module.exports = calcRestaurantPositions;