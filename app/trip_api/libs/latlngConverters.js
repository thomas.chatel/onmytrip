const radians = require("./radians");

const kmToLat = km => km/110.574
const kmToLng = (km,lat) => km/(111.320*Math.cos(radians(lat)))

module.exports = { kmToLat, kmToLng }