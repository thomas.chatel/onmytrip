const POIIsInPolygon = require("./POIIsInPolygon");
const {maxNbHostelsInChoice,POITYPE_HOSTEL} = require("../config");
const rand = require("./rand");
const gauss = require("./gauss");
const calcDistance = require("./calcDistance");

function metropolisSelectHostelsInCity(hostels, contour, chosenPOIs) {
    let sumPOIsLat = 0;
    let sumPOIsLng = 0;
    for (const {lat,lng} of chosenPOIs) {
        sumPOIsLat += lat;
        sumPOIsLng += lng;
    }

    const averagePOIsLat = sumPOIsLat/chosenPOIs.length;
    const averagePOIsLng = sumPOIsLng/chosenPOIs.length;

    const chosenHostels = [];

    const nbTriesByPOIId = {};

    let P = null
    while (chosenHostels.length < maxNbHostelsInChoice && hostels.length > 0) {
        const hostelIndex = rand(0, hostels.length - 1);
        const hostel = hostels[hostelIndex];
        if (!POIIsInPolygon(hostel.lat, hostel.lng, contour)) {
            hostels.splice(hostelIndex,1);
            continue;
        }

        const {lat, lng} = hostel;

        const d = calcDistance(averagePOIsLat,averagePOIsLng,lat,lng);
        const curP = gauss(d, 1);
        if (P === null || curP > P || Math.random() < curP/P || (nbTriesByPOIId[hostel.id]??0) > 1000*hostels.length) {
            if (P !== null) {
                hostels.splice(hostelIndex,1);
                chosenHostels.push({
                    ...hostel,
                    POIType: POITYPE_HOSTEL
                })
            }
            P = curP;
        } else {
            nbTriesByPOIId[hostel.id] = nbTriesByPOIId[hostel.id] ? nbTriesByPOIId[hostel.id]+1 : 1;
        }
    }

    return chosenHostels;
}

module.exports = metropolisSelectHostelsInCity;