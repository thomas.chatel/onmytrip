const calcDistance = require("./calcDistance");
const jsonToUrlEncoded = require("../common_libs/jsonToUrlEncoded");
const request = require("../common_libs/request");
const CustomError = require("../common_libs/CustomError");
const {POI_RADIUS} = process.env;

function getPOIsAlongTheRoad(road) {
    const promises = [];

    let [ lastLng, lastLat ] = road[0];
    for (let i=1; i<road.length;i++) {
        const [lng, lat] = road[i];
        if (calcDistance(lastLat,lastLng,lat,lng) >= POI_RADIUS) {
            lastLat = lat;
            lastLng = lng;
            promises.push(request(`${process.env.POI_API_HOST}/poi?`+jsonToUrlEncoded({
                lat,
                lng,
                radius: POI_RADIUS
            }), { port: 3000 })
                .then(({status,body}) =>
                    status === 200 ?
                        JSON.parse(body) :
                        (() => {throw new CustomError("Can't get POIs", {lat,lng,radius: POI_RADIUS,status,body}, status)})()
                )
            )
        }
    }

    return Promise.all(promises).then(responses => {
        let minRank = null, maxRank = null;
        const POIObject = {};
        for (const response of responses) {
            for (const POI of response) {
                POIObject[POI.id] = POI
                if (minRank === null || POI.rank < minRank)
                    minRank = POI.rank
                if (maxRank === null || POI.rank > maxRank)
                    maxRank = POI.rank
            }
        }
        return {POIs: Object.values(POIObject), minRank, maxRank};
    });
}

module.exports = getPOIsAlongTheRoad;
