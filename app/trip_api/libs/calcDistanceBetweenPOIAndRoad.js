const {POI_RADIUS} = process.env;
const calcDistance = require("./calcDistance");

function calcDistanceBetweenPOIAndRoad(road,lat,lng,checkPOIRadius = false) {
    let d = null;
    for (const [lngR,latR] of road) {
        const curD = calcDistance(lat,lng,latR,lngR);
        if (checkPOIRadius && curD > POI_RADIUS) {
            if (d === null)
                continue;
            else
                break;
        }
        if (d === null || curD < d)
            d = curD;
    }
    return d;
}

module.exports = calcDistanceBetweenPOIAndRoad;