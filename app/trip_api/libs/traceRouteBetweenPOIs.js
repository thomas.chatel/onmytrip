const request = require("../common_libs/request");
const jsonToUrlEncoded = require("../common_libs/jsonToUrlEncoded");

function traceRouteBetweenPOIs(choosePOIs,from = null,to = null) {
    const promises = [];
    for (let i=from ? -1 : 0;i<(to ? choosePOIs.length : (choosePOIs.length-1));i++) {
        let A = i, B = i+1;
        const {lat: lat1, lng: lng1} = A === -1 ? from : (A === choosePOIs.length ? to : choosePOIs[A]);
        const {lat: lat2, lng: lng2} = B === -1 ? from : (B === choosePOIs.length ? to : choosePOIs[B]);
        promises.push(request(`${process.env.ROUTE_API_HOST}/route?`+jsonToUrlEncoded({
            lat1, lng1, lat2, lng2
        }), {port: 3000}));
    }
    return Promise.all(promises).then(responses => responses.reduce((acc,{body, status}) => {
        body = JSON.parse(body);
        if (status !== 200 || (body.statusCode && body.statusCode !== 200))
            return acc;
        return [...acc, body.features[0].geometry.coordinates[0]];
    }, []));
}

module.exports = traceRouteBetweenPOIs;