const calcDistance = require("./calcDistance");

function getContourFarthestPoint(center, contour, max = null) {
    if (max === null)
        max = {
            d: null,
            lat: null,
            lng: null
        }
    for (const elem of contour) {
        if (elem[0] instanceof Array) {
            getContourFarthestPoint(center, elem, max)
            continue;
        }
        const [lng,lat] = elem
        const d = calcDistance(center.lat,center.lng,lat,lng);
        if (max.d === null ||  d > max.d) {
            max.d = d;
            max.lat = lat;
            max.lng = lng;
        }
    }
    return max.d
}

module.exports = getContourFarthestPoint;