function POIIsInPolygon(lat,lng,contour) {
    let nbPassedLine = 0;
    for (let i=0;i<contour.length;i++) {
        if (contour[i][0] instanceof Array && POIIsInPolygon(lat,lng,contour[i]))
            return true;

        if (i === contour.length-1)
            break;

        const [,latC1] = contour[i];
        const [lngC2,latC2] = contour[i+1];
        if (lngC2 < lng)
            continue;
        if (
            (latC1 < lat) !== (latC2 < lat)
        ) {
            nbPassedLine ++;
        }
    }
    return nbPassedLine % 2 === 1;
}

module.exports = POIIsInPolygon;