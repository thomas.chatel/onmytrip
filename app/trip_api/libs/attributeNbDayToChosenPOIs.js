function attributeNbDayToChosenPOIs(nbDays, nbStepsByDay, chosenPOIs) {
    let currentPOIIndex = 0;
    for (let nbDay=1;(nbDay<=nbDays) && (currentPOIIndex < chosenPOIs.length);nbDay++) {
        const nbStep = nbStepsByDay[nbDay]??nbStepsByDay.all;
        for (let i=0;(i<nbStep) && (currentPOIIndex < chosenPOIs.length);i++) {
            chosenPOIs[currentPOIIndex].nbDay = nbDay;
            currentPOIIndex ++;
        }
    }
}

module.exports = attributeNbDayToChosenPOIs