const request = require("../common_libs/request");
const jsonToUrlEncoded = require("../common_libs/jsonToUrlEncoded");
const metropolisSelectHostelsInCity = require("./metropolisSelectHostelsInCity");
const getHostelsInRectangle = require("./getHostelsInRectangle");
const CustomError = require("../common_libs/CustomError");


function findRectSearchCoordinates(center,contour,topLeft,btmRight) {
    for (const elem of contour) {
        if (elem[0] instanceof Array) {
            findRectSearchCoordinates(center,elem,topLeft,btmRight);
            continue;
        }
        const [lng,lat] = elem;
        if (
            lat > center.lat &&
            (topLeft.lat === null || lat > topLeft.lat)
        )
            topLeft.lat = lat;
        if (
            lat < center.lat &&
            (btmRight.lat === null || lat < btmRight.lat)
        )
            btmRight.lat = lat;

        if (
            lng < center.lng &&
            (topLeft.lng === null || lng < topLeft.lng)
        )
            topLeft.lng = lng

        if (
            lng > center.lng &&
            (btmRight.lng === null || lng > btmRight.lng)
        )
            btmRight.lng = lng
    }
}


async function findHostelsInCity(center,contour,chosenPOIs, categoriesHostel) {
    const topLeft = {
        lat: null,
        lng: null
    }

    const btmRight = {
        lat: null,
        lng: null
    }

    findRectSearchCoordinates(center,contour,topLeft,btmRight);

    const hostels = await getHostelsInRectangle(topLeft,btmRight,categoriesHostel)
        .then(({status,body}) =>
            status === 200 ?
                JSON.parse(body) :
                (() => { throw new CustomError("Can't find hostels in city tour", {topLeft,btmRight,categoriesHostel,status,body}, status)})()
    );


    return metropolisSelectHostelsInCity(hostels,contour,chosenPOIs);
}

module.exports = findHostelsInCity;
