const nbDay = require("../common_libs/asserts/nbDay");
const latlon = require("../common_libs/asserts/latlon");
const latlonFormat = require("../common_libs/asserts/format/latlon");
const timeFormat = require("../common_libs/asserts/format/time");
const numberFormat = require("../common_libs/asserts/format/number");
const nbStepsByDay = require("../common_libs/asserts/nbStepsByDay");
const nbStepsByDayFormat = require("../common_libs/asserts/format/nbStepsByDay");
const stringArray = require("../common_libs/asserts/stringArray");
const numberArray = require("../common_libs/asserts/numberArray");
const minimumDuration = require("../common_libs/asserts/minimumDuration");

const Validator = require("../common_libs/Validator");

module.exports = class AtoBValidator extends Validator {
    constructor(body) {
        super(body);

        this.fields = {
            from: { // latitude / longitude
                valid: latlon,
                format: latlonFormat,
                msg: "Point de départ invalide",
            },
            to: { // latitude / longitude
                valid: latlon,
                format: latlonFormat,
                msg: "Point d'arrivé invalide",
            },
            nbDays: {
                valid: nbDay,
                format: numberFormat,
                msg: "Vous devez rentrer un entier entre 1 et 30 inclus"
            },
            nbStepsByDay: {
                valid: nbStepsByDay,
                format: nbStepsByDayFormat,
                msg: "Pas plus de 5 étapes par jour"
            },
            minimumDurationByStep: {
                valid: minimumDuration,
                format: timeFormat,
                msg: "Vous devez spécifier une durée entre 1 demi heure et 3 heures"
            },
            interests: {
                valid: stringArray,
                msg: "Vous avez mal rentrez vos types d'étapes",
                required: false
            },
            interestsRestaurant: {
                valid: stringArray,
                msg: "Vous avez mal rentrez vos types de restaurants",
                required: false
            },
            categoriesHostel: {
                valid: numberArray,
                msg: "Vous avez mal rentrez vos catégories d'hôtel",
                required: false
            }
        }
    }
}
