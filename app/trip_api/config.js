const config = {
	nbActiveHourByDay: 8,
	radiusToGetTags: 50, // in km
	maxNbRestaurantsInChoice: 3,
	maxNbHostelsInChoice: 3,
	coordinatesToGetTags: [
		{ // Paris
			lat: 48.86869221521537,
			lng: 2.3447797171208697
		},
		{ // Lyon
			lat: 45.759403060573035,
			lng: 4.85059301386971
		},
		{ // Toulouse
			lat: 43.60001798934657,
			lng: 1.4405867651990667
		}
	],
	RESTAURANT_CATEGORY: "RESTAURANT",
	NIGHTLIFE_CATEGORY: "NIGHTLIFE",

	POITYPE_POI: "POI",
	POITYPE_RESTAURANT: "restaurant",
	POITYPE_HOSTEL: "hostel",
	LIMIT_TAGS: 5,
	LANGUAGE_TAGS: "fr"
}

config.categoriesToSkipForPoi = [config.RESTAURANT_CATEGORY,config.NIGHTLIFE_CATEGORY];

module.exports = config;