const express = require("express")
const cors = require("cors")
const AToBRoute = require("./routes/AToB")
const tourCityRoute = require("./routes/tourCity")
const tagsRoute = require("./routes/tags");
const hostelCategoriesRoute = require("./routes/hostelCategories");
const { isAuth } = require("./common_libs/security")
const preferenceRouter = require("./routes/preferences")
const logErrorsMiddleware = require("./common_libs/logErrorsMiddleware")

const app = express()

app.use(cors())
app.use(express.json())
app.use(express.static("public"))
app.use(isAuth)

app.use("/hostel_categories", hostelCategoriesRoute)

app.use("/tags", tagsRoute);

app.use("/tourCity", tourCityRoute)

app.use("/AtoB", AToBRoute)

app.use("/preferences", preferenceRouter)

app.use(logErrorsMiddleware)

module.exports = app;
